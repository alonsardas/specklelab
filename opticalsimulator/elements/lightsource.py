import numpy as np
from opticalsimulator.elements.general import Borders, FieldGrid


class LightSource(object):
    def __init__(self, wavelength=1):
        self.wavelength = wavelength

    def get_intensity(self, xs, ys) -> np.ndarray:
        raise NotImplementedError()


def create_field_grid_by_light_source(borders: Borders, x_pixels, y_pixels, light_source: LightSource) -> FieldGrid:
    x_range = np.linspace(borders.min_x, borders.max_x, x_pixels)
    y_range = np.linspace(borders.min_y, borders.max_y, y_pixels)
    xs, ys = np.meshgrid(x_range, y_range)

    Es = light_source.get_intensity(xs, ys)
    # Making sure the field is a complex number
    Es = Es.astype('complex128')
    return FieldGrid(xs, ys, Es, light_source.wavelength)


class EmptyLightSource(LightSource):

    def get_intensity(self, xs, ys) -> np.ndarray:
        return 0 * xs


class CircularLightSource(LightSource):
    CENTER_X = 0
    CENTER_Y = 0
    RADIUS = 0.05

    def __init__(self, center_x=CENTER_X, center_y=CENTER_Y, radius=RADIUS, **args):
        super().__init__(**args)
        self.center_x = center_x
        self.center_y = center_y
        self.radius = radius

    def get_intensity(self, xs, ys):
        is_inside = (xs - self.center_x) ** 2 + (ys - self.center_y) ** 2 < self.radius ** 2
        return is_inside.astype(int)


class CircularBlockLightSource(LightSource):
    # This can be used to show the fresnel_spot
    CENTER_X = 0
    CENTER_Y = 0
    RADIUS = 0.05

    def __init__(self, center_x=CENTER_X, center_y=CENTER_Y, radius=RADIUS):
        self.center_x = center_x
        self.center_y = center_y
        self.radius = radius

    def get_intensity(self, xs, ys):
        is_outside = (xs - self.center_x) ** 2 + (ys - self.center_y) ** 2 > self.radius ** 2
        return is_outside.astype(int)


class GaussianLightSource(LightSource):
    CENTER_X = 0
    CENTER_Y = 0
    SIGMA = 0.00001

    def __init__(self, center_x=CENTER_X, center_y=CENTER_Y, sigma=SIGMA, **args):
        super().__init__(**args)
        self.center_x = center_x
        self.center_y = center_y
        self.sigma = sigma

    def get_intensity(self, xs, ys):
        rs = np.sqrt((xs - self.center_x) ** 2 + (ys - self.center_y) ** 2)
        return np.exp(-rs ** 2 / (self.sigma ** 2))


class BoxLightSource(LightSource):
    def __init__(self, box_width: float, box_height: float, **args):
        super().__init__(**args)
        self.box_width = box_width
        self.box_height = box_height

    def get_intensity(self, xs, ys) -> np.ndarray:
        good_xs = np.logical_and(-self.box_width / 2 < xs, xs < self.box_width / 2)
        good_ys = np.logical_and(-self.box_height / 2 < ys, ys < self.box_height / 2)
        good_dots: np.ndarray = np.logical_and(good_xs, good_ys)
        return good_dots.astype('complex128')
