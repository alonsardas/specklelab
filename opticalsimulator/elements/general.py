import numpy as np


class Borders(object):
    def __init__(self, min_x, min_y, max_x, max_y):
        self.min_x = min_x
        self.min_y = min_y
        self.max_x = max_x
        self.max_y = max_y

    @property
    def width(self):
        return self.max_x - self.min_x

    @property
    def height(self):
        return self.max_y - self.min_y

    @property
    def center_x(self):
        return (self.min_x + self.max_x) / 2

    @property
    def center_y(self):
        return (self.min_y + self.max_y) / 2

    def __repr__(self):
        return f'Borders(x: [{self.min_x}-{self.max_x}], y: [{self.min_y}-{self.max_y}])'


def get_borders_around_center(center_x, center_y, width, height) -> Borders:
    return Borders(center_x - width / 2, center_y - width / 2, center_x + width / 2, center_y + height / 2)


class FieldGrid(object):
    def __init__(self, Xs: np.ndarray, Ys: np.ndarray, Es: np.ndarray, wavelength=1):
        self.Xs = Xs
        self.Ys = Ys
        self.Es = Es
        self.wavelength = wavelength

    def get_extent(self):
        xs = self.Xs[0, :]
        ys = self.Ys[:, 0]

        extent = (xs[0], xs[-1] + (xs[1] - xs[0]), ys[0], ys[-1] + (ys[1] - ys[0]))
        return extent


class OpticalDevice(object):
    def prop_through(self, field: FieldGrid):
        raise NotImplementedError()
