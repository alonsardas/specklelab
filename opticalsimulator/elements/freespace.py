import matplotlib.axes
import numpy as np

from opticalsimulator.elements.general import OpticalDevice, FieldGrid, Borders
from opticalsimulator.elements.lightsource import EmptyLightSource, create_field_grid_by_light_source


class AngularMomentumPropagator(OpticalDevice):
    def __init__(self, distance):
        super().__init__()
        self.distance = distance

    def prop_through(self, field: FieldGrid):
        fa = np.fft.fft2(field.Es)
        dx = field.Xs[0, 1] - field.Xs[0, 0]
        dy = field.Ys[1, 0] - field.Ys[0, 0]
        freq_x = np.fft.fftfreq(field.Es.shape[1], d=dx)
        freq_y = np.fft.fftfreq(field.Es.shape[0], d=dy)

        freq_Xs, freq_Ys = np.meshgrid(freq_x, freq_y)

        light_k = 2 * np.pi / field.wavelength
        k_x = freq_Xs * 2 * np.pi
        k_y = freq_Ys * 2 * np.pi

        k_z_sqr = light_k ** 2 - (k_x ** 2 + k_y ** 2)
        # Remove all the negative component, as they represent evanescent waves,
        # See Fourier Optics page 58
        np.maximum(k_z_sqr, 0, out=k_z_sqr)
        k_z = np.sqrt(k_z_sqr)

        # Propagate light by adding the phase,
        # see Fourier Optics page 74
        fa *= np.exp(1j * k_z * self.distance)

        field.Es = np.fft.ifft2(fa)
        return field


class FrenselPropagator(OpticalDevice):
    def __init__(self, borders: Borders, x_pixels, y_pixels, distance: float):
        super().__init__()

        self.new_field = create_field_grid_by_light_source(borders, x_pixels, y_pixels, EmptyLightSource())
        self.distance = distance

    def prop_through(self, field: FieldGrid):
        for y_index, Es_y in enumerate(self.new_field.Es):
            for x_index, Es in enumerate(Es_y):
                x = self.new_field.Xs[y_index, x_index]
                y = self.new_field.Ys[y_index, x_index]
                self.new_field.Es[y_index, x_index] = self.calc_E_at_point(x, y, field)

        # Update new field:
        field.Xs = self.new_field.Xs
        field.Ys = self.new_field.Ys
        field.Es = self.new_field.Es

        return field

    def calc_E_at_point(self, x, y, field: FieldGrid):
        E = 0
        wave_length = 1
        k = (2 * np.pi) / wave_length

        dx_tag = field.Xs[0, 1] - field.Xs[0, 0]
        dy_tag = field.Ys[1, 0] - field.Ys[0, 0]

        """
        for y_index, Es_y in enumerate(field.Es):
            for x_index, Es in enumerate(Es_y):
                x_tag = field.Xs[y_index, x_index]
                y_tag = field.Ys[y_index, x_index]
                E += np.exp((1j * k / (2 * self.distance)) * ((x - x_tag) ** 2 + (y - y_tag) ** 2)) * dx_tag * dy_tag
        """

        E = np.sum(field.Es * np.exp(
            (1j * k / (2 * self.distance)) * ((x - field.Xs) ** 2 + (y - field.Ys) ** 2)) * dx_tag * dy_tag)
        E *= np.exp(1j * k * self.distance) / (1j * wave_length * self.distance)
        return E


class ZScanner(OpticalDevice):
    def __init__(self, z_distance, z_pixels, ax: matplotlib.axes.Axes):
        self.zs = np.linspace(0, z_distance, z_pixels)
        self.ax = ax

        self.ax.set_title('propagation of light in free space')
        self.ax.set_xlabel('x')
        self.ax.set_ylabel('z - direction of propagation')

        self.image = None

    def prop_through(self, field: FieldGrid):
        y_pixels, x_pixels = field.Es.shape
        y_row = int(y_pixels / 2)  # Taking the middle row as representative
        picture = np.zeros((len(self.zs), x_pixels), 'complex128')
        picture[0, :] = field.Es[y_row, :]
        for i in range(1, len(self.zs)):
            dz = self.zs[i] - self.zs[i - 1]
            light_propagator = AngularMomentumPropagator(dz)
            light_propagator.prop_through(field)
            picture[i, :] = field.Es[y_row, :]
        field_extent = field.get_extent()
        extent = (field_extent[0], field_extent[1], 0, self.zs[-1])
        self.ax.imshow(abs(picture) ** 2, origin='lower', extent=extent, aspect='auto')
        self.image = abs(picture) ** 2

    def get_image(self) -> np.ndarray:
        return self.image
