import matplotlib.axes
import matplotlib.image
import matplotlib.colors
import numpy as np

from opticalsimulator.elements.general import FieldGrid, Borders, OpticalDevice


class Camera(OpticalDevice):
    def prop_through(self, f: FieldGrid):
        self.show(f)

    def show(self, f: FieldGrid):
        raise NotImplementedError()


class BasicCamera(Camera):
    def __init__(self, name, ax: matplotlib.axes.Axes, borders: Borders = None, slice_of_interest=None,
                 log_scale_intensity=False, live_update=True):
        self.name = name
        self.ax = ax
        self.borders = borders
        self.slice_of_interest = slice_of_interest or np.index_exp[:, :]
        self.log_scale_intensity = log_scale_intensity
        self.live_update = live_update

        self.ax.set_title(self.name)
        self.im: matplotlib.image.AxesImage = self.ax.imshow(np.zeros((1, 1)), aspect='auto', origin='lower')
        self.ax.figure.colorbar(self.im, ax=self.ax)
        self.image = None

    def show(self, f: FieldGrid):
        if self.borders is not None:
            f = self.crop_to_borders(f)
        intensity = self._calc_intensity(f)
        self.image = intensity

        Xs, Ys = f.Xs[self.slice_of_interest], f.Ys[self.slice_of_interest]
        xs = Xs[0, :]
        ys = Ys[:, 0]

        extent = (xs[0], xs[-1] + (xs[1] - xs[0]), ys[0], ys[-1] + (ys[1] - ys[0]))
        data = intensity[self.slice_of_interest]

        self.im.set_data(data)
        if self.log_scale_intensity:
            self.im.set_norm(matplotlib.colors.LogNorm(vmin=data.min(), vmax=data.max()))
        self.im.set_clim(vmin=data.min(), vmax=data.max())
        self.im.set_extent(extent)

        if self.live_update:
            self.ax.figure.show()
            self.ax.figure.canvas.flush_events()

    def _calc_intensity(self, f: FieldGrid):
        intensity = np.abs(f.Es) ** 2
        return intensity

    def get_image(self) -> np.ndarray:
        return self.image

    def crop_to_borders(self, field: FieldGrid):
        xs = field.Xs[0, :]
        ys = field.Ys[:, 0]

        min_x_index = np.where(xs > self.borders.min_x)[0][0]
        max_x_index = np.where(xs < self.borders.max_x)[0][-1]
        min_y_index = np.where(ys > self.borders.min_y)[0][0]
        max_y_index = np.where(ys < self.borders.max_y)[0][-1]
        return FieldGrid(field.Xs[min_y_index:max_y_index, min_x_index:max_x_index],
                         field.Ys[min_y_index:max_y_index, min_x_index:max_x_index],
                         field.Es[min_y_index:max_y_index, min_x_index:max_x_index])
