import numpy as np

from opticalsimulator.elements.general import Borders, FieldGrid, OpticalDevice


class Mask(OpticalDevice):
    def __init__(self, active_borders: Borders, dark_outside_borders=True):
        """
        The Mask has an active range where it does something to phase or amplitude.
        outside of it it either blocks the light or just lets it through
        """
        self.active_borders = active_borders
        self.dark_outside_borders = dark_outside_borders

    def prop_through(self, field_grid: FieldGrid) -> FieldGrid:
        """
        This just zero's whoever is out of active_borders. Inheriting classes should change phases etc.
        """
        if self.dark_outside_borders:
            to_zero_mask = field_grid.Xs < self.active_borders.min_x
            to_zero_mask = np.logical_or(to_zero_mask, field_grid.Xs > self.active_borders.max_x)
            to_zero_mask = np.logical_or(to_zero_mask, field_grid.Ys < self.active_borders.min_y)
            to_zero_mask = np.logical_or(to_zero_mask, field_grid.Ys > self.active_borders.max_y)

            field_grid.Es[to_zero_mask] = 0 + 0j
        return field_grid
