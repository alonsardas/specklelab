from opticalsimulator.elements.general import Borders, OpticalDevice
from opticalsimulator.elements.lightsource import create_field_grid_by_light_source, LightSource


class OpticalSystem(object):
    def __init__(self, borders: Borders, x_pixels, y_pixels, light_source: LightSource):
        self.borders = borders
        self.x_pixels = x_pixels
        self.y_pixels = y_pixels
        self.light_source = light_source
        self.devices = []

    def add_device(self, device: OpticalDevice):
        self.devices.append(device)

    def simulate(self):
        field = create_field_grid_by_light_source(self.borders, self.x_pixels, self.y_pixels, self.light_source)
        for device in self.devices:
            device: OpticalDevice = device
            device.prop_through(field)
        return field
