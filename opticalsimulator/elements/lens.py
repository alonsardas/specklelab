import numpy as np

from opticalsimulator.elements.general import OpticalDevice, FieldGrid


class FourierLens(OpticalDevice):
    def __init__(self, f=None):
        self.f = f

    def prop_through(self, field: FieldGrid):
        # Using norm='ortho' because this is way, having 2 lens one after the other will result
        # with the correct amplitudes
        field.Es = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(field.Es), norm='ortho'))

        if self.f is not None:
            wavelength = field.wavelength
            dx = field.Xs[0, 1] - field.Xs[0, 0]
            dy = field.Ys[1, 0] - field.Ys[0, 0]
            freq_x = np.fft.fftfreq(field.Es.shape[1], d=dx)
            freq_y = np.fft.fftfreq(field.Es.shape[0], d=dy)

            freq_x = np.fft.fftshift(freq_x)
            freq_y = np.fft.fftshift(freq_y)

            freq_Xs, freq_Ys = np.meshgrid(freq_x, freq_y)

            light_k = 2 * np.pi / wavelength
            k_xs = freq_Xs * 2 * np.pi
            k_ys = freq_Ys * 2 * np.pi

            field.Xs = k_xs * self.f / light_k
            field.Ys = k_ys * self.f / light_k
        return field