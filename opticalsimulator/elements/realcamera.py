from opticalsimulator.elements.general import Borders, OpticalDevice
import numpy as np

try:
    from vimba import Vimba
except ImportError:
    print("can't use vimba camera")
try:
    import pco
except ImportError:
    print("can't use pco camera")
import matplotlib.pyplot as plt


class VimbaCamera(OpticalDevice):
    def __init__(self, camera_num, exposure_time=None, borders: Borders = None):
        '''
            You can check camera_num using vimb.get_all_cameras()[0].get_model()
            exposure_time in us (micro seconds)
        '''
        self.camera_num = camera_num
        self.borders = None

        self._vimb = Vimba.get_instance()
        self._vimb.__enter__()
        self._cam = self._vimb.get_all_cameras()[camera_num]
        self._cam.__enter__()

        if exposure_time:
            self.set_exposure_time(exposure_time)
        if borders:
            self.set_borders(borders)
        else:
            pass
            # TODO: Check why this is doing problems...
            # b = Borders(0, 0, 800, 600)
            # self.set_borders(b)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def get_image(self) -> np.ndarray:
        # takes ~42ms
        frame = self._cam.get_frame()
        # Make sure pixel format if mono8 (can be set through the VimbaViewer->all->ImageFormatControl.
        # Probably can also from python, but see no reason to change it from here :P
        im = frame.as_numpy_ndarray()[:, :, 0]
        return im

    def show_image(self, im=None, title=None):
        if im is None:
            im = self.get_image()
        fig, axes = plt.subplots()
        im = axes.imshow(im)
        fig.colorbar(im, ax=axes)
        if title:
            axes.set_title(title)
        fig.show()
        return fig, axes

    def save_image(self, path):
        im = self.get_image()
        f = open(path, 'wb')
        np.savez(f, image=im)
        f.close()
        return im

    def load_image(self, path, show=True):
        im = np.load(path)['image']
        if show:
            self.show_image(im)
        return im

    def get_exposure_time(self):
        return self._cam.ExposureTime.get()

    def set_exposure_time(self, exposure_time):
        return self._cam.ExposureTime.set(exposure_time)

    def set_borders(self, borders: Borders):
        pixel_step = 8  # Only multiples of this are accepted
        # Setting offset to (0,0) to make sure we are able to resize the ROI according the required values
        self._cam.OffsetX.set(0)
        self._cam.OffsetY.set(0)
        self._cam.Width.set(borders.width // pixel_step * pixel_step)
        self._cam.Height.set(borders.height // pixel_step * pixel_step)
        self._cam.OffsetX.set(borders.min_x // pixel_step * pixel_step)
        self._cam.OffsetY.set(borders.min_y // pixel_step * pixel_step)

    def get_borders(self) -> Borders:
        borders = Borders(0, 0, 0, 0)
        borders.min_x = self._cam.OffsetX.get()
        borders.min_y = self._cam.OffsetY.get()
        borders.max_x = borders.min_x + self._cam.Width.get()
        borders.max_y = borders.min_y + self._cam.Height.get()
        return borders

    def close(self):
        self._cam.__exit__(0, 0, 0)
        self._vimb.__exit__(0, 0, 0)


class PCOCamera(OpticalDevice):
    def __init__(self, exposure_time=0.1, borders: Borders = None):
        """
        :param borders: (1-2054, 1-2054)
        :param exposure_time: in seconds
        """
        self.borders = borders

        self._cam = pco.Camera()

        self.set_exposure_time(exposure_time)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def get_image(self, roi=None):
        self._cam.record()
        if roi is None:
            if self.borders:
                roi = (self.borders.min_x, self.borders.min_y, self.borders.max_x, self.borders.max_y)

        im, meta = self._cam.image(roi=roi)
        return im

    def show_image(self, im=None):
        if im is None:
            im = self.get_image()
        fig, axes = plt.subplots()
        axes.imshow(im)
        fig.show()
        return fig, axes

    def save_image(self, path):
        im = self.get_image()
        f = open(path, 'wb')
        np.savez(f, image=im)
        f.close()

    def load_image(self, path, show=True):
        im = np.load(path)['image']
        if show:
            self.show_image(im)
        return im

    def set_exposure_time(self, exposure_time):
        """ Set exposure_time in seconds"""
        return self._cam.set_exposure_time(exposure_time)

    def get_exposure_time(self):
        """ Get exposure_time in seconds"""
        d = self._cam.sdk.get_delay_exposure_time()
        exposure = d['exposure']
        timebase = d['exposure timebase']
        if timebase == 'ms':
            exposure *= 1e-3
        elif timebase == 'us':
            exposure *= 1e-6
        elif timebase == 'ns':
            exposure *= 1e-9
        return exposure

    def set_borders(self, borders: Borders):
        self.borders = borders

    def get_borders(self) -> Borders:
        return self.borders

    def close(self):
        self._cam.close()
