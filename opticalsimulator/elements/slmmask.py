import numpy as np
import cv2

from opticalsimulator.elements.general import Borders, FieldGrid
from opticalsimulator.elements.mask import Mask


class SLMMask(Mask):
    def __init__(self, active_borders: Borders, pixels_x=1272, pixels_y=1024, **mask_params):
        """ active_borders in space"""
        super().__init__(active_borders=active_borders, **mask_params)
        self.pixels_x = pixels_x
        self.pixels_y = pixels_y
        self.pixel_size_x = active_borders.width / pixels_x
        self.pixel_size_y = active_borders.height / pixels_y

        x = np.arange(0, self.pixels_x)
        y = np.arange(0, self.pixels_y)
        self.x_pixel_indexes, self.y_pixel_indexes = np.meshgrid(x, y)

        self.phase_grid = np.zeros((pixels_y, pixels_x))
        self.correction = np.zeros((pixels_y, pixels_x))

    def update_phase(self, phase):
        assert isinstance(phase, np.ndarray)
        # pixels_x = amount of columns
        phase = cv2.resize(phase, (self.pixels_x, self.pixels_y), interpolation=cv2.INTER_AREA)
        assert phase.shape == (self.pixels_y, self.pixels_x)
        self.phase_grid = phase
        self.update()

    def update_phase_in_active(self, phase):
        return self.update_phase(phase)

    def update(self):
        pass

    def prop_through(self, field_grid: FieldGrid) -> FieldGrid:
        field_grid = super().prop_through(field_grid)
        phases = self._coords_to_phase(field_grid)
        field_grid.Es *= np.exp(1j * phases)

        return field_grid

    def _coords_to_phase(self, field_grid: FieldGrid):
        # Think of Xs as x coord and Ys as y coord. it works
        moved_Xs = field_grid.Xs - self.active_borders.min_x
        moved_Ys = field_grid.Ys - self.active_borders.min_y
        x_pix_index = moved_Xs / self.pixel_size_x
        y_pix_index = moved_Ys / self.pixel_size_y

        x_pix_index = x_pix_index.astype('int')
        y_pix_index = y_pix_index.astype('int')

        to_zero_x = x_pix_index >= self.pixels_x - 1
        to_zero_x = np.logical_or(to_zero_x, x_pix_index < 0)

        to_zero_y = y_pix_index >= self.pixels_y - 1
        to_zero_y = np.logical_or(to_zero_y, y_pix_index < 0)

        x_pix_index[to_zero_x] = 0
        y_pix_index[to_zero_y] = 0

        phases = self.phase_grid[y_pix_index, x_pix_index]
        phases[to_zero_x] = 0
        phases[to_zero_y] = 0

        return phases
