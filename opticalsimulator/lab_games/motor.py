import time
import numpy as np
from opticalsimulator.simulations.ronen.misc_utils import spiral_motor

SERIAL_PORT = "COM13"


class ZaberMotors(object):
    MAX_ABS_LOCATION = 300000
    MM_TO_ABS = 20997  # Moving 20997 in absolute moves 1 mm

    def __init__(self, serial_port=SERIAL_PORT, xy=True):

        try:
            from zaber_motion import Library, Units
            from zaber_motion.ascii import Connection
            Library.enable_device_db_store()
        except ImportError:
            print('can\t use zaber motor')
            raise

        self.con = Connection.open_serial_port(serial_port)
        self.device_list = self.con.detect_devices()
        self.ax_list = [dev.get_axis(1) for dev in self.device_list]
        self.xy = xy
        if xy:
            self.x_device, self.y_device = self.device_list
            self.x_ax, self.y_ax = self.ax_list

    def move_relative(self, ax, mms):
        # move in mm units
        ax.move_relative(mms, Units.LENGTH_MILLIMETRES)

    def move_x(self, mms):
        if not self.xy:
            raise Exception('this will work only if self.xy ! ')
        self.move_relative(self.x_ax, mms)

    def move_y(self, mms):
        if not self.xy:
            raise Exception('this will work only if self.xy ! ')
        self.move_relative(self.y_ax, mms)

    def home(self):
        for ax in self.ax_list:
            ax.home()

    def get_position(self):
        return [ax.get_position() for ax in self.ax_list]

    def spiral_scan(self, x_pixels, y_pixels, abs_beginning_x=MAX_ABS_LOCATION/2, abs_beginning_y=MAX_ABS_LOCATION/2,
                    pixel_size=0.5):
        # Pixel Size in mm

        self.x_ax.move_absolute(abs_beginning_x)
        self.y_ax.move_absolute(abs_beginning_y)

        for (dx, dy) in spiral_motor(x_pixels, y_pixels):
            cur_x, cur_y = self.get_position()
            next_x = abs_beginning_x + dx*self.MM_TO_ABS
            next_y = abs_beginning_y + dy*self.MM_TO_ABS
            if np.abs(cur_x - next_x) > pixel_size / 10:
                self.x_ax.move_absolute(next_x)
            if np.abs(cur_y - next_y) > pixel_size / 10:
                self.y_ax.move_absolute(next_y)

    def close(self):
        self.con.close()


try:
    import thorlabs_apt as apt
    # If python crashes - open the kinesis program and close it.
except ImportError:
    print('cant use thorlabs apt')


class ThorLabsMotors(object):
    """All units here are in mm"""
    # Max coincidence at x: 9.1, y:8.77

    def __init__(self, xyz=True):
        apt.core._cleanup()
        apt.core._lib = apt.core._load_library()
        self.serials = apt.list_available_devices()

        self.xyz = xyz
        if xyz:
            self.x_motor = apt.Motor(26001271)
            self.y_motor = apt.Motor(27500306)
            self.z_motor = apt.Motor(27501989)
        else:
            self.motors = [apt.Motor(ser[1]) for ser in self.serials]

    @staticmethod
    def move_relative(motor, mms):
        """move in mm units"""
        motor.move_by(mms, blocking=True)

    @staticmethod
    def move_absolute(motor, location):
        """Location in mms"""
        motor.move_to(location, blocking=True)

    def move_x_relative(self, mms):
        if not self.xyz:
            raise Exception('this will work only if self.xyz ! ')
        self.move_relative(self.x_motor, mms)

    def move_y_relative(self, mms):
        if not self.xyz:
            raise Exception('this will work only if self.xyz ! ')
        self.move_relative(self.y_motor, mms)

    def move_x_absolute(self, location):
        if not self.xyz:
            raise Exception('this will work only if self.xyz ! ')
        self.move_absolute(self.x_motor, location)

    def move_y_absolute(self, location):
        if not self.xyz:
            raise Exception('this will work only if self.xyz ! ')
        self.move_absolute(self.y_motor, location)

    def move_z_absolute(self, location):
        if not self.xyz:
            raise Exception('this will work only if self.xyz ! ')
        self.move_absolute(self.z_motor, location)

    def spiral_scan(self, x_pixels, y_pixels, abs_beginning_x, abs_beginning_y, pixel_size=0.5, sleep_period=0.2):
        # Pixel Size in mm

        self.move_x_absolute(abs_beginning_x)
        self.move_y_absolute(abs_beginning_y)

        cur_x = 0
        cur_y = 0
        for (dx, dy) in spiral_motor(x_pixels, y_pixels):
            print(abs_beginning_x + dx*pixel_size, abs_beginning_y+dy*pixel_size)
            rel_x = dx - cur_x
            rel_y = dy - cur_y
            if rel_x != 0:
                self.move_x_relative(rel_x * pixel_size)
            if rel_y != 0:
                self.move_y_relative(rel_y * pixel_size)
            time.sleep(sleep_period)
            cur_x = dx
            cur_y = dy
            # yield

    @staticmethod
    def close():
        apt.core._cleanup()
        apt.core._lib = apt.core._load_library()
