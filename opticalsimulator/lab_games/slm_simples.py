import os;
import sys;

sys.path.append(os.path.abspath('..'))
from opticalsimulator.elements import *

import numpy as np
import matplotlib.pyplot as plt
import time


def alternating_angles():
    slm = RealSLMDevice()

    angle = np.pi / 4
    sign = -1

    while True:
        sign *= -1
        phase = get_mirror_phase(sign * 50, sign * angle, slm.correction.shape[1], slm.correction.shape[0])
        time.sleep(0.6)
        plt.pause(0.0001)  # Note this correction
        slm.update_phase(phase)


def get_mirror_phase(m, angle, sizex, sizey):
    X = np.linspace(0, m * np.pi, sizex)
    Y = np.linspace(0, m * np.pi, sizey)
    Xs, Ys = np.meshgrid(X, Y)
    phase = np.sin(angle) * Xs + np.cos(angle) * Ys
    return phase


def pi_step(x):
    slm = RealSLMDevice(1)
    phase = np.zeros(slm.correction.shape)
    phase[:, x:] = np.pi
    slm.update_phase(phase)


def moving_pi_step():
    slm = RealSLMDevice(1)
    phase = np.ones(slm.correction.shape) * np.pi

    for x in np.linspace(650, 750, 20):
        pixel_index = int(x)
        phase[:, :pixel_index] = 0
        slm.update_phase(phase)
        time.sleep(0.2)


def normal():
    slm = RealSLMDevice(1)
    slm.update_phase(slm.correction * 0)


if __name__ == "__main__":
    normal()
    pi_step(700)
    # moving_pi_step()

    plt.show()
