import lazy_import
import logging

# Using lazy import on thorlabs_apt is important since just importing the module disables other programs
# (like Kinesis) from controlling the motors
lazy_import.lazy_module("thorlabs_apt")
lazy_import.lazy_module("vimba")
lazy_import.lazy_module("pco")

# NOTE: lazy_import package calls logging.basicConfig which adds a StreamHandler.
# Here we undo this
logging.root.removeHandler(logging.root.handlers[0])

from opticalsimulator.elements import *
