import glob
import traceback
import typing

import matplotlib.axes
import matplotlib.figure
import matplotlib.pyplot as plt
from matplotlib_scalebar.scalebar import ScaleBar
import numpy as np
import pyperclip

from opticalsimulator.simulations.ronen.misc_utils import my_mesh

LOGS_DIR = "C:\\temp"


class FullOptimizerExperimentResult(object):
    def __init__(self, title=None):
        self.diffuser = None
        self.fixer = None

        self.blue_orig = np.zeros((2, 2))
        self.blue_speckles = np.zeros((2, 2))
        self.blue_fixed = np.zeros((2, 2))
        self.blue_second_mode = np.zeros((2, 2))

        self.red_X = None
        self.red_Y = None
        self.red_orig = np.zeros((2, 2))
        self.red_speckles = np.zeros((2, 2))
        self.red_fixed = np.zeros((2, 2))
        self.red_second_mode = np.zeros((2, 2))

        self.title = title

    @property
    def enhancement(self):
        # print("### SPDC ENHANCEMENT ###")
        # Choose pixels using original focus
        max_power = self.red_orig.max().max()
        # print('max_power:', max_power)
        limit_power = max_power / 4
        # print('limit_power:', limit_power)
        relevant_pixels = self.red_orig > limit_power
        original_tot_power = np.sum(self.red_orig[relevant_pixels])
        # print("original_tot_power", original_tot_power)

        if True:
            fig, ax = plt.subplots()
            ax.set_title("Relevant pixels")
            ax.imshow(relevant_pixels)
            fig.show()

        # Sum powers after fixing
        relevant_powers = self.red_fixed[relevant_pixels]
        # print("relevant_powers", relevant_powers)
        power_after_fix = np.sum(relevant_powers)
        # print("power_after_fix", power_after_fix)

        # estimate random speckle intensity
        mean_speckle_pixel = self.red_speckles.mean()
        # print("mean_speckle_pixel", mean_speckle_pixel)

        # print("len of relevant pixels:", len(relevant_powers))
        power_in_speckles = mean_speckle_pixel * len(relevant_powers)
        # print("power_in_speckles", power_in_speckles)

        enhancement = power_after_fix / power_in_speckles
        # print('enhancement', enhancement)


        efficiency = power_after_fix / original_tot_power
        # print("efficiency", efficiency)

        efficiency_minus_speckles = (power_after_fix - power_in_speckles) / (original_tot_power - power_in_speckles)
        # print("(efficiency_minus_speckles", efficiency_minus_speckles, ")")

        print(f'\n------SPDC--------\npower before : {original_tot_power:.2f}\n'
              f'power speckles : {power_in_speckles:.2f}\n'
              f'power after fix : {power_after_fix:.2f}\n'
              f'before / speckles : {original_tot_power / power_in_speckles:.2f}\n'
              f'enhancement : {enhancement:.2f}\n'
              f'efficiency : {efficiency:.2f}\n'
              f'------------------\n')

        return enhancement

    @property
    def pump_enhancement(self):
        # print("### PUMP ENHANCEMENT ###")
        # Choose pixels using original focus
        max_power = self.blue_orig.max().max()
        # print('max_power:', max_power)
        limit_power = max_power / 4
        # print('limit_power:', limit_power)
        relevant_pixels = self.blue_orig > limit_power
        original_tot_power = np.sum(self.blue_orig[relevant_pixels])
        # print("original_tot_power", original_tot_power)

        if True:
            fig, ax = plt.subplots()
            ax.set_title("Relevant pixels")
            ax.imshow(relevant_pixels)
            fig.show()

        # Sum powers after fixing
        relevant_powers = self.blue_fixed[relevant_pixels]
        # print("relevant_powers", relevant_powers)
        power_after_fix = np.sum(relevant_powers)
        # print("power_after_fix", power_after_fix)

        # estimate random speckle intensity
        mean_speckle_pixel = self.blue_speckles[self._blue_slice_of_interest].mean()
        # print("mean_speckle_pixel", mean_speckle_pixel)

        # print("len of relevant pixels:", len(relevant_powers))
        power_in_speckles = mean_speckle_pixel * len(relevant_powers)
        # print("power_in_speckles", power_in_speckles)

        enhancement = power_after_fix / power_in_speckles
        # print('\nenhancement', enhancement)

        efficiency = power_after_fix / original_tot_power
        # print("efficiency", efficiency)

        efficiency_minus_speckle = (power_after_fix - power_in_speckles) / (original_tot_power - power_in_speckles)
        # print("(efficiency_minus_speckle", efficiency_minus_speckle, ")")

        print(f'\n------PUMP--------\npower before : {original_tot_power:.2f}\n'
              f'power speckles : {power_in_speckles:.2f}\n'
              f'power after fix : {power_after_fix:.2f}\n'
              f'before / speckles : {original_tot_power / power_in_speckles:.2f}\n'
              f'enhancement : {enhancement:.2f}\n'
              f'efficiency : {efficiency:.2f}\n'
              f'------------------\n')

        return enhancement

    def show(self, title=None):
        fig, axes = plt.subplots(3, 2, constrained_layout=True)
        title = title or self.title
        if title:
            fig.suptitle(title)

        im = axes[0, 1].imshow(self.blue_orig)
        fig.colorbar(im, ax=axes[0, 1])
        axes[0, 1].set_title('Blue Orig')

        im = axes[1, 1].imshow(self.blue_speckles)
        fig.colorbar(im, ax=axes[1, 1])
        axes[1, 1].set_title('Blue Speckles')

        im = axes[2, 1].imshow(self.blue_fixed)
        fig.colorbar(im, ax=axes[2, 1])
        axes[2, 1].set_title('Blue Fixed')

        my_mesh(self.red_X, self.red_Y, self.red_orig, axes[0, 0])
        axes[0, 0].invert_xaxis()
        axes[0, 0].set_title(f'Red orig')

        my_mesh(self.red_X, self.red_Y, self.red_speckles, axes[1, 0])
        axes[1, 0].invert_xaxis()
        axes[1, 0].set_title(f'Red Speckles')

        my_mesh(self.red_X, self.red_Y, self.red_fixed, axes[2, 0])
        axes[2, 0].invert_xaxis()
        axes[2, 0].set_title(f'Red fixed')

        # Showing only relevant part of blue
        row_max, col_max = np.unravel_index(np.argmax(self.blue_orig, axis=None), self.blue_orig.shape)
        xrange_in_um = np.abs(self.red_X[-1] - self.red_X[0]) * 1000
        yrange_in_um = np.abs(self.red_Y[-1] - self.red_Y[0]) * 1000
        x_pixels_cam = xrange_in_um / 4.8
        y_pixels_cam = yrange_in_um / 4.8

        # Red features are twice as big as blue features, so before we had the right size in mms,
        # but here we normalize have the same features in frame
        x_pixels_cam /= 2
        y_pixels_cam /= 2

        x_pixels_to_side = int(x_pixels_cam / 2)
        y_pixels_to_side = int(y_pixels_cam / 2)
        axes[0, 1].set_xlim([col_max - x_pixels_to_side, col_max + x_pixels_to_side])
        axes[0, 1].set_ylim([row_max + y_pixels_to_side, row_max - y_pixels_to_side])
        axes[1, 1].set_xlim([col_max - x_pixels_to_side, col_max + x_pixels_to_side])
        axes[1, 1].set_ylim([row_max + y_pixels_to_side, row_max - y_pixels_to_side])
        axes[2, 1].set_xlim([col_max - x_pixels_to_side, col_max + x_pixels_to_side])
        axes[2, 1].set_ylim([row_max + y_pixels_to_side, row_max - y_pixels_to_side])

        fig.show()

    def show_fig3(self, fig4=True, insets=True):
        if not fig4:
            fig, axes = plt.subplots(2, 2, constrained_layout=True)
        else:
            fig, axes = plt.subplots(3, 2, constrained_layout=True)

        # Fix RED_X, RED_Y so we'll be around (0, 0)
        RED_X = self.red_X - self.red_X.mean()
        RED_Y = self.red_Y - self.red_Y.mean()

        # Show red
        my_mesh(RED_X, RED_Y, self.red_orig, axes[0, 0], c_label='photon pairs/sec')
        axes[0, 0].set_title(f'(a) SPDC no diffuser')
        axes[0,0].set_xlabel("x (mm)")
        axes[0,0].set_ylabel("y (mm)")
        axes[0, 0].invert_xaxis()

        my_mesh(RED_X, RED_Y, self.red_speckles, axes[1, 0], c_label='photon pairs/sec')
        axes[1, 0].set_title(f'(c) Two-photon speckle')
        axes[1,0].set_xlabel("x (mm)")
        axes[1,0].set_ylabel("y (mm)")
        axes[1, 0].invert_xaxis()

        if fig4:
            my_mesh(RED_X, RED_Y, self.red_fixed, axes[2, 0], c_label='photon pairs/sec')
            axes[2, 0].set_title(f'(e) SPDC optimized')
            axes[2, 0].set_xlabel("x (mm)")
            axes[2, 0].set_ylabel("y (mm)")
            axes[2, 0].invert_xaxis()

        if insets:
            from mpl_toolkits.axes_grid1.inset_locator import inset_axes
            from mpl_toolkits.axes_grid1.colorbar import colorbar

            singles_before_ax = axes[0, 0].inset_axes((0, 0, 0.3, 0.3))
            before_path = r'"C:\ronen\HUJI\Amirim\Paper\Data For Figures\figs_4_5_all_data\red_before.scan"'
            if not fig4:
                before_path = r'"C:\ronen\HUJI\Amirim\Paper\Data For Figures\fig_3_all_data\red_before.scan"'
            r = ScanResult()
            r.loadfrom(before_path)
            im = singles_before_ax.imshow(r.single2s, clim=(0, r.single2s.max()))
            singles_before_ax.set_axis_off()

            self._add_cbar_to_inset(inset_ax=singles_before_ax, im=im)

            singles_speckles_ax = axes[1, 0].inset_axes((0, 0, 0.3, 0.3))
            speckles_path = r'"C:\ronen\HUJI\Amirim\Paper\Data For Figures\figs_4_5_all_data\red_speckles.scan"'
            if not fig4:
                speckles_path = r'"C:\ronen\HUJI\Amirim\Paper\Data For Figures\fig_3_all_data\red_speckles.scan"'
            r = ScanResult()
            r.loadfrom(speckles_path)
            im = singles_speckles_ax.imshow(r.single2s, clim=(0, r.single2s.max()))
            singles_speckles_ax.set_axis_off()

            self._add_cbar_to_inset(inset_ax=singles_speckles_ax, im=im)

        if insets and fig4:
            from mpl_toolkits.axes_grid1.inset_locator import inset_axes
            from mpl_toolkits.axes_grid1.colorbar import colorbar

            singles_after_ax = axes[2, 0].inset_axes((0, 0, 0.3, 0.3))
            after_path = r'"C:\ronen\HUJI\Amirim\Paper\Data For Figures\figs_4_5_all_data\red_fixed.scan"'
            r = ScanResult()
            r.loadfrom(after_path)
            im = singles_after_ax.imshow(r.single2s, clim=(0, r.single2s.max()))
            singles_after_ax.set_axis_off()

            self._add_cbar_to_inset(inset_ax=singles_after_ax, im=im)


        # Showing only relevant part of blue
        # middle of beam
        row_max, col_max = np.unravel_index(np.argmax(self.blue_orig, axis=None), self.blue_orig.shape)
        xrange_in_um = np.abs(self.red_X[-1] - self.red_X[0]) * 1000
        yrange_in_um = np.abs(self.red_Y[-1] - self.red_Y[0]) * 1000
        x_pixels_cam = xrange_in_um / 4.8
        y_pixels_cam = yrange_in_um / 4.8

        # This is the right size in mms, with same size as red.
        # Red features are twice as big so we half pixel number to have the same features in frame
        x_pixels_cam /= 2
        y_pixels_cam /= 2

        x_pixels_to_side = int(x_pixels_cam / 2)
        y_pixels_to_side = int(y_pixels_cam / 2)

        blue_slice_of_interest = np.index_exp[row_max - y_pixels_to_side:row_max + y_pixels_to_side,
                                 col_max - x_pixels_to_side:col_max + x_pixels_to_side]

        # This works because of how my_mesh works, which actually cares only about dx, x[0], x[-1]
        # + I suffer from L.A.Z.Y
        BLUE_X = RED_X / 2
        BLUE_Y = RED_Y / 2

        blue_orig = self.blue_orig[blue_slice_of_interest]
        my_mesh(BLUE_X, BLUE_Y, blue_orig, axes[0, 1], c_label='arbitrary units')
        axes[0, 1].set_title('(b) Pump no diffuser')
        axes[0,1].set_xlabel("x (mm)")
        axes[0,1].set_ylabel("y (mm)")

        blue_speckles = self.blue_speckles[blue_slice_of_interest]
        my_mesh(BLUE_X, BLUE_Y, blue_speckles, axes[1, 1], c_label='arbitrary units')
        axes[1, 1].set_title('(d) Pump speckle')
        axes[1,1].set_xlabel("x (mm)")
        axes[1,1].set_ylabel("y (mm)")

        if fig4:
            blue_fixed = self.blue_fixed[blue_slice_of_interest]
            my_mesh(BLUE_X, BLUE_Y, blue_fixed, axes[2, 1], c_label='arbitrary units')
            axes[2, 1].set_title('(f) Pump optimized')
            axes[2, 1].set_xlabel("x (mm)")
            axes[2, 1].set_ylabel("y (mm)")

        fig.show()
        return fig

    def _add_cbar_to_inset(self, inset_ax, im):
        from mpl_toolkits.axes_grid1.inset_locator import inset_axes
        from mpl_toolkits.axes_grid1.colorbar import colorbar

        option = 1
        if option == 1:
            return
        elif option == 2:
            cbar_ax = inset_axes(inset_ax,
                                 width="5%",  # width = 10% of parent_bbox width
                                 height="100%",  # height : 50%
                                 loc='lower right',
                                 bbox_to_anchor=(0.05, 0., 1, 1),
                                 bbox_transform=inset_ax.transAxes,
                                 borderpad=0,
                                 )
            cbar = colorbar(im, cax=cbar_ax, ticks=[1000, 4000])
            cbar.ax.tick_params(labelsize=6, colors='white')
            cbar.ax.yaxis.set_ticks_position('right')
        elif option == 3:
            cbar_ax = inset_axes(inset_ax,
                                        width="5%",  # width = 10% of parent_bbox width
                                        height="100%",  # height : 50%
                                        loc='lower left',
                                        bbox_to_anchor=(-0.1, 0., 1, 1),
                                        bbox_transform=inset_ax.transAxes,
                                        borderpad=0,
                                        )
            cbar = colorbar(im, cax=cbar_ax, ticks=[1000, 5000])
            cbar.ax.tick_params(labelsize=6, colors='black')
            cbar.ax.yaxis.set_ticks_position('left')

    @property
    def _blue_slice_of_interest(self):
        # Showing only relevant part of blue
        # middle of beam
        row_max, col_max = np.unravel_index(np.argmax(self.blue_orig, axis=None), self.blue_orig.shape)
        xrange_in_um = np.abs(self.red_X[-1] - self.red_X[0]) * 1000
        yrange_in_um = np.abs(self.red_Y[-1] - self.red_Y[0]) * 1000
        x_pixels_cam = xrange_in_um / 4.8
        y_pixels_cam = yrange_in_um / 4.8

        # This is the right size in mms, with same size as red.
        # Red features are twice as big so we half pixel number to have the same features in frame
        x_pixels_cam /= 2
        y_pixels_cam /= 2

        x_pixels_to_side = int(x_pixels_cam / 2)
        y_pixels_to_side = int(y_pixels_cam / 2)

        blue_slice_of_interest = np.index_exp[row_max - y_pixels_to_side:row_max + y_pixels_to_side,
                                 col_max - x_pixels_to_side:col_max + x_pixels_to_side]
        return blue_slice_of_interest

    def show_fig3v2(self):
        fig, axes = plt.subplots(1, 2, constrained_layout=True)

        # Fix RED_X, RED_Y so we'll be around (0, 0)
        RED_X = self.red_X - self.red_X.mean()
        RED_Y = self.red_Y - self.red_Y.mean()

        my_mesh(RED_X, RED_Y, self.red_speckles, axes[0], c_label='photon pairs/sec')
        axes[0].set_title(f'(a) Two-photon speckle')
        axes[0].set_xlabel("x (mm)")
        axes[0].set_ylabel("y (mm)")
        axes[0].invert_xaxis()


        # Showing only relevant part of blue
        # middle of beam
        row_max, col_max = np.unravel_index(np.argmax(self.blue_orig, axis=None), self.blue_orig.shape)
        xrange_in_um = np.abs(self.red_X[-1] - self.red_X[0]) * 1000
        yrange_in_um = np.abs(self.red_Y[-1] - self.red_Y[0]) * 1000
        x_pixels_cam = xrange_in_um / 4.8
        y_pixels_cam = yrange_in_um / 4.8

        # This is the right size in mms, with same size as red.
        # Red features are twice as big so we half pixel number to have the same features in frame
        x_pixels_cam /= 2
        y_pixels_cam /= 2

        x_pixels_to_side = int(x_pixels_cam / 2)
        y_pixels_to_side = int(y_pixels_cam / 2)

        blue_slice_of_interest = np.index_exp[row_max - y_pixels_to_side:row_max + y_pixels_to_side,
                                 col_max - x_pixels_to_side:col_max + x_pixels_to_side]

        # This works because of how my_mesh works, which actually cares only about dx, x[0], x[-1]
        # + I suffer from L.A.Z.Y
        BLUE_X = RED_X / 2
        BLUE_Y = RED_Y / 2

        blue_speckles = self.blue_speckles[blue_slice_of_interest]
        my_mesh(BLUE_X, BLUE_Y, blue_speckles, axes[1], c_label='arbitrary units')
        axes[1].set_title('(b) Pump speckle')
        axes[1].set_xlabel("x (mm)")
        axes[1].set_ylabel("y (mm)")

        fig.show()
        return fig

    def show_nano(self):

        fig, axes = plt.subplots(2, 1, constrained_layout=True)

        # Fix RED_X, RED_Y so we'll be around (0, 0)
        RED_X = self.red_X - self.red_X.mean()
        RED_Y = self.red_Y - self.red_Y.mean()

        # Show red
        my_mesh(RED_X, RED_Y, self.red_speckles, axes[0], c_label='photon pairs/sec')
        axes[0].set_title(f'Correlations Before Opt.')
        axes[0].set_xlabel("x (mm)")
        axes[0].set_ylabel("y (mm)")
        axes[0].invert_xaxis()


        my_mesh(RED_X, RED_Y, self.red_fixed, axes[1], c_label='photon pairs/sec')
        axes[1].set_title(f'Correlations After Opt.')
        axes[1].set_xlabel("x (mm)")
        axes[1].set_ylabel("y (mm)")
        axes[1].invert_xaxis()
        fig.show()

        return fig

    def show_fig5(self, insets=True):
        fig, axes = plt.subplots(1, 2, constrained_layout=True)

        # Fix RED_X, RED_Y so we'll be around (0, 0)
        RED_X = self.red_X - self.red_X.mean()
        RED_Y = self.red_Y - self.red_Y.mean()

        # Show red
        my_mesh(RED_X, RED_Y, self.red_second_mode, axes[0], c_label='photon pairs/sec')
        axes[0].set_title(f'(a) SPDC second mode')
        axes[0].set_xlabel("x (mm)")
        axes[0].set_ylabel("y (mm)")
        axes[0].invert_xaxis()

        if insets:
            from mpl_toolkits.axes_grid1.inset_locator import inset_axes
            from mpl_toolkits.axes_grid1.colorbar import colorbar

            inset_ax = axes[0].inset_axes((0, 0, 0.3, 0.3))
            second_mode_path = r'"C:\ronen\HUJI\Amirim\Paper\Data For Figures\figs_4_5_all_data\red_second_mode.scan"'
            r = ScanResult()
            r.loadfrom(second_mode_path)
            im = inset_ax.imshow(r.single2s, clim=(0, r.single2s.max()))
            inset_ax.set_axis_off()

            self._add_cbar_to_inset(inset_ax, im)

        # Showing only relevant part of blue
        # middle of beam
        row_max, col_max = np.unravel_index(np.argmax(self.blue_orig, axis=None), self.blue_orig.shape)
        xrange_in_um = np.abs(self.red_X[-1] - self.red_X[0]) * 1000
        yrange_in_um = np.abs(self.red_Y[-1] - self.red_Y[0]) * 1000
        x_pixels_cam = xrange_in_um / 4.8
        y_pixels_cam = yrange_in_um / 4.8

        # This is the right size in mms, with same size as red.
        # Red features are twice as big so we half pixel number to have the same features in frame
        x_pixels_cam /= 2
        y_pixels_cam /= 2

        x_pixels_to_side = int(x_pixels_cam / 2)
        y_pixels_to_side = int(y_pixels_cam / 2)

        blue_slice_of_interest = np.index_exp[row_max - y_pixels_to_side:row_max + y_pixels_to_side,
                                 col_max - x_pixels_to_side:col_max + x_pixels_to_side]

        # This works because of how my_mesh works, which actually cares only about dx, x[0], x[-1]
        # + I suffer from L.A.Z.Y
        BLUE_X = RED_X / 2
        BLUE_Y = RED_Y / 2

        blue_second_mode = self.blue_second_mode[blue_slice_of_interest]
        my_mesh(BLUE_X, BLUE_Y, blue_second_mode, axes[1], c_label='arbitrary units')
        axes[1].set_title('(b) Pump second mode')
        axes[1].set_xlabel("x (mm)")
        axes[1].set_ylabel("y (mm)")


        fig.show()
        return fig

    def show_second_mode(self):
        fig, axes = plt.subplots(1, 2)

        im = axes[0].imshow(self.blue_second_mode)
        fig.colorbar(im, ax=axes[0])
        axes[0].set_title('Blue Second Mode')

        my_mesh(self.red_X, self.red_Y, self.red_second_mode, axes[1])
        axes[1].invert_xaxis()
        axes[1].set_title(f'Red Second Mode')

        fig.show()

    def show_diffuser_fixer(self):
        fig, ax = plt.subplots(1, 2)
        im = ax[0].imshow(self.diffuser, cmap='gray')
        fig.colorbar(im, ax=ax[0])
        ax[0].set_title("Diffuser")

        im = ax[1].imshow(self.fixer, cmap='gray')
        fig.colorbar(im, ax=ax[1])
        ax[1].set_title("Fixer")

        fig.show()

    def show_diffuser(self):
        fig, ax = plt.subplots()

        pixel_size_x = 12.5e-3  # mm
        MAX_UM = pixel_size_x * 150
        X = np.linspace(-MAX_UM/2, MAX_UM/2)
        Y = X
        dx = (X[1] - X[0]) / 2
        dy = (Y[1] - Y[0]) / 2
        extent = (X[0]-dx, X[-1]+dx, Y[0]-dy, Y[-1]+dy)

        # 150 pixels, and picture is 150, 270 or something like that...
        d = 70
        im = ax.imshow(self.diffuser[d:d+150, :], cmap='gray', extent=extent)
        ax.set_xlabel("x (mm)")
        ax.set_ylabel("y (mm)")

        cbar = fig.colorbar(im, ax=ax)
        cbar.set_label("Phase (Rad)")
        ax.set_title("Kolmogorov Phase screen")
        fig.show()

        return fig

    def show_diffuser_plain(self):
        fig, ax = plt.subplots()

        d = 70
        im = ax.imshow(self.diffuser[d:d+150, :], cmap='gray')
        cbar = fig.colorbar(im, ax=ax)
        ax.set_axis_off()
        # cbar.set_label("Phase (Rad)")
        # ax.set_title("Kolmogorov Phase screen")
        # scalebar = ScaleBar(0.2)  # 1 pixel = 0.2 meter
        # ax.add_artist(scalebar)

        fig.show()

        return fig

    def saveto(self, path):
        try:
            f = open(path, 'wb')
            np.savez(f,
                     diffuser=self.diffuser,
                     fixer=self.fixer,

                     blue_orig=self.blue_orig,
                     blue_speckles=self.blue_speckles,
                     blue_fixed=self.blue_fixed,
                     blue_second_mode=self.blue_second_mode,

                     red_X=self.red_X,
                     red_Y=self.red_Y,
                     red_orig=self.red_orig,
                     red_speckles=self.red_speckles,
                     red_fixed=self.red_fixed,
                     red_second_mode=self.red_second_mode,
                     )
            f.close()
        except Exception as e:
            print("ERROR!!")
            print(e)
            traceback.print_exc()

    def loadfrom(self, path=None, remove_accidentals=True):
        if path is None:
            paths = glob.glob(f"{LOGS_DIR}\\*scan*.npz*")
            for i, path in enumerate(paths):
                print(f'{i}: {path}')
            choice = int(input('which one?'))
            path = paths[choice]
        elif path == 0:
            path = pyperclip.paste()

        path = path.strip('"')
        path = path.strip("'")

        f = open(path, 'rb')
        data = np.load(f, allow_pickle=True)
        self.diffuser = data['diffuser']
        self.fixer = data['fixer']

        self.blue_orig = data['blue_orig']
        self.blue_speckles = data['blue_speckles']
        self.blue_fixed = data['blue_fixed']

        self.red_X = data['red_X']
        self.red_Y = data['red_Y']
        self.red_orig = data['red_orig']
        self.red_speckles = data['red_speckles']
        self.red_fixed = data['red_fixed']

        if 'blue_second_mode' in data.keys():
            self.blue_second_mode = data['blue_second_mode']
            self.red_second_mode = data['red_second_mode']

        f.close()

        if remove_accidentals:

            before_path = r'"C:\ronen\HUJI\Amirim\Paper\Data For Figures\figs_4_5_all_data\red_before.scan"'
            if remove_accidentals == 'fig3':
                before_path = r'"C:\ronen\HUJI\Amirim\Paper\Data For Figures\fig_3_all_data\red_before.scan"'

            speckles_path = r'"C:\ronen\HUJI\Amirim\Paper\Data For Figures\figs_4_5_all_data\red_speckles.scan"'
            if remove_accidentals == 'fig3':
                speckles_path = r'"C:\ronen\HUJI\Amirim\Paper\Data For Figures\fig_3_all_data\red_speckles.scan"'

            after_path = r'"C:\ronen\HUJI\Amirim\Paper\Data For Figures\figs_4_5_all_data\red_fixed.scan"'

            second_mode_path = r'"C:\ronen\HUJI\Amirim\Paper\Data For Figures\figs_4_5_all_data\red_second_mode.scan"'


            self.red_orig -= self._get_accidentals(before_path)
            self.red_speckles -= self._get_accidentals(speckles_path)
            self.red_fixed -= self._get_accidentals(after_path)
            self.red_second_mode -= self._get_accidentals(second_mode_path)

    def _get_accidentals(self, scan_path):
        s = ScanResult()
        s.loadfrom(scan_path)
        accidentals = s.single1s * s.single2s * 2 * 4e-9  # ~0.14, 4e-9 is windows size of coincidence
        return accidentals


class OptimizerResult(object):
    def __init__(self, diffuser_phase_grid=None, original_speckle_pattern=None, best_result=None,
                 slm_phase_grid=None, powers=None, mid_results=None, original_good=None, opt_method=''):
        self.original_good = original_good
        self.diffuser_phase_grid = diffuser_phase_grid
        self.original_speckle_pattern = original_speckle_pattern
        self.best_result = best_result
        self.slm_phase_grid = slm_phase_grid
        self.powers = powers
        self.mid_results = mid_results
        self.opt_method = opt_method

    @property
    def enhacement(self):
        # Choose pixels using original focus
        print("## Relevant only for simulation!! ##")
        max_power = self.original_good.max()
        print('max_power:', max_power)
        limit_power = max_power / 4
        print('limit_power:', limit_power)
        relevant_pixels = self.original_good > limit_power
        original_tot_power = np.sum(self.original_good[relevant_pixels])
        print("original_tot_power", original_tot_power)

        if False:
            fig, ax = plt.subplots()
            ax.set_title("Relevant pixels")
            ax.imshow(relevant_pixels)
            fig.show()

        # Sum powers after fixing
        relevant_powers = self.best_result[relevant_pixels]
        # print("relevant_powers", relevant_powers)
        power_after_fix = np.sum(relevant_powers)
        print("power_after_fix", power_after_fix)

        # estimate random speckle intensity
        relevant_powers = self.original_speckle_pattern[relevant_pixels]
        # print("relevant_powers", relevant_powers)
        power_speckles = np.sum(relevant_powers)
        print("power_before_fix", power_speckles)

        enhancement = power_after_fix / power_speckles
        print('enhancement', enhancement)

        efficiency1 = power_after_fix / original_tot_power
        efficiency2 = (power_after_fix - power_speckles) / (original_tot_power - power_speckles)
        print("efficiency1", efficiency1)
        print("efficiency2", efficiency2)

        return enhancement

    def show_all(self):
        self.show_diffuser()
        self.show_orig()
        self.show()

    def show_diffuser(self):
        show_phase_mask(self.diffuser_phase_grid, 'phase grid on diffuser')

    def show_orig(self):
        fig, ax = plt.subplots()
        ax.imshow(self.original_good)
        ax.set_title('Before diffuser')
        fig.show()
        fig.canvas.flush_events()

    def show(self):
        fig, axes = plt.subplots(2, 2)
        fig.suptitle(f'Results Of {self.opt_method} Optimization')

        axes[0, 0].imshow(self.original_speckle_pattern)
        axes[0, 0].set_title('Original Speckle Pattern')

        axes[0, 1].imshow(self.best_result)
        axes[0, 1].set_title('Best Result')

        axes[1, 0].imshow(self.slm_phase_grid, cmap='gray')
        axes[1, 0].set_title('final phase grid on SLM')

        axes[1, 1].plot(self.powers)
        axes[1, 1].set_title("Power over Iterations")

        fig.show()
        fig.canvas.flush_events()

    def show_optimization_review(self):
        fig, ax = plt.subplots()
        ax.plot(self.powers)
        ax.set_xlabel("Iterations")
        ax.set_ylabel("Power (Arb. units)")
        return fig, ax

    @property
    def best_slm_phase_grid(self):
        best_mid_phase, best_mid_power = max(self.mid_results.values(), key=lambda x: x[1])
        if best_mid_power > self.powers[-1]:
            return best_mid_phase
        return self.slm_phase_grid

    def saveto(self, path):
        try:
            f = open(path, 'wb')
            np.savez(f,
                     diffuser_phase_grid=self.diffuser_phase_grid,
                     original_speckle_pattern=self.original_speckle_pattern,
                     best_result=self.best_result,
                     slm_phase_grid=self.slm_phase_grid,
                     powers=self.powers,
                     mid_results=self.mid_results,
                     original_good=self.original_good,
                     opt_method=self.opt_method)
            f.close()
        except Exception as e:
            print("ERROR!!")
            print(e)
            traceback.print_exc()

    def loadfrom(self, path=None):
        if path is None:
            paths = glob.glob(f"{LOGS_DIR}\\*scan*.npz*")
            for i, path in enumerate(paths):
                print(f'{i}: {path}')
            choice = int(input('which one?'))
            path = paths[choice]
        elif path == 0:
            path = pyperclip.paste()

        path = path.strip('"')
        path = path.strip("'")

        f = open(path, 'rb')
        data = np.load(f, allow_pickle=True)

        self.diffuser_phase_grid = data['diffuser_phase_grid']
        self.original_speckle_pattern = data['original_speckle_pattern']
        self.best_result = data['best_result']
        self.slm_phase_grid = data['slm_phase_grid']
        self.powers = data['powers']
        try:
            self.mid_results = data['mid_results'].all()
            self.original_good = data['original_good']
            self.opt_method = data['opt_method']
        except Exception:
            print('old optimization')
        f.close()


class ScanResult(object):
    def __init__(self, coincidences=None, single1s=None, single2s=None, X=None, Y=None, integration_time=None):
        self.coincidences = coincidences
        self.single1s = single1s
        self.single2s = single2s
        self.X = X
        self.Y = Y
        self.integration_time = integration_time

    def show(self, show_singles=False, title='') -> typing.Tuple[matplotlib.figure.Figure, matplotlib.axes.Axes]:
        fig, ax = plt.subplots()
        ax.set_title(f'Coincidences {title}')
        my_mesh(self.X, self.Y, self.coincidences, ax)
        ax.invert_xaxis()
        fig.show()

        if show_singles:
            fig, axes = plt.subplots(1, 2)
            my_mesh(self.X, self.Y, self.single1s, axes[0])
            my_mesh(self.X, self.Y, self.single2s, axes[1])
            # TODO RSS: make sure
            axes[0].invert_xaxis()
            axes[1].invert_xaxis()
            axes[0].set_title(f'Single counts 1 {title}')
            axes[1].set_title(f'Single counts 2 {title}')
            fig.show()
        return fig, ax

    def saveto(self, path):
        try:
            f = open(path, 'wb')
            np.savez(f,
                     coincidences=self.coincidences,
                     single1s=self.single1s,
                     single2s=self.single2s,
                     X=self.X,
                     Y=self.Y)
            f.close()
        except Exception as e:
            print("ERROR!!")
            print(e)
            traceback.print_exc()

    def loadfrom(self, path=None, BAK=True):
        if path is None:
            BAK = '' if not BAK else '.BAK'
            paths = glob.glob(f"{LOGS_DIR}\\*scan*.npz*")
            for i, path in enumerate(paths):
                print(f'{i}: {path}')
            choice = int(input('which one?'))
            path = paths[choice]
        elif path == 0:
            path = pyperclip.paste()

        path = path.strip('"')
        path = path.strip("'")

        f = open(path, 'rb')
        data = np.load(f, allow_pickle=True)
        self.coincidences = data['coincidences']
        self.single1s = data['single1s']
        self.single2s = data['single2s']
        self.X = data['X']
        self.Y = data['Y']

        f.close()


class RedOptimizerResult(object):
    def __init__(self, blue_powers=None, red_powers=None):
        self.blue_powers = blue_powers  # List of powers
        self.red_powers = red_powers  # List of tuples (index, power)

    def show(self):
        x_blue = np.arange(1, len(self.blue_powers) + 1)
        fig, ax = plt.subplots()
        ax.set_title('optimization of red and blue')
        ax.plot(x_blue, self.blue_powers, color='blue')
        ax.set_xlabel('iterations')
        ax.set_ylabel('blue power')
        ax2 = ax.twinx()
        ax2.plot(self.red_powers[:, 0], self.red_powers[:, 1], color='red', marker='*')
        ax2.set_ylabel('red power')
        fig.show()

    def saveto(self, path):
        f = open(path, 'wb')
        np.savez(f,
                 blue_powers=self.blue_powers,
                 red_powers=self.red_powers)
        f.close()

    def loadfrom(self, path=None):
        if path is None:
            paths = glob.glob(f"{LOGS_DIR}\\*red_blue*.npz*")
            for i, path in enumerate(paths):
                print(f'{i}: {path}')
            choice = int(input('which one?'))
            path = paths[choice]

        f = open(path, 'rb')
        data = np.load(f, allow_pickle=True)
        self.red_powers = data['red_powers']
        self.blue_powers = data['blue_powers']
        f.close()


class DynamicDiffuserResult(object):
    def __init__(self, N1=None, N2=None, N3=None, red_powers=None, blue_powers=None):
        self.N1 = N1
        self.N2 = N2
        self.N3 = N3
        self.red_powers = red_powers
        self.blue_powers = blue_powers

    @property
    def enhancement(self):
        print("\n#### SPDC ####")
        red_powers = np.array(self.red_powers)
        powers = red_powers[:, 1]

        conv_rect = 30
        rect = np.ones(conv_rect)
        powers = np.convolve(powers, rect, mode='full') / rect.size
        powers = powers[:-conv_rect+1]

        red_times = red_powers[:, 0] / 3600
        indexes_with_opt = np.logical_and(red_times > 0.33, red_times < 3.5)
        indexes_no_opt = np.logical_and(red_times > 4.3, red_times < 7)
        powers_with_opt = powers[indexes_with_opt]
        powers_no_opt = powers[indexes_no_opt]

        mean_power_with_opt = powers_with_opt.mean()
        mean_power_no_opt = powers_no_opt.mean()
        print('mean_power_with_opt', mean_power_with_opt)
        print('mean_power_no_opt', mean_power_no_opt)

        enhancement = mean_power_with_opt / mean_power_no_opt
        print("enhancement", enhancement)
        return enhancement

    @property
    def pump_enhancement(self):
        print("\n#### PUMP ####")
        blue_powers = np.array(self.blue_powers)
        powers = blue_powers[:, 1]

        conv_rect = 30
        rect = np.ones(conv_rect)
        powers = np.convolve(powers, rect, mode='full') / rect.size
        powers = powers[:-conv_rect + 1]

        blue_times = blue_powers[:, 0] / 3600
        indexes_with_opt = np.logical_and(blue_times > 0.33, blue_times < 3.5)
        indexes_no_opt = np.logical_and(blue_times > 4.3, blue_times < 7)
        powers_with_opt = powers[indexes_with_opt]
        powers_no_opt = powers[indexes_no_opt]

        mean_power_with_opt = powers_with_opt.mean()
        mean_power_no_opt = powers_no_opt.mean()
        print('pump mean_power_with_opt', mean_power_with_opt)
        print('pump mean_power_no_opt', mean_power_no_opt)

        enhancement = mean_power_with_opt / mean_power_no_opt
        print("enhancement", enhancement)
        return enhancement

    def show(self, title=''):
        fig, ax = plt.subplots()
        ax.set_title(f'Power at focus {title}')

        red_powers = np.array(self.red_powers)
        blue_powers = np.array(self.blue_powers)

        if red_powers.ndim == 1:
            ax.plot(self.red_powers, 'r-')
        elif red_powers.ndim == 2:
            ax.plot(self.red_powers[:, 0], self.red_powers[:, 1], 'r-')
        else:
            raise Exception("how many dims are there??")

        ax.axvline(x=self.N1, linestyle='--', color='g')
        ax.axvline(x=self.N2, linestyle='--', color='g')

        ax2 = ax.twinx()
        if blue_powers.ndim == 1:
            ax2.plot(blue_powers, 'b-')
        elif blue_powers.ndim == 2:
            ax2.plot(blue_powers[:, 0], blue_powers[:, 1], 'b-')
        else:
            raise Exception("how many dims are there??")

        fig.show()

        return fig, ax

    def show_fig6_new(self, conv_rect=30):
        fig, ax = plt.subplots()

        red_powers = np.array(self.red_powers)
        blue_powers = np.array(self.blue_powers)

        red_times = red_powers[:, 0] / 3600
        powers = red_powers[:, 1]

        red_index_mask = red_times > (self.N1 / 3600)
        powers = powers[red_index_mask]
        red_times = red_times[red_index_mask]

        if conv_rect:
            rect = np.ones(conv_rect)
            powers = np.convolve(powers, rect, mode='full') / rect.size
            powers = powers[:-conv_rect+1]
        lns1 = ax.plot(red_times, powers, 'r-', label='coincidence rate')

        ax.axvline(x=self.N1 / 3600, linestyle='--', color='g')
        ax.axvline(x=self.N2 / 3600, linestyle='--', color='g')

        ax2 = ax.twinx()
        blue_times = blue_powers[:, 0] / 3600
        powers = blue_powers[:, 1]

        blue_index_mask = blue_times > (self.N1 / 3600)
        powers = powers[blue_index_mask]
        blue_times = blue_times[blue_index_mask]

        if conv_rect:
            rect = np.ones(conv_rect)
            powers = np.convolve(powers, rect, mode='full') / rect.size
            powers = powers[:-conv_rect+1]
        lns2 = ax2.plot(blue_times, powers, 'b-', label='pump intensity')

        if conv_rect:
            lns = lns1 + lns2
            labs = [l.get_label() for l in lns]
            ax.legend(lns, labs, loc=0)

        ax.set_xlabel('time (hr)')
        ax.set_ylabel('Coincidence rate (pairs/sec)')
        ax2.set_ylabel('pump intensity (arb. units)')

        fig.show()

        return fig, ax


    def show_fig6(self, conv_rect=30):
        fig, ax = plt.subplots()

        red_powers = np.array(self.red_powers)
        blue_powers = np.array(self.blue_powers)

        if red_powers.ndim == 1:
            ax.plot(self.red_powers, 'r-')
        elif red_powers.ndim == 2:
            red_times = red_powers[:, 0] / 3600
            powers = red_powers[:, 1]
            if conv_rect:
                rect = np.ones(conv_rect)
                powers = np.convolve(powers, rect, mode='full') / rect.size
                powers = powers[:-conv_rect + 1]
            lns1 = ax.plot(red_times, powers, 'r-', label='coincidence rate')

        else:
            raise Exception("how many dims are there??")

        ax.axvline(x=self.N1 / 3600, linestyle='--', color='g')
        ax.axvline(x=self.N2 / 3600, linestyle='--', color='g')

        ax2 = ax.twinx()
        if blue_powers.ndim == 1:
            ax2.plot(blue_powers, 'b-')
        elif blue_powers.ndim == 2:
            blue_times = blue_powers[:, 0] / 3600
            powers = blue_powers[:, 1]
            if conv_rect:
                rect = np.ones(conv_rect)
                powers = np.convolve(powers, rect, mode='full') / rect.size
                powers = powers[:-conv_rect + 1]
            lns2 = ax2.plot(blue_times, powers, 'b-', label='pump intensity')
        else:
            raise Exception("how many dims are there??")

        if conv_rect:
            lns = lns1 + lns2
            labs = [l.get_label() for l in lns]
            ax.legend(lns, labs, loc=0)

        ax.set_xlabel('time (hr)')
        ax.set_ylabel('Coincidence rate (pairs/sec)')
        ax2.set_ylabel('pump intensity (arb. units)')

        fig.show()

        return fig, ax

    def saveto(self, path):
        try:
            f = open(path, 'wb')
            np.savez(f,
                     N1=self.N1,
                     N2=self.N2,
                     N3=self.N3,
                     red_powers=self.red_powers,
                     blue_powers=self.blue_powers)
            f.close()
        except Exception as e:
            print("ERROR!!")
            print(e)
            traceback.print_exc()

    def loadfrom(self, path=None, BAK=True, remove_accidentals=True):
        if path is None:
            BAK = '' if not BAK else '.BAK'
            paths = glob.glob(f"{LOGS_DIR}\\*scan*.npz*")
            for i, path in enumerate(paths):
                print(f'{i}: {path}')
            choice = int(input('which one?'))
            path = paths[choice]
        elif path == 0:
            path = pyperclip.paste()

        path = path.strip('"')
        path = path.strip("'")

        f = open(path, 'rb')
        data = np.load(f, allow_pickle=True)
        self.N1 = data['N1']
        self.N2 = data['N2']
        self.N3 = data['N3']
        self.red_powers = data['red_powers']
        self.blue_powers = data['blue_powers']

        f.close()

        if remove_accidentals:
            accidentals = 3800 * 4500 * 4e-9 * 2  # = 0.14  # singles1 * singles2 * window size * 2
            self.red_powers[:, 1] -= accidentals


def show_phase_mask(A, title='diffuser'):
    fig, ax = plt.subplots()
    im = ax.imshow(A, cmap='gray')
    fig.colorbar(im, ax=ax)
    fig.suptitle(title)
    fig.show()


def show_image(path):
    im = np.load(path)['image']
    fig, axes = plt.subplots()
    im = axes.imshow(im)
    fig.colorbar(im, ax=axes)
    fig.show()
    return fig, axes
