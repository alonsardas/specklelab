"""
Tests that the expansion of a gaussian in free space is as expected,

See
https://www.edmundoptics.com/knowledge-center/application-notes/lasers/gaussian-beam-propagation/
"""
import matplotlib.axes
import numpy as np
from matplotlib import pyplot as plt
from uncertainties import unumpy as unp

from opticalsimulator import BasicCamera, Borders, GaussianLightSource, AngularMomentumPropagator, \
    OpticalSystem, ZScanner
from opticalsimulator.simulations.alon.randomwalks import fitter


def test_gaussian_propagation():
    fig, axes = plt.subplots(1, 2)
    input_ax, output_ax = axes
    input_camera = BasicCamera('input', input_ax)
    output_camera = BasicCamera('output', output_ax)
    assert isinstance(input_ax, matplotlib.axes.Axes)
    assert isinstance(output_ax, matplotlib.axes.Axes)

    world_size = 50
    world_borders = Borders(-world_size / 2, -world_size / 2, world_size / 2, world_size / 2)
    world_x_pixels, world_y_pixels = (2000, 200)

    gaussian_sigma = 1
    # gaussian_sigma = 6.1268634458
    wavelength = 633e-6
    light_source = GaussianLightSource(0, 0, gaussian_sigma, wavelength=wavelength)

    distance = 6e4

    use_z_scanner = False
    if use_z_scanner:
        fig, ax = plt.subplots()
        propgagator = ZScanner(distance, 200, ax)

    else:
        propgagator = AngularMomentumPropagator(distance)

    simulator = OpticalSystem(world_borders, world_x_pixels, world_y_pixels, light_source)

    simulator.add_device(input_camera)
    simulator.add_device(propgagator)
    simulator.add_device(output_camera)

    simulator.simulate()

    center_row = int(world_y_pixels / 2)
    intensity = input_camera.get_image()
    input_gaussian = intensity[center_row, :]
    intensity = output_camera.get_image()
    output_gaussian = intensity[center_row, :]

    xs = np.linspace(world_borders.min_x, world_borders.max_x, world_x_pixels)

    def gaussian_func(xs, A, sigma): return A * np.exp(-2 * xs ** 2 / (sigma ** 2))

    params = fitter.FitParams(gaussian_func, xs, input_gaussian)
    input_fit = fitter.FuncFit(params)
    input_fit.print_results()
    input_sigma = input_fit.fit_results[1]

    params = fitter.FitParams(gaussian_func, xs, output_gaussian)
    output_fit = fitter.FuncFit(params)
    output_fit.print_results()
    output_sigma = output_fit.fit_results[1]

    fig, ax = plt.subplots()
    output_fit.plot_data(ax)
    output_fit.plot_fit(ax)

    def get_w_by_z(w0, wavelength, z):
        return w0 * unp.sqrt(1 + (wavelength * z / (np.pi * w0 ** 2)) ** 2)

    print(f'expected={get_w_by_z(w0=input_sigma, wavelength=wavelength, z=distance)}')


def main():
    test_gaussian_propagation()
    plt.show()


if __name__ == '__main__':
    main()
