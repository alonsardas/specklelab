import logging
import os
import time

from opticalsimulator.simulations.alon import runutils

logger = logging.getLogger('lab')


def test_logger():
    runutils.initialize_logger(os.path.abspath(r'/home/alon/temp'))
    logger.debug('debug message')
    logger.info('info message')
    time.sleep(20)
    logger.info('done')
    logging.shutdown()
    time.sleep(20)
