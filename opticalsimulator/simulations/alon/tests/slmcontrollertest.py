import matplotlib.axes
import numpy as np
from matplotlib import pyplot as plt
from uncertainties import unumpy as unp

from opticalsimulator import Borders, BasicCamera, GaussianLightSource, get_borders_around_center, OpticalSystem, \
    FourierLens
from opticalsimulator.simulations.alon.randomwalks import fitter
from opticalsimulator.simulations.alon.slm.lcosdevice import LCoSSLMDevice
from opticalsimulator.simulations.alon.slm.monitorimagewriter import MonitorImageWriter
from opticalsimulator.simulations.alon.slm.simulationdevice import SimulationSLMDevice
from opticalsimulator.simulations.alon.slm.sizeadapterdevice import SizeAdapterDevice
from opticalsimulator.simulations.alon.slm.slmcontroller import SLMController


def test_angular_spectrum_method():
    pixel_size = 12.5e-3
    pixels_x = 800
    pixels_y = 600
    # outer_device = MonitorSLMDevice(800, 600, pixel_size, pixel_size, 100, 40, 400, 'slm1',
    #                                 allow_wavelength_overflow=False)
    outer_device = SimulationSLMDevice(Borders(0, 0, pixels_x * pixel_size, pixels_y * pixel_size), pixels_x, pixels_y)

    device = SizeAdapterDevice(outer_device, Borders(10, 20, 700, 500))
    controller = SLMController(device)
    controller.put_angular_spectrum_method(25, 633e-6, 300)
    controller.plot_phase_mask()


def test_propagation_with_gaussian():
    fig, axes = plt.subplots(1, 2)
    input_ax, output_ax = axes
    input_camera = BasicCamera('input', input_ax)
    output_camera = BasicCamera('output', output_ax)
    assert isinstance(input_ax, matplotlib.axes.Axes)
    assert isinstance(output_ax, matplotlib.axes.Axes)

    world_size = 50
    world_borders = Borders(-world_size / 2, -world_size / 2, world_size / 2, world_size / 2)
    world_x_pixels, world_y_pixels = (2000, 200)

    gaussian_sigma = 1
    # gaussian_sigma = 6.1268634458
    wavelength = 633e-6
    light_source = GaussianLightSource(0, 0, gaussian_sigma, wavelength=wavelength)

    distance = 6e4

    f = 2000
    lens1 = FourierLens(f)

    propagator_slm = SimulationSLMDevice(get_borders_around_center(0, 0, gaussian_sigma * 3, gaussian_sigma * 3), 4000,
                                         600)
    controller = SLMController(propagator_slm)
    controller.put_angular_spectrum_method(distance, wavelength, f)
    controller.plot_phase_mask()

    lens2 = FourierLens(f)

    simulator = OpticalSystem(world_borders, world_x_pixels, world_y_pixels, light_source)

    simulator.add_device(input_camera)
    simulator.add_device(lens1)
    simulator.add_device(propagator_slm)
    simulator.add_device(lens2)
    simulator.add_device(output_camera)

    simulator.simulate()

    center_row = int(world_y_pixels / 2)
    intensity = input_camera.get_image()
    input_gaussian = intensity[center_row, :]
    intensity = output_camera.get_image()
    output_gaussian = intensity[center_row, :]

    xs = np.linspace(world_borders.min_x, world_borders.max_x, world_x_pixels)

    def gaussian_func(xs, A, sigma):
        return A * np.exp(-2 * xs ** 2 / (sigma ** 2))

    params = fitter.FitParams(gaussian_func, xs, input_gaussian)
    input_fit = fitter.FuncFit(params)
    input_fit.print_results()
    input_sigma = input_fit.fit_results[1]

    params = fitter.FitParams(gaussian_func, xs, output_gaussian)
    output_fit = fitter.FuncFit(params)
    output_fit.print_results()
    output_sigma = output_fit.fit_results[1]

    fig, ax = plt.subplots()
    output_fit.plot_data(ax)
    output_fit.plot_fit(ax)

    def get_w_by_z(w0, wavelength, z):
        return w0 * unp.sqrt(1 + (wavelength * z / (np.pi * w0 ** 2)) ** 2)

    print(f'expected={get_w_by_z(w0=input_sigma, wavelength=wavelength, z=distance)}')


def test_plot_phases():
    pixel_size = 12.5e-3
    image_writer = MonitorImageWriter(800, 600, 40, 400, 'slm1')
    outer_device = LCoSSLMDevice(image_writer, pixel_size, pixel_size, 100,
                                 allow_wavelength_overflow=False)

    # device.phase_grid = (np.pi * 2) / 10 * device.x_pixel_indexes
    device = SizeAdapterDevice(outer_device, Borders(10, 20, 700, 500))
    controller = SLMController(device)
    controller.plot_phase_mask()


def test_resize():
    pixel_size = 12.5e-3

    image_writer = MonitorImageWriter(800, 600, 40, 400, 'slm1')
    outer_device = LCoSSLMDevice(image_writer, pixel_size, pixel_size, 100,
                                 allow_wavelength_overflow=False)

    device = SizeAdapterDevice(outer_device, Borders(10, 20, 700, 500))
    controller = SLMController(device)
    controller.resize_and_put(np.random.random((5, 3)) * 2 * np.pi)


def main():
    # test_angular_spectrum_method()
    test_propagation_with_gaussian()
    # test_plot_phases()
    # test_resize()
    plt.show()


if __name__ == '__main__':
    main()
