import numpy as np
from matplotlib import pyplot as plt

from opticalsimulator import Borders
from opticalsimulator.simulations.alon.slm.lcosdevice import LCoSSLMDevice
from opticalsimulator.simulations.alon.slm.monitorimagewriter import MonitorImageWriter
from opticalsimulator.simulations.alon.slm.sizeadapterdevice import SizeAdapterDevice


def test_monitor_device():
    image_writer = MonitorImageWriter(200, 400, 40, 400, 'slm1')
    device = LCoSSLMDevice(image_writer, 3, 3, 100,
                           allow_wavelength_overflow=False)
    device.phase_grid = (np.pi * 2) / 10 * device.x_pixel_indexes
    device.update()


def test_size_adapter_same_size():
    image_writer = MonitorImageWriter(800, 600, 40, 400, 'slm1')
    outer_device = LCoSSLMDevice(image_writer, 3, 3, 100,
                                 allow_wavelength_overflow=False)

    # device.phase_grid = (np.pi * 2) / 10 * device.x_pixel_indexes
    device = SizeAdapterDevice(outer_device, Borders(10, 20, 200, 300))
    device.phase_grid = (np.pi * 2) / 10 * device.y_pixel_indexes
    device.update()


def test_size_adapter_macro_pixels():
    image_writer = MonitorImageWriter(800, 600, 40, 400, 'slm1')
    outer_device = LCoSSLMDevice(image_writer, 3, 3, 100,
                                 allow_wavelength_overflow=False)
    # device.phase_grid = (np.pi * 2) / 10 * device.x_pixel_indexes
    device = SizeAdapterDevice(outer_device, Borders(10, 20, 10 + 200, 20 + 300), 30, 10)
    device.phase_grid = (np.pi * 2) / 10 * device.x_pixel_indexes
    device.update()


def test_close():
    image_writer = MonitorImageWriter(200, 400, 40, 400, 'slm1')
    device = LCoSSLMDevice(image_writer, 3, 3, 100,
                           allow_wavelength_overflow=False)
    device.phase_grid = (np.pi * 2) / 10 * device.x_pixel_indexes
    device.update()
    plt.pause(1)
    image_writer.close()


def test_wavelength_overflow():
    image_writer1 = MonitorImageWriter(800, 600, 40, 400, 'slm1 - NO overflow')
    image_writer2 = MonitorImageWriter(800, 600, 40, 400, 'slm2 - WITH overflow')
    device1 = LCoSSLMDevice(image_writer1, 3, 3, 100)
    device2 = LCoSSLMDevice(image_writer2, 3, 3, 100, allow_wavelength_overflow=True)
    device1.phase_grid = (np.pi * 2) / 10 * device1.x_pixel_indexes
    device2.phase_grid = (np.pi * 2) / 10 * device2.x_pixel_indexes
    device1.update()
    device2.update()


def main():
    # mpl.use('TkAgg')
    # test_monitor_device()
    # test_size_adapter_same_size()
    # test_size_adapter_macro_pixels()
    # test_close()
    test_wavelength_overflow()
    plt.show()


if __name__ == '__main__':
    main()
