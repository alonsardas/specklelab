from matplotlib import pyplot as plt

from opticalsimulator.elements import * 
from opticalsimulator.simulations.alon.oldslmdevice import SpeckleSLMDevice

def show_speckles():
    fig, axes = plt.subplots(1, 2)

    borders = Borders(-1, -1, 1, 1)
    x_pixels = 1000
    y_pixels = 1000

    # light_source = CircularLightSource()
    light_source = GaussianLightSource(sigma=0.05)
    input_camera = BasicCamera('input', axes[0])
    slm_device = SpeckleSLMDevice(borders, 0.005)
    fourier_lens = FourierLens()
    output_camera = BasicCamera('output', axes[1])

    simulator = OpticalSystem(borders, x_pixels, y_pixels, light_source)
    simulator.add_device(input_camera)
    simulator.add_device(slm_device)
    simulator.add_device(fourier_lens)
    simulator.add_device(output_camera)
    simulator.simulate()

    plt.show()


def main():
    show_speckles()


if __name__ == '__main__':
    main()
