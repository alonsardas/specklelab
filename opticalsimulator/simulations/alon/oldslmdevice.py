import numpy as np

from opticalsimulator.elements.general import Borders, FieldGrid, OpticalDevice


class SLMDevice(OpticalDevice):
    def prop_through(self, f: FieldGrid):
        phases = self.get_phases(f.Xs, f.Ys)
        f.Es *= np.exp(1j * phases)

    def get_phases(self, xs, ys):
        raise NotImplementedError


class MirrorSLMDevice(SLMDevice):
    def __init__(self, phase_inc=0, angle=0):
        super().__init__()
        self.phase_inc = phase_inc
        self.angle = angle

    def get_phases(self, xs, ys):
        return xs * self.phase_inc * np.cos(self.angle) + ys * self.phase_inc * np.sin(self.angle)


class EmptySLMDevice(SLMDevice):
    def get_phases(self, xs, ys):
        return 0


class SpeckleSLMDevice(SLMDevice):
    def __init__(self, borders: Borders, speckle_size):
        self.borders = borders
        self.speckle_size = speckle_size
        x_speckles = int((self.borders.max_x - self.borders.min_x) / speckle_size) + 1
        y_speckles = int((self.borders.max_y - self.borders.min_y) / speckle_size) + 1
        self.speckles_phase_map = np.random.random((y_speckles, x_speckles)) * 2 * np.pi

    def get_phases(self, xs, ys):
        x_indexes = xs / self.speckle_size
        y_indexes = ys / self.speckle_size
        x_indexes: np.ndarray = x_indexes.astype('int')
        y_indexes: np.ndarray = y_indexes.astype('int')
        return self.speckles_phase_map[x_indexes, y_indexes]


class PhaseFuncSLMDevice(SLMDevice):
    def __init__(self, phase_func):
        super().__init__()
        self.phase_func = phase_func

    def get_phases(self, xs, ys):
        return self.phase_func(xs, ys)
