"""
Using Angular Spectrum to simulate propagation in the lab, in order to verify
that the camera is in focus
"""
import matplotlib.axes
import numpy as np
from matplotlib import pyplot as plt

from opticalsimulator import VimbaCamera
from opticalsimulator.simulations.alon.slm.labslm import get_slm_device
from opticalsimulator.simulations.alon.slm.slmcontroller import SLMController


def check_focus():
    zs = np.arange(-15, 15, 1)
    max_intensity = np.zeros(len(zs))
    exposure_time = 100

    wavelength = 400e-6
    f = 300

    image_writer, slm, grating, inner_slm = get_slm_device(1)
    controller = SLMController(inner_slm)

    with VimbaCamera(0, exposure_time) as cam:
        for i, z in enumerate(zs):
            controller.put_angular_spectrum_method(z, wavelength, f)
            plt.pause(0.2)
            image = cam.get_image()
            max_intensity[i] = image.max()

    fig, ax = plt.subplots()
    assert isinstance(ax, matplotlib.axes.Axes)
    ax.plot(zs, max_intensity)
    ax.set_xlabel('z - propagation distance')
    ax.set_ylabel('max intensity in the camera')


def main():
    check_focus()
    plt.show()


if __name__ == '__main__':
    main()
