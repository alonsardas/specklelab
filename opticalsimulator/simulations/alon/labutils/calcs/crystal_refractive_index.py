import numpy as np
from matplotlib import pyplot as plt


def calc_ny(wavelength):
    # Based on https://www.unitedcrystals.com/KTPProp.html
    # or here http://www.bblaser.com/pdf/KTP.pdf
    ny_2 = 3.0333 + 0.04154 / (wavelength ** 2 - 0.04547) - 0.01408 * wavelength ** 2
    return np.sqrt(ny_2)


def main():
    base_wavelength = 800e-9
    Dl = 80e-9
    ls = np.linspace(base_wavelength - Dl, base_wavelength + Dl, 100)
    nys = calc_ny(ls * 1e3)  # Equation is given by lambda in micro meter

    fig, ax = plt.subplots()
    ax.plot(ls, nys)

    plt.show()


if __name__ == '__main__':
    main()
