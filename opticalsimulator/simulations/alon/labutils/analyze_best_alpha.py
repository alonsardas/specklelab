import glob
import os

from opticalsimulator.results import ScanResult

RESULTS_BASE_DIR = r'/media/Windows/Users/Alon/Documents/Uni/Amirim/Results'
dir1 = '2020-11-07_18-19-38'
dir2 = '2020-11-07_21-22-15'


def save_images(results_dir):
    for name in glob.glob(os.path.join(RESULTS_BASE_DIR, results_dir, '*.scan')):
        scan = ScanResult()
        scan.loadfrom(name)
        fig, ax = scan.show()
        fig.savefig(name[:-5] + '.png')


if __name__ == '__main__':
    save_images(dir2)
