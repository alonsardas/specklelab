import numpy as np
from matplotlib import pyplot as plt

from opticalsimulator.simulations.alon.slm.labslm import get_slm_device
from opticalsimulator.simulations.alon.slm.slmcontroller import SLMController

_, slm1, _, inner_slm1 = get_slm_device(1)
_, slm2, _, inner_slm2 = get_slm_device(2)
controller1, inner_controller1 = SLMController(slm1), SLMController(inner_slm1)
controller2, inner_controller2 = SLMController(slm2), SLMController(inner_slm2)


def main():
    diffuser = np.random.random((4, 3)) * 2 * np.pi
    inner_controller1.resize_and_put(diffuser)

    fixer = -diffuser[:, ::-1]
    inner_controller2.resize_and_put(fixer)


if __name__ == '__main__':
    main()
    plt.show()
