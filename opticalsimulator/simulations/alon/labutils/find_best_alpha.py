import logging

import numpy as np

from opticalsimulator.simulations.alon import runutils
from opticalsimulator.simulations.alon.photonscanner import PhotonScanner
from opticalsimulator.simulations.alon.slm.labslm import get_slm_device
from opticalsimulator.simulations.alon.slm.slmcontroller import SLMController


logger = logging.getLogger('lab')


def main():
    print('Starting script')
    scan_alphas_pi_step()


def scan_alphas_pi_step():
    best_x = 5.85
    best_y = 9.75

    x_pixels, y_pixels = 9, 10
    pixel_size_x, pixel_size_y = 0.05, 0.025

    integration_time = 10

    alphas = np.arange(130, 150, 5)

    single_scan_time = x_pixels * y_pixels * integration_time
    total_seconds = single_scan_time * len(alphas)
    runutils.show_running_time_message(total_seconds)

    results_dir = runutils.create_results_folder()
    runutils.initialize_logger(results_dir)

    # TODO: Consider using mirroring, to reduce DC

    logger.info(f'Scanning alphas {alphas}')

    logger.debug('Initializing SLMs')
    _, slm1, _, inner_slm1 = get_slm_device(1)
    _, slm2, _, inner_slm2 = get_slm_device(3)
    controller1, inner_controller1 = SLMController(slm1), SLMController(inner_slm1)
    controller2, inner_controller2 = SLMController(slm2), SLMController(inner_slm2)

    controller1.normal()

    start_x = best_x - pixel_size_x * x_pixels / 2
    start_y = best_y - pixel_size_y * y_pixels / 2

    # inner_controller2.pi_step_x(75)
    inner_controller2.pi_step_y(140)

    try:
        for alpha in alphas:
            alpha = int(alpha)
            slm2.alpha = alpha
            slm2.update()
            scanner = PhotonScanner(integration_time, start_x, start_y, x_pixels, y_pixels, pixel_size_x, pixel_size_y,
                                    run_name=f'pi_step_alpha={alpha}.scan', results_dir=results_dir)
            single1s, single2s, coincidences = scanner.scan()
            # scanner.plot_coincidence(f'_pi_step_alpha={alpha}')
    except Exception:
        logger.exception('Caught an exception!')
        raise
    logger.info('Done!')


if __name__ == '__main__':
    main()
