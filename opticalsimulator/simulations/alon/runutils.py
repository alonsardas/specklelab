import datetime
import logging
import logging.handlers
import os
import time

RESULTS_BASE_DIR = r"C:\Users\Owner\Google Drive\People\Alon"


def show_running_time_message(total_seconds, abort_waiting_time=5):
    total_time = datetime.timedelta(seconds=total_seconds)
    now = datetime.datetime.now()
    end_time = now + total_time
    end_time_str = end_time.isoformat(' ', timespec='seconds')
    print(f'Running will end at ~ {end_time_str}')
    print(f'Total running time will be ~ {total_time}')
    print(f'You have {abort_waiting_time} seconds to abort')
    time.sleep(abort_waiting_time)
    # answer = input('Are you sure? [y/N]: ')
    # if answer.lower() == 'y':
    #     return
    # else:
    #     raise KeyboardInterrupt()


def create_results_folder(name=''):
    timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    if name:
        dir_name = f'{timestamp}__{name}'
    else:
        dir_name = timestamp
    results_dir = os.path.join(RESULTS_BASE_DIR, dir_name)
    os.mkdir(results_dir)
    print(f'results dir: {results_dir}')
    return results_dir


def initialize_logger(results_dir):
    formatter = logging.Formatter('%(asctime)s-%(name)s-%(levelname)s: %(message)s', datefmt='%H:%M:%S')
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)

    logs_dir = os.path.join(results_dir, 'logs')
    os.mkdir(logs_dir)
    # file_handler = logging.handlers.RotatingFileHandler(os.path.join(logs_dir, 'log.txt'),
    #                                                     maxBytes=int(1e5), backupCount=20)
    log_file_path = os.path.join(logs_dir, 'log.txt')
    file_handler = logging.FileHandler(log_file_path)
    file_handler.setFormatter(formatter)

    logger = logging.getLogger('lab')
    logger.addHandler(stream_handler)
    logger.addHandler(file_handler)

    stream_handler.setLevel('DEBUG')
    file_handler.setLevel('DEBUG')
    logger.setLevel('DEBUG')

    print(f'log file path: {log_file_path}')
