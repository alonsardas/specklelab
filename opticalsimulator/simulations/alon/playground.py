from opticalsimulator.elements import *
from opticalsimulator.simulations.alon.oldslmdevice import MirrorSLMDevice
from opticalsimulator.simulations.alon.slmsimulators import SLMSimulator


def main():
    # light_propagation_test()
    # single_slit_test()
    # circular_slit_test()
    # imaging_test()
    mirror_slm_test2()


def single_slit_test():
    fig, axes = plt.subplots(1, 2)

    slit_borders = Borders(-1, -1, 1, 1)
    x_pixels = 500
    y_pixels = 2

    output_borders = Borders(-10e7, -1, 10e7, 1)

    # The light source is the whole field
    light_source = BoxLightSource(10, 10)
    input_camera = BasicCamera('input', axes[0])
    propagator = FrenselPropagator(output_borders, x_pixels, y_pixels, 1e7)
    output_camera = BasicCamera('output', axes[1])

    simulator = OpticalSystem(slit_borders, x_pixels, y_pixels, light_source)
    simulator.add_device(input_camera)
    simulator.add_device(propagator)
    simulator.add_device(output_camera)
    simulator.simulate()

    plt.show()


def circular_slit_test():
    fig, axes = plt.subplots(1, 2)

    slit_borders = Borders(-2, -2, 2, 2)
    # x_pixels = 150
    # y_pixels = 150
    x_pixels = 100
    y_pixels = 100

    # output_borders = Borders(-2e7, -2e7, 2e7, 2e7)
    z_distance = 5000
    output_borders = Borders(-2 * z_distance, -2 * z_distance, 2 * z_distance, 2 * z_distance)

    # The light source is the whole field
    light_source = CircularLightSource(radius=1)
    input_camera = BasicCamera('input', axes[0])

    propagator = FrenselPropagator(output_borders, x_pixels, y_pixels, z_distance)
    output_camera = BasicCamera('output', axes[1])

    simulator = OpticalSystem(slit_borders, x_pixels, y_pixels, light_source)
    simulator.add_device(input_camera)
    simulator.add_device(propagator)
    simulator.add_device(output_camera)
    simulator.simulate()

    plt.show()


def single_slit_test2():
    fig, axes = plt.subplots(1, 2)

    borders = Borders(-5e5, -1, 5e5, 1)
    x_pixels = int(10e5)
    y_pixels = 2

    light_source = BoxLightSource(2, 10)
    input_camera = BasicCamera('input', axes[0])
    propagator = AngularMomentumPropagator(5e5)
    output_camera = BasicCamera('output', axes[1])

    simulator = OpticalSystem(borders, x_pixels, y_pixels, light_source)
    simulator.add_device(input_camera)
    simulator.add_device(propagator)
    simulator.add_device(output_camera)
    simulator.simulate()

    plt.show()


def mirror_slm_test():
    fig, ax = plt.subplots()
    slm_device = MirrorSLMDevice(500, np.pi)
    SLMSimulator(slm_device, ax).simulate()

    plt.show()


def mirror_slm_test2():
    fig, ax = plt.subplots()

    omega = 500
    wavelength = 1
    f = 20
    light_k = 2 * np.pi / wavelength
    focal_distance = omega * f / light_k

    print(focal_distance)

    slm_device = MirrorSLMDevice(omega, np.pi)
    SLMSimulator(slm_device, ax, f).simulate()

    plt.show()


def light_propagation_test():
    fig, axes = plt.subplots(1, 2)

    borders = Borders(-1000, -1000, 1000, 1000)
    x_pixels = 1000
    y_pixels = 1000

    light_source = CircularLightSource(radius=20)
    # light_source = GaussianLightSource(sigma=20)
    input_camera = BasicCamera('input', axes[0])
    propagator = LightPropagator(5000)
    output_camera = BasicCamera('output', axes[1])

    simulator = OpticalSystem(borders, x_pixels, y_pixels, light_source)
    simulator.add_device(input_camera)
    simulator.add_device(propagator)
    simulator.add_device(output_camera)
    simulator.simulate()

    plt.show()


def basic_slm_test():
    fig, axes = plt.subplots(1, 2)

    borders = Borders(-1, -1, 1, 1)
    x_pixels = 1000
    y_pixels = 1000

    # light_source = CircularLightSource()
    light_source = GaussianLightSource(sigma=0.1)
    input_camera = BasicCamera('input', axes[0])
    slm_device = MirrorSLMDevice()
    # slm_device = EmptySLMDevice()
    fourier_lens = FourierLens()
    output_camera = BasicCamera('output', axes[1])

    simulator = OpticalSystem(borders, x_pixels, y_pixels, light_source)
    simulator.add_device(input_camera)
    simulator.add_device(slm_device)
    simulator.add_device(fourier_lens)
    simulator.add_device(output_camera)
    simulator.simulate()

    plt.show()


def imaging_test():
    fig, axes = plt.subplots(1, 3)
    input_ax, fourier_ax, image_ax = axes

    borders = Borders(-1, -1, 1, 1)
    x_pixels = 500
    y_pixels = 500

    light_source = CircularLightSource(center_x=0.4, radius=0.2)
    # light_source = GaussianLightSource(sigma=0.1)
    input_camera = BasicCamera('input', input_ax)
    lens1 = FourierLens(f=100)
    fourier_camera = BasicCamera('fourier', fourier_ax)
    lens2 = FourierLens(f=20)
    image_camera = BasicCamera('image', image_ax)

    simulator = OpticalSystem(borders, x_pixels, y_pixels, light_source)
    simulator.add_device(input_camera)
    simulator.add_device(lens1)
    simulator.add_device(fourier_camera)
    simulator.add_device(lens2)
    simulator.add_device(image_camera)
    simulator.simulate()

    plt.show()


if __name__ == '__main__':
    main()
