import matplotlib.axes

from opticalsimulator.elements import *
from opticalsimulator.simulations.alon.oldslmdevice import SLMDevice


class SLMSimulator(object):
    def __init__(self, slm_device: SLMDevice, ax: matplotlib.axes.Axes, f=None, roi=None):
        borders = Borders(-1, -1, 1, 1)
        x_pixels = 1000
        y_pixels = 1000

        light_source = GaussianLightSource(-0.3, sigma=0.6)
        fourier_lens = FourierLens(f)
        output_camera = BasicCamera('output', ax, roi)

        simulator = OpticalSystem(borders, x_pixels, y_pixels, light_source)
        simulator.add_device(slm_device)
        simulator.add_device(fourier_lens)
        simulator.add_device(output_camera)

        self.simulator = simulator

    def simulate(self):
        self.simulator.simulate()
