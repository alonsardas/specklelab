import time

import matplotlib.axes
import numpy as np
from matplotlib import pyplot as plt, patches
from scipy import ndimage
from scipy.spatial import distance

from opticalsimulator import Borders, RealSLMDevice, VimbaCamera, SLMMask, OpticalSystem, FieldGrid, \
    GaussianLightSource, FourierLens, BasicCamera
from opticalsimulator.simulations.alon import slmsimulators
from opticalsimulator.simulations.alon.oldslmdevice import SLMDevice


class StepSLMDevice(SLMDevice):
    def __init__(self):
        self.x_step = 0

    def get_phases(self, xs, ys):
        return np.pi * (xs > self.x_step)


def find_beam_center_using_step_phase():
    camera_size = 0.05
    roi = Borders(-camera_size / 2, -camera_size / 2, camera_size / 2, camera_size / 2)
    slm_device = StepSLMDevice()
    fig, axes = plt.subplots(3, 5)
    for x, ax in zip(np.linspace(-1, 1, 15), axes.flat):
        slm_device.x_step = x
        simulator = slmsimulators.SLMSimulator(slm_device, ax, roi)
        simulator.simulate()
        ax.set_title(f'x={x}')

    plt.show()


class RealSLMDeviceAdapter(object):
    def __init__(self):
        self.device = RealSLMDevice()
        self.x_pixels = 1272
        self.y_pixels = 1024
        self.phase_map = np.zeros((self.y_pixels, self.x_pixels))
        x = np.arange(0, self.x_pixels)
        y = np.arange(0, self.y_pixels)
        self.xs, self.ys = np.meshgrid(x, y)

    def put_x_step(self, x):
        x = int(x)
        self.phase_map[:, x:] = 0
        self.phase_map[:, :x] = np.pi
        self.update_phase_map()

    def put_x_strip(self, start_x, end_x):
        start_x = int(start_x)
        end_x = int(end_x)
        self.phase_map[:, :] = 0
        self.phase_map[:, start_x:end_x] = np.pi
        self.update_phase_map()

    def put_y_step(self, y):
        y = int(y)
        self.phase_map[y:, :] = np.pi
        self.phase_map[:y, :] = 0
        self.update_phase_map()

    def put_y_strip(self, start_y, end_y):
        start_y = int(start_y)
        end_y = int(end_y)
        self.phase_map[:, :] = np.pi
        self.phase_map[start_y:end_y, :] = 0
        self.update_phase_map()

    def update_phase_map(self):
        self.device.update_phase(self.phase_map)
        time.sleep(0.2)

    def update(self):
        self.device.update_phase(self.phase_map)
        time.sleep(0.2)


def find_beam_center_using_step_phase_in_lab_x():
    fig, axes = plt.subplots(3, 5)
    slm_device = RealSLMDeviceAdapter()
    # roi = Borders(360, 260, 450, 320)
    roi = Borders(400, 240, 460, 320)
    # roi = Borders(0, 0, 640, 480)

    camera = VimbaCamera(1, borders=roi)
    # for x, ax in zip(np.linspace(0, 1272, 15), axes.flat):
    # for x, ax in zip(np.linspace(500, 800, 15), axes.flat):
    # for x, ax in zip(np.linspace(620, 750, 15), axes.flat):
    for x, ax in zip(np.linspace(670, 730, 15), axes.flat):
        slm_device.put_x_step(int(x))
        ax.set_title(f'x={int(x)}')
        im = camera.get_image()
        ax.imshow(im, vmin=0, vmax=255)

    plt.show()


def find_beam_center_using_step_phase_in_lab_y():
    fig, axes = plt.subplots(3, 5)
    slm_device = RealSLMDeviceAdapter()
    roi = Borders(400, 240, 460, 320)
    # roi = Borders(0, 0, 440, 344)

    with VimbaCamera(1, borders=roi) as camera:
        # for y, ax in zip(np.linspace(0, 1024, 15), axes.flat):
        # for y, ax in zip(np.linspace(400, 600, 15), axes.flat):
        for y, ax in zip(np.linspace(430, 530, 15), axes.flat):
            slm_device.put_y_step(int(y))
            ax.set_title(f'y={int(y)}')
            im = camera.get_image()
            ax.imshow(im, vmin=0, vmax=255)

    plt.show()


def find_beam_center_using_curved_mirror_test():
    class CurvedMirrorSLMDevice(SLMDevice):
        def get_phases(self, xs, ys):
            alpha = 500
            return (xs ** 2 + ys ** 2) * alpha

    fig, ax = plt.subplots()
    simulator = slmsimulators.SLMSimulator(CurvedMirrorSLMDevice(), ax)
    simulator.simulate()
    plt.show()


# TODO: Move to optical simulator
class SimulationCamera(object):
    def __init__(self, simulator: OpticalSystem = None):
        self.image = None
        self.simulator = simulator

    def prop_through(self, field: FieldGrid):
        self.image = np.abs(field.Es) ** 2

    def get_image(self):
        if self.simulator is not None:
            self.simulator.simulate()
        return self.image


class SimulationSLMDevice(object):
    def __init__(self, borders, x_pixels, y_pixels):
        self.device = SLMMask(borders, x_pixels, y_pixels)
        self.pixels_x = self.device.pixels_x
        self.pixels_y = self.device.pixels_y

        self.x_pixel_size = self.device.pixel_size_x
        self.y_pixel_size = self.device.pixel_size_y

        x = np.arange(0, self.pixels_x)
        y = np.arange(0, self.pixels_y)
        self.x_pixel_indexes, self.y_pixel_indexes = np.meshgrid(x, y)

        self.phase_grid = self.device.phase_grid

    def update(self):
        self.device.update_phase(self.phase_grid)

    def apply(self, field: FieldGrid):
        self.device.prop_through(field)


def get_center_by_maximum(image: np.ndarray):
    return np.unravel_index(np.argmax(image), image.shape)


def get_center_of_mass(image: np.ndarray):
    # TODO: Here, raising in the power of 4 to put more mass and the strong area,
    #  best solution would be to find the location where most light reaches
    # return ndimage.measurements.center_of_mass(image ** 4)
    return ndimage.measurements.center_of_mass(image)


def find_beam_center_using_curved_mirror(slm_device, camera):
    fig, axes = plt.subplots(1, 3)
    basic_axes, calibration_axes, curved_mirror_axes = axes
    assert isinstance(basic_axes, matplotlib.axes.Axes)
    assert isinstance(calibration_axes, matplotlib.axes.Axes)
    assert isinstance(curved_mirror_axes, matplotlib.axes.Axes)

    basic_axes.set_title('basic')
    calibration_axes.set_title('calibration - simple mirror')
    curved_mirror_axes.set_title('curved_mirror')

    slm_device.phase_grid.fill(0)
    slm_device.update()
    basic_image = camera.get_image()
    basic_axes.imshow(basic_image)

    # We assume that x,y pixels are of the same size, therefore, phase per distance is independent of the axis
    calibration_phase_per_pixel = 0.2
    slm_device.phase_grid = slm_device.x_pixel_indexes * calibration_phase_per_pixel
    slm_device.update()
    calibration_image = camera.get_image()
    calibration_axes.imshow(calibration_image)

    center_x = slm_device.pixels_x / 2
    center_y = slm_device.pixels_y / 2

    # We want that no pixel will get more than x phase than its neighbor
    alpha = (np.pi / 12) / (center_x ** 2 - (center_x - 1) ** 2)
    # alpha *= 1.9
    curved_mirror_phase = alpha * (
            (slm_device.x_pixel_indexes - center_x) ** 2 + (slm_device.y_pixel_indexes - center_y) ** 2)
    slm_device.phase_grid = curved_mirror_phase
    slm_device.update()
    curved_mirror_image = camera.get_image()
    curved_mirror_axes.imshow(curved_mirror_image)

    basic_center = get_center_by_maximum(basic_image)
    calibration_center = get_center_by_maximum(calibration_image)
    curved_mirror_center = get_center_by_maximum(curved_mirror_image)
    # basic_center = get_center_of_mass(basic_image)
    # calibration_center = get_center_of_mass(calibration_image)
    # curved_mirror_center = get_center_of_mass(curved_mirror_image)

    curved_mirror_axes.plot(curved_mirror_center[1], curved_mirror_center[0], '*')

    print(f'centers found: basic={basic_center}, calibration={calibration_center}, '
          f'curved mirror={curved_mirror_center}')

    distance_gained = distance.euclidean(curved_mirror_center, basic_center)
    calibration_distance = distance.euclidean(calibration_center, basic_center)

    phase_gained = distance_gained / calibration_distance * calibration_phase_per_pixel
    print(f'phase gained={phase_gained}')
    # r is the radius where the center of the light source hit the SLM device
    r = phase_gained / (2 * alpha)
    angle = np.arctan2(-curved_mirror_center[0] + basic_center[0], curved_mirror_center[1] - basic_center[1])

    x = center_x + r * np.cos(angle)
    y = center_y + r * np.sin(angle)
    print(f'r={r},angle={angle / np.pi * 180}; x={x}, y={y}')
    return x, y


def simulate_find_beam_center_using_curved_mirror():
    borders = Borders(-1, -1, 1, 1)
    x_pixels = 1500
    y_pixels = 1500

    # Creating the SLM Device
    slm_x_pixels = 1272
    slm_y_pixels = 1024
    slm_pixel_size = 0.0015
    slm_borders = Borders(-slm_x_pixels * slm_pixel_size / 2, -slm_y_pixels * slm_pixel_size / 2,
                          slm_x_pixels * slm_pixel_size / 2,
                          slm_y_pixels * slm_pixel_size / 2)
    slm_device = SLMMask(slm_borders, slm_x_pixels, slm_y_pixels)

    # Creating the light source
    center_x = 0
    center_y = 0.2
    light_source = GaussianLightSource(center_x, center_y, sigma=0.4)

    # Adding plot of the input on the slm device
    fig, ax = plt.subplots()
    assert (isinstance(ax, matplotlib.axes.Axes))
    input_camera = BasicCamera('input', ax)
    simulator = OpticalSystem(borders, x_pixels, y_pixels, light_source)
    simulator.add_device(input_camera)
    simulator.simulate()
    rect = patches.Rectangle((slm_borders.min_x, slm_borders.min_y), slm_borders.width,
                             slm_borders.height,
                             edgecolor='r', facecolor="none")
    print('plotting rect at', (slm_borders.min_x, slm_borders.min_y), slm_borders.width,
          slm_borders.height)
    # rect = patches.Rectangle((0, 0), 0.2, 0.2,
    #                          edgecolor='r', facecolor="none")
    ax.add_patch(rect)

    simulator.devices = []

    fourier_lens = FourierLens()
    camera = SimulationCamera(simulator)

    simulator.add_device(slm_device)
    simulator.add_device(fourier_lens)
    simulator.add_device(camera)

    x_shift = center_x / slm_device.pixel_size_x
    y_shift = -center_y / slm_device.pixel_size_y
    print(f'expected: r={np.sqrt(x_shift ** 2 + y_shift ** 2)}; x={1272 / 2 + x_shift},{1024 / 2 + y_shift}')
    x, y = find_beam_center_using_curved_mirror(slm_device, camera)
    ax.plot(slm_borders.min_x + x * slm_device.pixel_size_x, slm_borders.max_y - y * slm_device.pixel_size_y, '+w',
            markersize=20, markeredgecolor='b', markerfacecolor='b', markeredgewidth=4)

    print(f'error distance: {distance.euclidean((x, y), (x_shift, y_shift))}')


def find_beam_center_using_curved_mirror_in_lab():
    slm_device = RealSLMDeviceAdapter()

    with VimbaCamera(1, borders=Borders(0, 0, 800, 600)) as camera:
        find_beam_center_using_curved_mirror(slm_device, camera)
    plt.show()


def put_step():
    slm_device = RealSLMDeviceAdapter()
    # slm_device.put_x_step(1272)
    sign = 1
    # while True:
    #     sign *= -1
    #     slm_device.put_x_step(650 + sign*50)
    #     time.sleep(10)
    slm_device.put_x_step(690)
    plt.show()


def main():
    # find_beam_center_using_step_phase()
    # find_beam_center_using_step_phase_in_lab_x()
    # find_beam_center_using_step_phase_in_lab_y()
    # find_beam_center_using_step_phase()
    # find_beam_center_using_step_phase_in_lab()
    simulate_find_beam_center_using_curved_mirror()
    # put_step()
    # find_beam_center_using_curved_mirror_in_lab()
    plt.show()


if __name__ == '__main__':
    main()
