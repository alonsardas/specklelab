"""
This is a simulation that shows that classical light propagating in free space
behaves like a quantum random walker on a periodic lattice potential

For further reading, see
 Toni Eichelkraut, Christian Vetter, Armando Perez-Leija, Hector Moya-Cessa, Demetrios N. Christodoulides,
 and Alexander Szameit, "Coherent random walks in free space," Optica 1, 268-271 (2014)
"""
import matplotlib.axes
import numpy as np
import scipy.special
from matplotlib import pyplot as plt

from opticalsimulator import Borders, BasicCamera, ZScanner, OpticalSystem, \
    GaussianLightSource, SLMMask, FourierLens, AngularMomentumPropagator
from opticalsimulator.simulations.alon.randomwalks import general_function_encoder
from opticalsimulator.simulations.alon.randomwalks.general_function_encoder import encode_function_on_slm, \
    get_phases_for_slm4
from opticalsimulator.simulations.alon.randomwalks.utils import BesselInitialLight, FieldCropper, FieldCamera, \
    analyse_z0_field

should_analyse_z0_field = True


def basic_propagation():
    """
    Here we only use the desired pattern as the light source,
    and propagate it through space
    """
    fig, axes = plt.subplots(1, 2)

    borders = Borders(-100, -0.5, 100, 0.5)
    x_pixels = 1000
    y_pixels = 5

    light_source = BesselInitialLight(alpha=1.0)
    # light_source = BoxLightSource(1, 1000)
    input_camera = BasicCamera('z=0 plane', axes[0])
    z_scanner = ZScanner(600, 1000, axes[1])

    simulator = OpticalSystem(borders, x_pixels, y_pixels, light_source)
    simulator.add_device(input_camera)
    simulator.add_device(z_scanner)
    simulator.simulate()

    plt.show()


def basic_propagation1():
    fig, axes = plt.subplots(1, 2)

    borders = Borders(-1, -0.5, 1, 0.5)
    x_pixels = 1000
    y_pixels = 5

    light_source = BesselInitialLight(alpha=60.0, sigma=0.6, wavelength=400e-6)
    input_camera = BasicCamera('z=0 plane', axes[0])
    z_scanner = ZScanner(800, 200, axes[1])

    simulator = OpticalSystem(borders, x_pixels, y_pixels, light_source)
    simulator.add_device(input_camera)
    simulator.add_device(z_scanner)
    simulator.simulate()

    plt.show()


def basic_propagation_with_real_params():
    fig, axes = plt.subplots(1, 2)

    borders = Borders(-300e-3, -0.5, 300e-3, 0.5)
    x_pixels = 1000
    y_pixels = 5

    # wavelength = 633e-6
    # wavelength = 800e-6
    wavelength = 400e-6

    light_source = BesselInitialLight(alpha=60.0, sigma=0.6, wavelength=wavelength)
    input_camera = BasicCamera('z=0 plane', axes[0])
    z_scanner = ZScanner(25, 200, axes[1])

    simulator = OpticalSystem(borders, x_pixels, y_pixels, light_source)
    simulator.add_device(input_camera)
    simulator.add_device(z_scanner)
    simulator.simulate()

    plt.show()


def simulate_using_slm(world_borders, world_x_pixels, world_y_pixels,
                       slm_borders: Borders, slm_x_pixels, slm_y_pixels, alpha, sigma, d, f, camera_roi, z0_shift,
                       z_propagation,
                       gaussian_sigma=2, beam_center_x=None, beam_center_y=None, should_plot_input=False):
    """
    Here we use SLM device, and the desired pattern is given in the far field (using fourier lens)
    In order to create the desired pattern, the method used is described here:
    J. Davis, D. Cottrell, J. Campos, M. Yzuel, and I. Moreno, "Encoding amplitude information onto phase-only filters,"
     Appl. Opt.  38, 5004-5013 (1999).
    https://www.osapublishing.org/ao/abstract.cfm?uri=ao-38-23-5004
    """
    # Sizes are based on the article, in mm units

    input_camera = None
    if should_plot_input:
        fig, axes = plt.subplots(1, 4)
        input_axes, slm_axes, z0_ax, propagation_axes = axes
        input_camera = BasicCamera('input', input_axes)
    else:
        fig, axes = plt.subplots(1, 3)
        slm_axes, z0_ax, propagation_axes = axes
    assert isinstance(slm_axes, matplotlib.axes.Axes)
    assert isinstance(z0_ax, matplotlib.axes.Axes)
    assert isinstance(propagation_axes, matplotlib.axes.Axes)

    light_source = GaussianLightSource(0, 0, gaussian_sigma, wavelength=633e-6)
    # light_source = BoxLightSource(1000, 1000, wavelength=633e-6)

    slm_device = SLMMask(slm_borders, slm_x_pixels, slm_y_pixels, dark_outside_borders=False)
    print(slm_device.pixel_size_x, slm_device.pixel_size_y)

    """
    import cv2
    # This puts random phases, generating speckles, for testing
    phases = 2 * np.pi * np.random.random((100, 100))
    phases = cv2.resize(phases, (slm_device.pixels_x, slm_device.pixels_y))
    slm_device.phase_grid = phases
    slm_device.update()
    """

    polar_bessel_func = lambda xs, ys: scipy.special.jv(0, alpha * np.sqrt(xs ** 2 + ys ** 2))
    x_bessel_func = lambda xs, ys: scipy.special.jv(0, alpha * xs) * np.exp(-(xs ** 2 + ys ** 2) / (2 * sigma ** 2))

    # sin_func = lambda xs, ys: np.sin(alpha/5 * xs)
    # func_to_encode = sin_func
    func_to_encode = x_bessel_func

    if beam_center_x is None:
        beam_center_x = slm_borders.center_x
    if beam_center_y is None:
        beam_center_y = slm_borders.center_y

    center_on_diffraction = False
    encode_function_on_slm(slm_device, func_to_encode, d, 0, beam_center_x, beam_center_y,
                           center_on_1st_diffraction=center_on_diffraction)

    slm_axes.set_title('slm mask')
    slm_axes.pcolormesh(slm_device.phase_grid, vmin=0, vmax=2 * np.pi)

    # flattener = AmplitudeFlattener(0.000000005)
    fourier_lens = FourierLens(f)
    # camera_roi = Borders(0.01, borders.min_y, 100, borders.max_y)
    cropper = FieldCropper(camera_roi)
    z0_field_recorder = FieldCamera()
    z0_camera = BasicCamera('z=0 plane', z0_ax, camera_roi)
    z0_ax.set_xlabel('x mm')
    z0_ax.set_ylabel('y mm')

    z_scanner = ZScanner(z_propagation, 100, propagation_axes)
    propagation_axes.set_xlabel('x mm')
    propagation_axes.set_ylabel('z mm - direction of propagation')

    cropped_field_recorder = FieldCamera()

    simulator = OpticalSystem(world_borders, world_x_pixels, world_y_pixels, light_source)

    if should_plot_input:
        simulator.add_device(input_camera)
    simulator.add_device(slm_device)
    simulator.add_device(fourier_lens)
    # simulator.add_device(flattener)

    # It seems that it takes some distance until the light is as desired
    if z0_shift != 0:
        light_propagator = AngularMomentumPropagator(z0_shift)
        simulator.add_device(light_propagator)

    simulator.add_device(z0_camera)
    simulator.add_device(z0_field_recorder)
    simulator.add_device(cropper)
    simulator.add_device(cropped_field_recorder)
    simulator.add_device(z_scanner)
    simulator.simulate()

    cropped_z0_field = cropped_field_recorder.field
    z0_field = z0_field_recorder.field

    if should_analyse_z0_field:
        analyse_z0_field(cropped_z0_field, z0_field, (2.5, 3.5), fit_bounds=((100, 2.5), (150, 3.5)))


def simulate_using_slm_config1():
    """Simulate with something similar to reality"""
    world_borders = Borders(-5, -2.5, 5, 2.5)
    world_x_pixels = 2000
    world_y_pixels = 2000
    slm_borders = Borders(-5, -5, 5, 5)
    slm_x_pixels = 1200
    slm_y_pixels = 1200
    alpha = 125  # As in the article
    # alpha = 30
    sigma = 0.13  # As in the article
    # sigma = 20  # As in the article
    d = 64e-3  # As in the article
    # d = 6
    # d = 0.08
    # d = 0
    f = 300
    camera_roi = Borders(1.7, -0.1, 4.3, 0.1)
    # Note: Interestingly, the phases in z=0 plane aren't flat, yet the behavior is similar to the expected
    # z_propagation = 25  # As in the article
    z_propagation = 150
    simulate_using_slm(world_borders, world_x_pixels, world_y_pixels, slm_borders, slm_x_pixels, slm_y_pixels, alpha,
                       sigma, d, f, camera_roi, 0, z_propagation)


def simulate_using_slm_config2():
    """Simulate with something similar to reality"""
    world_borders = Borders(-5, -5, 5, 5)
    world_x_pixels = 20000
    world_y_pixels = 10  # Something weird happens below that...
    slm_borders = Borders(-5, -5, 5, 5)
    slm_x_pixels = 1272
    slm_y_pixels = world_y_pixels
    alpha = 125  # As in the article
    # alpha = 30
    sigma = 0.13  # As in the article
    # sigma = 20  # As in the article
    d = 64e-3  # As in the article
    # d = 6
    # d = 0.08
    # d = 0
    f = 300
    camera_roi = Borders(1.7, -0.1, 4.3, 0.1)
    # camera_roi = Borders(0.1, -0.1, 10, 0.1)
    # Note: Interestingly, the phases in z=0 plane aren't flat, yet the behavior is similar to the expected
    # z_propagation = 25  # As in the article
    z_propagation = 150
    general_function_encoder.should_plot = True
    simulate_using_slm(world_borders, world_x_pixels, world_y_pixels, slm_borders, slm_x_pixels, slm_y_pixels, alpha,
                       sigma, d, f, camera_roi, 0, z_propagation, should_plot_input=True)


def simulate_using_slm_config3():
    """Simulate with something similar to reality"""
    # world_borders = Borders(-5, -5, 5, 5)
    world_borders = Borders(-25, -5, 25, 5)
    world_x_pixels = 10000
    world_y_pixels = 1200
    slm_borders = Borders(-5, -5, 5, 5)
    # slm_x_pixels = 1200
    slm_x_pixels = 1200
    slm_y_pixels = 1200
    alpha = 125  # As in the article
    sigma = 0.13  # As in the article
    d = 64e-3  # As in the article
    f = 300  # As in the article
    # camera_roi = Borders(1.7, -0.1, 4.3, 0.1)
    camera_roi = Borders(1.7, -2, 4.3, 2)
    # Note: Interestingly, the phases in z=0 plane aren't flat, yet the behavior is similar to the expected
    z0_shift = 7
    # z_propagation = 25  # As in the article
    z_propagation = 150
    # gaussian_sigma = 2  # Reasonable
    gaussian_sigma = 15
    simulate_using_slm(world_borders, world_x_pixels, world_y_pixels, slm_borders, slm_x_pixels, slm_y_pixels, alpha,
                       sigma, d, f, camera_roi, z0_shift, z_propagation)


def simulate_using_slm_config4():
    """Simulate with something even more realistic"""
    slm_width = 15.9
    slm_height = 12.8

    # Making large boundaries (in x axis) let us have more resolution in the far field (after Fourier is applied)
    world_borders = Borders(-30, -slm_height * 2, 30, slm_height * 2)
    world_x_pixels = 25000
    world_y_pixels = 300

    slm_borders = Borders(-slm_width / 2, -slm_height / 2, slm_width / 2, slm_height / 2)
    # slm_x_pixels = 1200
    slm_x_pixels = 1272
    slm_y_pixels = 1024
    alpha = 125  # As in the article
    sigma = 0.13  # As in the article
    d = 64e-3  # As in the article
    f = 300  # As in the article
    # camera_roi = Borders(0.1, -0.1, 15, 0.1)
    beam_center = 2.95
    max_expand = 0.2
    # camera_roi = Borders(1.7, -2, 4.3, 2)
    camera_roi = Borders(beam_center - max_expand, -2, beam_center + max_expand, 2)

    # Note: Interestingly, the phases in z=0 plane aren't flat, yet the behavior is similar to the expected
    # z0_shift = 7
    z0_shift = 1.3

    z_propagation = 25  # As in the article
    # z_propagation = 40

    # gaussian_sigma = 2  # Reasonable
    gaussian_sigma = 5
    general_function_encoder.should_plot = True
    simulate_using_slm(world_borders, world_x_pixels, world_y_pixels, slm_borders, slm_x_pixels, slm_y_pixels, alpha,
                       sigma, d, f, camera_roi, z0_shift, z_propagation, gaussian_sigma=gaussian_sigma)


def simulate_using_slm_config5():
    slm_width = 15.9
    slm_height = 12.8

    # Making large boundaries (in x axis) let us have more resolution in the far field (after Fourier is applied)
    world_borders = Borders(-30, -slm_height * 2, 30, slm_height * 2)
    world_x_pixels = 25000
    world_y_pixels = 300

    slm_borders = Borders(-slm_width / 2, -slm_height / 2, slm_width / 2, slm_height / 2)
    # slm_x_pixels = 1200
    slm_x_pixels = 1272
    slm_y_pixels = 1024
    alpha = 50
    sigma = 0.13  # As in the article
    d = 64e-3  # As in the article
    f = 300  # As in the article
    # camera_roi = Borders(0.1, -0.1, 15, 0.1)
    beam_center = 2.95
    max_expand = 0.2
    # camera_roi = Borders(1.7, -2, 4.3, 2)
    camera_roi = Borders(beam_center - max_expand, -2, beam_center + max_expand, 2)

    # Note: Interestingly, the phases in z=0 plane aren't flat, yet the behavior is similar to the expected
    # z0_shift = 7
    z0_shift = 1.3

    z_propagation = 25  # As in the article
    # z_propagation = 40

    # gaussian_sigma = 2  # Reasonable
    gaussian_sigma = 100
    general_function_encoder.should_plot = True
    simulate_using_slm(world_borders, world_x_pixels, world_y_pixels, slm_borders, slm_x_pixels, slm_y_pixels, alpha,
                       sigma, d, f, camera_roi, z0_shift, z_propagation, gaussian_sigma=gaussian_sigma)


def simulate_using_slm_test_shift():
    """Simulate with something similar to reality"""
    world_borders = Borders(-25, -5, 25, 5)
    world_x_pixels = 10000
    world_y_pixels = 300
    slm_borders = Borders(-5, -5, 5, 5)
    slm_x_pixels = 1200
    slm_y_pixels = 1200
    alpha = 125  # As in the article
    sigma = 0.13  # As in the article
    d = 300e-3
    f = 300  # As in the article
    # camera_roi = Borders(1.7, -0.1, 4.3, 0.1)
    # camera_roi = Borders(1.7, -2, 4.3, 2)
    camera_roi = Borders(-0.4, -0.4, 0.4, 0.4)
    z0_shift = 7
    z_propagation = 150
    # gaussian_sigma = 2  # Reasonable
    gaussian_sigma = 2

    pixel_size_x = slm_borders.width / slm_x_pixels
    pixel_size_y = slm_borders.height / slm_y_pixels

    simulate_using_slm(world_borders, world_x_pixels, world_y_pixels, slm_borders, slm_x_pixels, slm_y_pixels, alpha,
                       sigma, d, f, camera_roi, z0_shift, z_propagation, gaussian_sigma=gaussian_sigma,
                       beam_center_x=300 * pixel_size_x, beam_center_y=100 * pixel_size_y)


def test_encoder_shift():
    general_function_encoder.should_plot = True

    slm_device = SLMMask(Borders(0, 0, 1, 1))

    slm_device.pixel_size_x = 12.5e-3  # Based on the spec
    slm_device.pixel_size_y = 12.5e-3  # Based on the spec
    slm_width = slm_device.pixels_x * slm_device.pixel_size_x
    slm_height = slm_device.pixels_y * slm_device.pixel_size_y
    slm_device.active_borders = Borders(-slm_width / 2, -slm_height / 2, slm_width / 2, slm_height / 2)
    print(slm_device.active_borders)

    # alpha = 125  # As in the article
    alpha = 50

    # sigma = 0.13  # As in the article
    sigma = 0.6
    # d = 64e-3  # As in the article
    d = 100e-3
    # d = 0

    beam_center_x = slm_device.pixels_x / 2 - 20
    beam_center_y = slm_device.pixels_y / 2 + 200

    # For testing
    # beam_center_x = 1100
    # beam_center_y = 0

    print(f'beam center: ({beam_center_x}, {beam_center_y}) in pixels')

    # Make in mm units
    beam_center_x *= slm_device.pixel_size_x
    beam_center_y *= slm_device.pixel_size_y

    beam_center_x += slm_device.active_borders.min_x
    beam_center_y += slm_device.active_borders.min_y
    print(f'beam center: ({beam_center_x}, {beam_center_y}) in mm')

    x_bessel_func = lambda xs, ys: scipy.special.jv(0, alpha * xs) * np.exp(
        -(xs ** 2 + ys ** 2) / (2 * sigma ** 2))

    phases = get_phases_for_slm4(slm_device, x_bessel_func, d, np.pi / 4, beam_center_x, beam_center_y, False)
    slm_device.update_phase(phases)


def test_encoding_method1():
    global should_analyse_z0_field
    should_analyse_z0_field = False

    slm_width = 15.9
    slm_height = 12.8

    # Making large boundaries (in x axis) let us have more resolution in the far field (after Fourier is applied)
    world_borders = Borders(-30, -slm_height * 2, 30, slm_height * 2)
    world_x_pixels = 25000
    world_y_pixels = 300

    slm_borders = Borders(-slm_width / 2, -slm_height / 2, slm_width / 2, slm_height / 2)
    # slm_x_pixels = 1200
    slm_x_pixels = 1272
    slm_y_pixels = 1024
    alpha = 50
    sigma = 0.13  # As in the article
    d = 64e-3  # As in the article
    f = 300  # As in the article
    camera_roi = Borders(0.02, -0.1, 15, 0.1)
    # beam_center = 2.95
    # max_expand = 0.2
    # camera_roi = Borders(1.7, -2, 4.3, 2)
    # camera_roi = Borders(beam_center - max_expand, -2, beam_center + max_expand, 2)

    # Note: Interestingly, the phases in z=0 plane aren't flat, yet the behavior is similar to the expected
    # z0_shift = 7
    # z0_shift = 1.3
    z0_shift = 0

    z_propagation = 25  # As in the article
    # z_propagation = 40

    gaussian_sigma = 5  # Reasonable
    general_function_encoder.should_plot = True
    simulate_using_slm(world_borders, world_x_pixels, world_y_pixels, slm_borders, slm_x_pixels, slm_y_pixels,
                       alpha,
                       sigma, d, f, camera_roi, z0_shift, z_propagation, gaussian_sigma=gaussian_sigma)


def main():
    basic_propagation1()
    # basic_propagation_with_real_params()
    # simulate_using_slm_config3()
    # simulate_using_slm_config4()
    # simulate_using_slm_config5()
    # simulate_using_slm_test_shift()
    # test_encoder_shift()
    # test_encoding_method1()
    plt.show()


if __name__ == '__main__':
    main()
