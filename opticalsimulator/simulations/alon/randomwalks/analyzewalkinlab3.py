import os

import numpy as np
import scipy.special
import uncertainties
from matplotlib import pyplot as plt
import matplotlib.axes
from uncertainties import unumpy

import opticalsimulator
from opticalsimulator import Borders, ZScanner, OpticalSystem, get_borders_around_center
from opticalsimulator.results import ScanResult
from opticalsimulator.simulations.alon.randomwalks import walkinlab2, fitter
from opticalsimulator.simulations.alon.randomwalks.utils import BesselInitialLight
from opticalsimulator.simulations.alon.slm import labslm, lcosphasemask

# results_base_dir = r"C:\Users\Owner\Google Drive\People\Alon"


results_base_dir = os.path.join(opticalsimulator.__path__[0], r'../../Results')


class SingleSliceAnalyzer(object):
    def __init__(self, basedir, scan_pixel_size, bessel_alpha, bessel_sigma, integration_time):
        self.dir = basedir

        results_dir = os.path.join(results_base_dir, self.dir)

        self.results_dir = os.path.abspath(results_dir)
        self.output_dir = os.path.join(self.results_dir, 'analyzed')
        if not os.path.exists(self.output_dir):
            os.mkdir(self.output_dir)

        self.bessel_alpha = bessel_alpha
        self.bessel_sigma = bessel_sigma
        self.integration_time = integration_time

        scan = self._get_scan()
        self.scan_pixels = scan.coincidences.shape[0]
        # self.integration_time = scan.integration_time
        self.scan_pixel_size = scan_pixel_size
        self.scan_width = self.scan_pixels * self.scan_pixel_size

    def _get_scan(self) -> ScanResult:
        # scan_name = f'{z:.3f}.scan'
        scan_name = 'bessel.scan'
        scan_path = os.path.join(self.results_dir, scan_name)
        results = ScanResult()
        results.loadfrom(scan_path)
        return results

    def show(self):
        scan = self._get_scan()
        bessel = scan.coincidences[:, 0]
        # bessel = scan.coincidences[:, 1]
        # bessel = scan.coincidences.mean(axis=1)
        xs = self.scan_pixel_size * (np.arange(0, len(bessel)) - len(bessel) / 2)

        fig, ax = plt.subplots()
        assert isinstance(ax, matplotlib.axes.Axes)
        photons = bessel
        errors = np.sqrt(photons)

        ax.errorbar(xs, photons, errors, fmt='.')
        ax.set_xlabel('x [mm]')
        ax.set_title(f'Total photons - integration time={self.integration_time}s')

        fig.savefig(os.path.join(self.output_dir, f'photons.png'))


class Analyzer(object):

    def __init__(self, basedir, max_distance, dz, scan_pixel_size, bessel_alpha, bessel_sigma, integration_time):
        self.dir = basedir

        mask = labslm.configs[1]['active_mask_slice']
        self.slm1_borders = Borders(mask[1].start, mask[0].start, mask[1].stop, mask[0].stop)

        mask = labslm.configs[2]['active_mask_slice']
        self.slm2_borders = Borders(mask[1].start, mask[0].start, mask[1].stop, mask[0].stop)

        results_dir = os.path.join(results_base_dir, self.dir)

        self.results_dir = os.path.abspath(results_dir)
        self.output_dir = os.path.join(self.results_dir, 'analyzed')
        if not os.path.exists(self.output_dir):
            os.mkdir(self.output_dir)

        self.max_distance = max_distance
        self.dz = dz
        self.zs = np.arange(0, max_distance, dz)

        self.bessel_alpha = bessel_alpha
        self.bessel_sigma = bessel_sigma
        self.integration_time = integration_time

        scan = self._get_scan(0)
        self.scan_pixels = scan.coincidences.shape[0]
        # self.integration_time = scan.integration_time
        self.scan_pixel_size = scan_pixel_size
        self.scan_width = self.scan_pixels * self.scan_pixel_size
        # print(f'Integration time: {self.integration_time}')

    def save_slm1_bessel(self):
        mask = lcosphasemask.load(os.path.join(self.results_dir, 'slm1_bessel.npz'))
        fig, ax = mask.plot()
        fig.savefig(os.path.join(self.output_dir, 'slm1_bessel.png'))
        mask.crop(self.slm1_borders)
        fig, ax = mask.plot()
        fig.savefig(os.path.join(self.output_dir, 'slm1_bessel-cropped.png'))

    def save_slm2_masks(self):
        for z in self.zs:
            mask_name = f'slm2_prop={z:.3f}.npz'
            mask = lcosphasemask.load(os.path.join(self.results_dir, mask_name))
            fig, ax = mask.plot()
            # fig.savefig(os.path.join(self.output_dir, f'slm2_prop={int(z)}.png'))
            plt.close(fig)
            mask.crop(self.slm2_borders)
            fig, ax = mask.plot()
            fig.savefig(os.path.join(self.output_dir, f'slm2_prop={z:.3f}-cropped.png'))
            plt.close(fig)

    def save_photons_scan(self):
        for z in self.zs:
            results = self._get_scan(z)
            fig, ax = results.show(title=f'{z:.3f}')
            fig.savefig(os.path.join(self.output_dir, f'{z:.3f}.png'))

    def show_photon_propagation(self):
        propagation = np.zeros((len(self.zs), self.scan_pixels))

        for i, z in enumerate(self.zs):
            scan = self._get_scan(z)
            propagation[i, :] = scan.coincidences[:, 0]

        fig, ax = plt.subplots()
        ax.set_title('light propagation')
        ax.set_xlabel('pixels')
        ax.set_ylabel('propagation (mm)')
        im = ax.imshow(propagation, origin='lower', aspect='auto',
                       extent=(
                           -self.scan_width / 2, self.scan_width / 2, self.zs[0], self.zs[-1] + self.dz))
        fig.savefig(os.path.join(self.output_dir, 'propagation.png'))
        fig.colorbar(im, ax=ax)
        fig.savefig(os.path.join(self.output_dir, 'propagation-colorbar.png'))

    def show_theoretical(self):
        fig, ax = plt.subplots()

        width = self.scan_pixels * self.scan_pixel_size
        borders = get_borders_around_center(0, 0, width * 2, 1)
        x_pixels = 1000
        y_pixels = 5

        light_source = BesselInitialLight(alpha=self.bessel_alpha / 2, sigma=self.bessel_sigma * 2,
                                          wavelength=walkinlab2.wavelength * 2)
        z_scanner = ZScanner(self.max_distance, 1000, ax)

        simulator = OpticalSystem(borders, x_pixels, y_pixels, light_source)
        simulator.add_device(z_scanner)
        simulator.simulate()

        ax.set_title('propagation in free-space, simulated')
        ax.set_xlabel('mm')
        ax.set_ylabel('propagation (mm)')
        ax.set_xlim(-width / 2, width / 2)
        fig.savefig(os.path.join(self.output_dir, 'simulated.png'))

    def fit_to_bessel(self, z=0):
        scan = self._get_scan(z)
        bessel = scan.coincidences[:, 0]
        # bessel = scan.coincidences[:, 1]
        # bessel = scan.coincidences.mean(axis=1)
        intensity = bessel / bessel.max()
        xs = self.scan_pixel_size * (np.arange(0, len(intensity)) - len(intensity) / 2)

        fig, ax = plt.subplots()
        assert isinstance(ax, matplotlib.axes.Axes)
        # photons = bessel * self.integration_time
        photons = bessel
        errors = np.sqrt(photons)
        # ax.errorbar(xs, photons, errors, fmt='.')

        ax.set_title(f'Total photons - integration time={self.integration_time}s')

        def func(x, A, offset, alpha, sigma, center): return A * (scipy.special.jv(0, alpha * (x - center)) * np.exp(
            -x ** 2 / (2 * sigma ** 2))) ** 2 + A * offset

        fit_params = fitter.FitParams(func, xs, unumpy.uarray(photons, errors))
        # fit_params.bounds = ((5, 0.1, -0.5), (60, 2, 0.5))
        peak = np.max(photons)
        print(peak)
        fit_params.bounds = ((peak * 0.8, 0, 10, 2, -0.5), (peak * 1.2, 1, 20, 5, 0.5))
        fit_params.should_plot_errors = True
        fit = fitter.FuncFit(fit_params)
        fit.print_results()

        fit.plot_data(ax)
        fit.plot_fit(ax, x_points=600)

        fig.savefig(os.path.join(self.output_dir, f'photons-z={z:.3f}.png'))

        simple_fit = False
        if simple_fit:
            def func(x, alpha, sigma, center): return (scipy.special.jv(0, alpha * (x - center)) * np.exp(
                -x ** 2 / (2 * sigma ** 2))) ** 2

            fit_params = fitter.FitParams(func, xs, intensity)
            # fit_params.bounds = ((5, 0.1, -0.5), (60, 2, 0.5))
            fit_params.bounds = ((5, 0.1, -0.5), (100, 2, 0.5))
            fit = fitter.FuncFit(fit_params)
            fit.print_results()
            fit.print_results_latex()

            fig, ax = plt.subplots()
            data_line = fit.plot_data(ax, 'intensity')
            data_line.set_marker('*')
            fit.plot_fit(ax, 'fit to bessel', 600)
            ax.legend()
            ax.set_xlabel('x mm')
            ax.set_ylabel('normalized intensity')
            fig.savefig(os.path.join(self.output_dir, f'fit_to_bessel-z={z:.3f}.png'))

    def save_photons_vs_x(self, z=0):
        scan = self._get_scan(z)
        bessel = scan.coincidences[:, 0]
        xs = self.scan_pixel_size * (np.arange(0, len(bessel)) - len(bessel) / 2)

        fig, ax = plt.subplots()
        assert isinstance(ax, matplotlib.axes.Axes)
        ax.set_title(f'Total photons - integration time={self.integration_time}s')

        ax.plot(xs, bessel)
        fig.savefig(os.path.join(self.output_dir, f'photons-scan-z={z:.3f}.png'))

    def _get_scan(self, z) -> ScanResult:
        scan_name = f'{z:.3f}.scan'
        scan_path = os.path.join(self.results_dir, scan_name)
        results = ScanResult()
        results.loadfrom(scan_path)
        return results


dir1 = '2020-10-17_03-25-41'
dir2 = '2020-10-17_10-35-34'
dir3 = '2020-10-17_20-24-31'
dir4 = '2020-11-13_03-22-42'
dir5 = '2020-11-13_13-44-33-bessel-stopped'
dir6 = '2020-11-13_16-28-30'
dir7 = '2020-11-13_23-23-51'
dir8 = '2020-11-14_02-35-45'
dir9 = '2020-11-14_13-00-39'
dir10 = '2020-12-08_22-32-09'
dir11 = '2020-12-11_01-23-30'
dir12 = '2020-12-11_11-14-16'


def main():
    # analyzer = Analyzer(dir3, 650, 100, 0.05, bessel_alpha=30, bessel_sigma=0.6)
    analyzer = Analyzer(dir12, 10, 100, 0.025, bessel_alpha=30, bessel_sigma=0.6, integration_time=120)
    # analyzer.save_photons_scan()
    # analyzer.show_photon_propagation()
    # analyzer.show_theoretical()
    analyzer.save_photons_vs_x(z=160)
    # analyzer = SingleSliceAnalyzer(dir4, 0.025, bessel_alpha=30, bessel_sigma=0.6, integration_time=120)
    # analyzer.show()
    plt.show()


if __name__ == '__main__':
    main()
