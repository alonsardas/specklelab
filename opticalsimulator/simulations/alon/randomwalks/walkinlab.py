import itertools

import matplotlib.figure
import matplotlib.axes
import numpy as np
import scipy.ndimage
import scipy.special
from matplotlib import pyplot as plt

from opticalsimulator import RealSLMDevice, Borders, SLMMask
from opticalsimulator.simulations.alon.randomwalks import fitter
from opticalsimulator.simulations.alon.randomwalks.general_function_encoder import get_phases_for_slm4


def put_phases_on_slm_and_rotate():
    # All length units are mm

    slm1 = RealSLMDevice(config_num=1)
    slm1.normal()

    slm_device = RealSLMDevice(config_num=2)

    slm_device.pixel_size_x = 12.5e-3  # Based on the spec
    slm_device.pixel_size_y = 12.5e-3  # Based on the spec
    slm_width = slm_device.pixels_x * slm_device.pixel_size_x
    slm_height = slm_device.pixels_y * slm_device.pixel_size_y
    slm_device.active_borders = Borders(-slm_width / 2, -slm_height / 2, slm_width / 2, slm_height / 2)
    print(slm_device.active_borders)

    # alpha = 125  # As in the article
    alpha = 50

    # sigma = 0.13  # As in the article
    sigma = 0.6
    # d = 64e-3  # As in the article
    d = 150e-3
    # d = 0

    # Assuming the beam falls on
    # SLM2 beam:
    # rows 270 <--> 570
    # cols 520 <--> 720

    beam_center_x = (520 + 720) / 2
    beam_center_y = (270 + 570) / 2

    # For testing
    # beam_center_x = 1100
    # beam_center_y = 0

    print(f'beam center: ({beam_center_x}, {beam_center_y}) in pixels')

    # Make in mm units
    beam_center_x *= slm_device.pixel_size_x
    beam_center_y *= slm_device.pixel_size_y

    beam_center_x += slm_device.active_borders.min_x
    beam_center_y += slm_device.active_borders.min_y
    print(f'beam center: ({beam_center_x}, {beam_center_y}) in mm')

    x_bessel_func = lambda xs, ys: scipy.special.jv(0, alpha * xs) * np.exp(
        -(xs ** 2 + ys ** 2) / (2 * sigma ** 2))

    center_on_diffraction = True
    for angle in itertools.cycle(np.linspace(0, 2 * np.pi, 13)):
        bessel_func_on_angle = lambda xs, ys: scipy.special.jv(0, alpha * (
                xs * np.cos(angle) + ys * np.sin(angle))) * np.exp(
            -(xs ** 2 + ys ** 2) / (2 * sigma ** 2))

        # phases = get_phases_for_slm(slm_device, x_bessel_func, d, angle, beam_center_x, beam_center_y,
        #                             center_on_diffraction)

        phases = get_phases_for_slm4(slm_device, bessel_func_on_angle, d, np.pi / 4, beam_center_x, beam_center_y,
                                     center_on_diffraction)

        slm_device.update_phase(phases)
        print(f'angle={angle}, diffraction={center_on_diffraction}')

        plt.pause(10)

        # center_on_diffraction = not center_on_diffraction
        plt.pause(1)
        # slm_device.normal()
        # print('normal')
        plt.pause(0.5)

    fig, ax = plt.subplots()
    ax.imshow(slm_device.phase_grid, cmap='gray', vmin=0, vmax=2 * np.pi)


def put_bessel_in_lab():
    slm1 = RealSLMDevice(config_num=1)
    slm1.normal()

    slm_device = RealSLMDevice(config_num=2)

    slm_device.pixel_size_x = 12.5e-3  # Based on the spec
    slm_device.pixel_size_y = 12.5e-3  # Based on the spec
    slm_width = slm_device.pixels_x * slm_device.pixel_size_x
    slm_height = slm_device.pixels_y * slm_device.pixel_size_y
    slm_device.active_borders = Borders(-slm_width / 2, -slm_height / 2, slm_width / 2, slm_height / 2)
    print(slm_device.active_borders)
    put_bessel_on_slm(slm_device)


def put_bessel_on_slm(slm_device: SLMMask):
    # alpha = 50
    alpha = 20
    sigma = 0.3
    # sigma = 0.6
    sigma = 0.9
    d = 130e-3

    beam_center_x = (520 + 720) / 2
    beam_center_y = (270 + 570) / 2

    # Make in mm units
    beam_center_x *= slm_device.pixel_size_x
    beam_center_y *= slm_device.pixel_size_y

    beam_center_x += slm_device.active_borders.min_x
    beam_center_y += slm_device.active_borders.min_y
    print(f'beam center: ({beam_center_x}, {beam_center_y}) in mm')

    y_bessel_func = lambda xs, ys: scipy.special.jv(0, alpha * ys) * np.exp(
        -(xs ** 2 + ys ** 2) / (2 * sigma ** 2))

    center_on_diffraction = False
    phases = get_phases_for_slm4(slm_device, y_bessel_func, d, np.pi * 3 / 4, beam_center_x, beam_center_y,
                                 center_on_diffraction)

    slm_device.update_phase(phases)


def mock_slm_device():
    pixels_x = 1272
    pixels_y = 1024
    pixel_size_x = 12.5e-3  # Based on the spec
    pixel_size_y = 12.5e-3  # Based on the spec
    slm_width = pixels_x * pixel_size_x
    slm_height = pixels_y * pixel_size_y
    active_borders = Borders(-slm_width / 2, -slm_height / 2, slm_width / 2, slm_height / 2)

    slm_device = SLMMask(active_borders, pixels_x, pixels_y)
    put_bessel_on_slm(slm_device)
    fig, ax = plt.subplots()
    ax.imshow(slm_device.phase_grid, cmap='gray', vmin=0, vmax=2 * np.pi)


def analyze_results():
    # Taken with exposure of ~2e6 (nano-sec?)
    path = r'../../../../../Data/2020-09-15/bessel2-alpha=20.cam'
    config = {'bessel_borders': Borders(345, 0, 413, 197),
              'selected_slice_index': 50}
    # path = r'../../../../../Data/2020-09-15/bessel1.cam'

    im: np.ndarray = np.load(path)['image']

    fig, ax = plt.subplots()
    assert isinstance(fig, matplotlib.figure.Figure)
    image = ax.imshow(im)
    fig.colorbar(image, ax=ax)

    borders = config['bessel_borders']
    im = im[borders.min_y:borders.max_y, borders.min_x:borders.max_x]

    fig, ax = plt.subplots()
    image = ax.imshow(im)
    fig.colorbar(image, ax=ax)

    bessel = im.mean(axis=1)
    bessel /= bessel.max()
    # fig, ax = plt.subplots()
    # TODO: Fix pixel width...
    pixel_size_x = 1
    xs = (np.arange(len(bessel)) - len(bessel) / 2) * pixel_size_x

    # ax.plot(xs, bessel)

    def fit_func(x, alpha, center): return scipy.special.jv(0, alpha * (x - center)) ** 2

    fit_params = fitter.FitParams(fit_func, xs, bessel)
    # fit_params.bounds = fit_bounds
    fit = fitter.FuncFit(fit_params)
    fit.print_results()

    fig, ax = plt.subplots()
    data_line = fit.plot_data(ax, 'intensity')
    data_line.set_marker('*')
    fit.plot_fit(ax, 'fit to bessel', 800)
    ax.legend()
    # ax.set_xlabel('x mm')
    ax.set_ylabel('normalized intensity')

    bessel_slice = im[:, config['selected_slice_index']]
    fig, ax = plt.subplots()
    assert isinstance(ax, matplotlib.axes.Axes)
    ax.plot(xs, bessel_slice)
    ax.set_ylim(0, 255)
    ax.set_title('single slice')
    ax.set_xlabel('pixels')
    ax.set_ylabel('intensity in camera')


def main():
    # put_bessel_in_lab()
    # analyze_results()
    mock_slm_device()
    plt.show()


if __name__ == '__main__':
    main()
