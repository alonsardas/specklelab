"""
Put bessel shape in the lab and propagate it

About the parameters:
 * bigger alpha means that the bessel's lines would be more dense. Obviously,
    the lines shouldn't be smaller than the resolution of the camera
    In addition, having dense lines would make the bessel spread more quickly,
    meaning that the propagation pattern can be of a less distance (and therefore, more flat)
 * sigma is important to make the diffraction pattern shown.
    Having small sigma means that the bessel would be wider in general (which is good)
    but the diffraction grating wouldn't appear.
"""
import datetime
import logging
import os

import numpy as np
import scipy.special
from matplotlib import pyplot as plt

from opticalsimulator import Borders, VimbaCamera
from opticalsimulator.simulations.alon import runutils
from opticalsimulator.simulations.alon.randomwalks.general_function_encoder import Encoder
from opticalsimulator.simulations.alon.slm.basedevice import BaseSLMDevice

try:
    from opticalsimulator.simulations.alon.photonscanner import PhotonScanner
except ImportError:
    print("can't scan photons")

from opticalsimulator.simulations.alon.slm.labslm import get_slm_device
from opticalsimulator.simulations.alon.slm.lcosdevice import LCoSSLMDevice
from opticalsimulator.simulations.alon.slm.monitorimagewriter import MonitorImageWriter
from opticalsimulator.simulations.alon.slm.simulationdevice import SimulationSLMDevice
from opticalsimulator.simulations.alon.slm.slmcontroller import SLMController

f = 300
wavelength = 404e-6
encoding_method = 1

RESULTS_BASE_DIR = r"C:\Users\Owner\Google Drive\People\Alon"

image_writer1, slm1, grating1, inner_slm1 = None, None, None, None
image_writer2, slm2, grating2, inner_slm2 = None, None, None, None
controller1, inner_controller1 = None, None
controller2, inner_controller2 = None, None

logger = logging.getLogger('lab')


def initialize():
    print('Initializing SLMs')

    global image_writer1, slm1, grating1, inner_slm1, image_writer2, slm2, grating2, inner_slm2, \
        controller1, inner_controller1, controller2, inner_controller2
    image_writer1, slm1, grating1, inner_slm1 = get_slm_device(1)
    image_writer2, slm2, grating2, inner_slm2 = get_slm_device(2)
    controller1, inner_controller1 = SLMController(grating1), SLMController(inner_slm1)
    controller2, inner_controller2 = SLMController(grating2), SLMController(inner_slm2)

    controller1.normal()
    controller2.normal()


def put_bessel_on_slm(slm_device, alpha, sigma_y, sigma_x, d, diffraction_angle, global_phase, general_d=0,
                      should_update=False):
    encoder = Encoder()
    # encoder.should_plot_debug = True
    # encoder.should_plot_phase_mask = True
    encoder.set_diffraction_grating(d, diffraction_angle)
    encoder.set_general_grating(-general_d, diffraction_angle)
    encoder.global_phase = global_phase

    def y_bessel_func(xs, ys): return scipy.special.jv(0, alpha * ys) * np.exp(
        -(xs ** 2) / (2 * sigma_x ** 2) - (ys ** 2) / (2 * sigma_y ** 2))

    encoder.encode_fourier(slm_device, y_bessel_func, encoding_method, f, wavelength)
    if should_update:
        slm_device.update()


def put_propagation_pattern(slm_device, distance):
    controller = SLMController(slm_device)
    controller.put_angular_spectrum_method(distance, wavelength, f, axis='y')


def add_propagation_pattern(slm_device: BaseSLMDevice, z_propagation, should_update):
    helper_device = SimulationSLMDevice(
        Borders(0, 0, slm_device.pixels_x * slm_device.pixel_size_x, slm_device.pixels_y * slm_device.pixel_size_y),
        slm_device.pixels_x, slm_device.pixels_y)
    helper_controller = SLMController(helper_device)
    helper_controller.put_angular_spectrum_method(z_propagation, wavelength, f, axis='y')
    slm_device.phase_grid += helper_device.phase_grid
    if should_update:
        slm_device.update()


def plot_phase_masks():
    global encoding_method
    pixel_size_x, pixel_size_y = 12.5e-3, 12.5e-3
    pixels_x = 740 - 640
    pixels_y = 480 - 260
    borders = Borders(0, 0, pixels_x * pixel_size_x, pixels_y * pixel_size_y)
    slm = SimulationSLMDevice(borders, pixels_x, pixels_y)
    put_bessel_on_slm(slm, alpha=60, sigma_y=0.6, sigma_x=0.001, d=150e-3, diffraction_angle=np.pi * 3 / 4, general_d=0,
                      global_phase=1.2)
    controller = SLMController(slm)
    fig, ax = controller.plot_phase_mask()
    ax.set_title('slm1 phase mask - Bessel Fourier')

    pixels_x = 440 - 340
    pixels_y = 550 - 330
    encoding_method = 1
    borders = Borders(0, 0, pixels_x * pixel_size_x, pixels_y * pixel_size_y)
    slm = SimulationSLMDevice(borders, pixels_x, pixels_y)
    put_propagation_pattern(slm, 600)
    controller = SLMController(slm)
    fig, ax = controller.plot_phase_mask()
    ax.set_title('slm2 phase mask - Angular Spectrum Propagation')
    plt.show()


def propagate_bessel():
    global encoding_method
    assert isinstance(inner_slm1, BaseSLMDevice)
    assert isinstance(slm1, LCoSSLMDevice)
    assert isinstance(slm2, LCoSSLMDevice)

    # From what I found, grating doesn't help...

    # Units in mm, as always
    max_distance = 400
    dz = 5
    encoding_method = 1

    exposure_time = 13e3

    put_bessel_on_slm(inner_slm1, sigma_x=0.001, sigma_y=0.6, alpha=30, d=150e-3, diffraction_angle=np.pi * 3 / 4,
                      global_phase=1)

    sleep_time = 0.3
    slm2.allow_wavelength_overflow = False

    timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    results_dir = os.path.join(RESULTS_BASE_DIR, timestamp)
    os.mkdir(results_dir)
    print(f'results dir: {results_dir}')

    # Put the bessel:
    phase_mask = slm1.get_lcos_phase_mask()
    phase_mask.save_phase_mask(os.path.join(results_dir, 'slm1_bessel.npz'))

    with VimbaCamera(0, exposure_time) as cam:
        for z in np.arange(0, max_distance, dz):
            print(f'Propagating z={z}')
            put_propagation_pattern(inner_slm2, z)
            phase_mask = slm2.get_lcos_phase_mask()
            mask_name = f'slm2_prop={z:.3f}.npz'
            phase_mask.save_phase_mask(os.path.join(results_dir, mask_name))

            plt.pause(sleep_time)
            pic_name = f'z={z:.3f}.npz'
            cam.save_image(os.path.join(results_dir, pic_name))


def propagate_bessel_photons():
    global encoding_method

    # Photon scan params
    best_x = 7.025
    best_y = 8.6

    x_pixels, y_pixels = 1, 40
    pixel_size_x, pixel_size_y = 0.05, 0.05

    integration_time = 60

    # start_x = best_x - pixel_size_x * x_pixels / 2
    start_x = best_x
    start_y = best_y - pixel_size_y * y_pixels / 2

    # Bessel params propagation
    # Units in mm, as always
    max_distance = 650
    dz = 100
    encoding_method = 1

    # slm2_alpha = 135
    slm2_alpha = None

    if slm1 is None:
        initialize()
    assert isinstance(inner_slm1, BaseSLMDevice)
    assert isinstance(slm2, LCoSSLMDevice)
    assert isinstance(image_writer1, MonitorImageWriter)
    assert isinstance(image_writer2, MonitorImageWriter)

    put_bessel_on_slm(inner_slm1, sigma_x=0.001, sigma_y=0.6, alpha=30, d=150e-3, diffraction_angle=np.pi * 3 / 4,
                      global_phase=1)

    if slm2_alpha:
        slm2.alpha = slm2_alpha

    zs = np.arange(0, max_distance, dz)

    scan_time = x_pixels * y_pixels * integration_time
    total_seconds = scan_time * len(zs)
    runutils.show_running_time_message(total_seconds)

    results_dir = runutils.create_results_folder()

    runutils.initialize_logger(results_dir)

    for z in zs:
        logger.info(f'Propagating z={z}')
        put_propagation_pattern(inner_slm2, z)
        phase_mask = slm2.get_lcos_phase_mask()
        mask_name = f'slm2_prop={z:.3f}.npz'
        phase_mask.save_phase_mask(os.path.join(results_dir, mask_name))

        scanner = PhotonScanner(integration_time, start_x, start_y, x_pixels, y_pixels, pixel_size_x, pixel_size_y,
                                run_name=f'{z:.3f}.scan', results_dir=results_dir)
        scanner.scan()
        image_writer1.restore_position()
        image_writer2.restore_position()
    logger.info('DONE')


def calc_location_of_red():
    beam_center_x, beam_center_y = 460, 375
    bessel_center_x, bessel_center_y = 370, 380

    d = np.sqrt((beam_center_x - bessel_center_x) ** 2 + (beam_center_y - bessel_center_y) ** 2)
    PIXEL_TO_MILLIMETER = 4.8e-3
    mm_d = d * PIXEL_TO_MILLIMETER
    bx, by = 5.75, 9.75
    # red_x = bx + mm_d * np.sqrt(2)
    # red_y = by - mm_d * np.sqrt(2)
    angle = np.arctan2(bessel_center_y-beam_center_y, bessel_center_x-beam_center_x)
    red_x = bx - mm_d * np.cos(angle)
    red_y = by + mm_d * np.sin(angle)

    print(f'{red_x}, {red_y}')


def run_script():
    print('Start running')
    propagate_bessel_photons()
    print('!!!DONE!!!')


if __name__ == '__main__':
    # run_script()
    plot_phase_masks()
