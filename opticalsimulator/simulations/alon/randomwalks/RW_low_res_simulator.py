"""
This module simulate the results from a scan with low resolution
In this scan, each scanned pixel is mapped to a single fiber in the waveguide equivalent for Bessel propagation
"""

import matplotlib.axes
import matplotlib.axes
import matplotlib.figure
import numpy as np
import scipy.special
import scipy.special
from matplotlib import pyplot as plt
from opticalsimulator import ZScanner, OpticalSystem, get_borders_around_center
from opticalsimulator.simulations.alon.randomwalks import fitter
from opticalsimulator.simulations.alon.randomwalks import walkinlab3
from opticalsimulator.simulations.alon.randomwalks.utils import BesselInitialLight


class Simulator(object):
    def __init__(self, alpha, sigma, prop_distance, pixels_per_site=20, sites=5):
        self.alpha = alpha
        self.sigma = sigma
        self.sites = sites
        self.site_size = np.pi / alpha
        site_size = self.site_size

        width = site_size * sites * 4
        self.pixels = pixels_per_site * sites * 4
        self.pixel_size = width / self.pixels
        pixel_size = self.pixel_size

        self.xs = np.arange(-width / 2, width / 2, pixel_size)

        borders = get_borders_around_center(0, 0, width, 1)
        x_pixels = self.pixels
        y_pixels = 5

        light_source = BesselInitialLight(alpha=alpha, sigma=sigma,
                                          wavelength=walkinlab3.wavelength)

        fig, ax = plt.subplots()
        assert isinstance(fig, matplotlib.figure.Figure)
        fig.canvas.set_window_title('Propagation fig')
        z_scanner = ZScanner(prop_distance, 1000, ax)
        self.zs = z_scanner.zs

        simulator = OpticalSystem(borders, x_pixels, y_pixels, light_source)
        simulator.add_device(z_scanner)
        simulator.simulate()

        self.theoretical_scan = z_scanner.get_image()

        fig.set_visible(False)
        self.prop_fig = fig

        self.theoretical_scan *= 1.3  # Ugly patch to make things normalized, (when using window_size)

    def show_propagation(self):
        self.prop_fig.set_visible(True)

    def show_z0(self):
        scan = self.theoretical_scan[0, :]
        bessel = scan

        xs = self.xs

        fig, ax = plt.subplots()
        assert isinstance(ax, matplotlib.axes.Axes)
        # photons = bessel * self.integration_time

        ax.set_title(f'z=0, Bessel')

        def func(x, A, offset, alpha, sigma, center): return A * (scipy.special.jv(0, alpha * (x - center)) * np.exp(
            -x ** 2 / (2 * sigma ** 2))) ** 2 + A * offset

        fit_params = fitter.FitParams(func, xs, bessel)
        # fit_params.bounds = ((5, 0.1, -0.5), (60, 2, 0.5))
        fit_params.bounds = ((0.8, 0, 40, 0, -0.5), (1.2, 1, 80, 3, 0.5))
        fit = fitter.FuncFit(fit_params)
        fit.print_results()

        fit.plot_data(ax)
        fit.plot_fit(ax, x_points=600)

        # add the sites expected location
        site_xs = np.arange(0, (self.sites + 1) * self.site_size, self.site_size)

        for x in site_xs:
            ax.plot([x, x], [0, 1], '--g')

        # Show windows
        indexes = self._indexes_around_site(0, 20)
        x = self.xs[indexes[0]]
        ax.plot([x, x], [0, 1], '-r')
        x = self.xs[indexes[-1]]
        ax.plot([x, x], [0, 1], '-r')

        indexes = self._indexes_around_site(1, 20)
        x = self.xs[indexes[0]]
        ax.plot([x, x], [0, 1], '-r')
        x = self.xs[indexes[-1]]
        ax.plot([x, x], [0, 1], '-r')

    def show_site(self, m, window_size=1):
        # x_index = self._x_to_index(m * self.site_size)
        pixels_intensities: np.ndarray = self.theoretical_scan[:, self._indexes_around_site(m, window_size)]
        site_intensity = pixels_intensities.sum(axis=1) / window_size

        fig, ax = plt.subplots()
        assert isinstance(ax, matplotlib.axes.Axes)
        fig.canvas.set_window_title(f'Site m={m} fig')

        ax.plot(self.zs, site_intensity)
        ax.set_title(f'Propagation for site {m}')
        ax.set_xlabel('z mm')
        ax.set_ylabel('Intensity')

        # add theoretical
        wavelength = 404e-6
        light_k = 2 * np.pi / wavelength
        func = lambda z: (scipy.special.jv(m, self.alpha ** 2 * z / (4 * light_k))) ** 2
        expected = func(self.zs)
        ax.plot(self.zs, expected)

    def show_all_sites(self, window_size=1):
        for m in range(self.sites):
            self.show_site(m, window_size=window_size)

    def _x_to_index(self, x):
        x_in_pixels = x / self.pixel_size
        return int(self.pixels / 2 + x_in_pixels)

    def _indexes_around_site(self, m, window_size):
        site_index = self._x_to_index(self.site_size * m)
        return site_index - int(window_size / 2) + np.arange(window_size)


def main():
    # simulator = Simulator(alpha=60, sigma=0.6, prop_distance=250, sites=10)
    simulator = Simulator(alpha=60, sigma=0.6, prop_distance=125, sites=5)
    # simulator = Simulator(alpha=60, sigma=2, prop_distance=125, sites=5)
    # simulator.show_propagation()
    # simulator.show_z0()
    # simulator.plot_site(0)
    # simulator.show_all_sites(window_size=1)
    simulator.show_all_sites(window_size=18)
    plt.show()


if __name__ == '__main__':
    main()
