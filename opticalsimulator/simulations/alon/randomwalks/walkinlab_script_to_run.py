import datetime
import os

import numpy as np
import scipy.special
from matplotlib import pyplot as plt

from opticalsimulator import Borders, VimbaCamera
from opticalsimulator.simulations.alon.randomwalks.general_function_encoder import Encoder
from opticalsimulator.simulations.alon.slm.labslm import get_slm_device
from opticalsimulator.simulations.alon.slm.lcosdevice import LCoSSLMDevice
from opticalsimulator.simulations.alon.slm.simulationdevice import SimulationSLMDevice
from opticalsimulator.simulations.alon.slm.slmcontroller import SLMController

f = 300
wavelength = 400e-6
encoding_method = 1

RESULTS_BASE_DIR = r"C:\Users\Owner\Google Drive\People\Alon"

slm1, grating1, inner_slm1 = None, None, None
slm2, grating2, inner_slm2 = None, None, None
controller1, inner_controller1 = None, None
controller2, inner_controller2 = None, None


def initialize():
    print('Initializing SLMs')

    global slm1, grating1, inner_slm1, slm2, grating2, inner_slm2, \
        controller1, inner_controller1, controller2, inner_controller2
    image_writer1, slm1, grating1, inner_slm1 = get_slm_device(1)
    image_writer2, slm2, grating2, inner_slm2 = get_slm_device(2)
    controller1, inner_controller1 = SLMController(grating1), SLMController(inner_slm1)
    controller2, inner_controller2 = SLMController(grating2), SLMController(inner_slm2)

    controller1.normal()
    controller2.normal()


def put_bessel_on_slm(slm_device, alpha=20, sigma=0.3, d=130e-3, diffraction_angle=np.pi * 3 / 4, general_d=0,
                      global_phase=1.2):
    encoder = Encoder()
    # encoder.should_plot_debug = True
    # encoder.should_plot_phase_mask = True
    encoder.set_diffraction_grating(d, diffraction_angle)
    encoder.set_general_grating(-general_d, diffraction_angle)
    encoder.global_phase = global_phase

    def y_bessel_func(xs, ys): return scipy.special.jv(0, alpha * ys) * np.exp(
        -(xs ** 2 + ys ** 2) / (2 * sigma ** 2))

    encoder.encode_fourier(slm_device, y_bessel_func, encoding_method, f, wavelength)
    slm_device.update()


def put_propagation_pattern(slm_device, distance, keep_center_fixed=False):
    controller = SLMController(slm_device)
    controller.put_angular_spectrum_method(distance, wavelength, f, keep_center_fixed)


def plot_phase_masks():
    global encoding_method
    pixel_size_x, pixel_size_y = 12.5e-3, 12.5e-3
    pixels_x = 740 - 640
    pixels_y = 480 - 260
    borders = Borders(0, 0, pixels_x * pixel_size_x, pixels_y * pixel_size_y)
    slm = SimulationSLMDevice(borders, pixels_x, pixels_y)
    put_bessel_on_slm(slm, alpha=50)
    controller = SLMController(slm)
    fig, ax = controller.plot_phase_mask()
    ax.set_title('slm1 phase mask - Bessel Fourier')

    pixels_x = 440 - 340
    pixels_y = 550 - 330
    encoding_method = 1
    borders = Borders(0, 0, pixels_x * pixel_size_x, pixels_y * pixel_size_y)
    slm = SimulationSLMDevice(borders, pixels_x, pixels_y)
    put_propagation_pattern(slm, 30, True)
    controller = SLMController(slm)
    fig, ax = controller.plot_phase_mask()
    ax.set_title('slm2 phase mask - Angular Spectrum Propagation')


def propagate_bessel():
    global encoding_method
    assert isinstance(slm1, LCoSSLMDevice)
    assert isinstance(slm2, LCoSSLMDevice)

    # From what I found, grating doesn't help...

    # Units in mm, as always
    max_distance = 80
    dz = 0.2
    encoding_method = 1

    exposure_time = 500e3

    put_bessel_on_slm(inner_slm1, sigma=0.4, alpha=75, d=80e-3, diffraction_angle=np.pi * 4 / 5, global_phase=1)
    slm2.allow_wavelength_overflow = False

    timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    results_dir = os.path.join(RESULTS_BASE_DIR, timestamp)
    os.mkdir(results_dir)

    # Put the bessel:
    # About the parameters:
    # bigger alpha means that the bessel's lines would be more dense. Obviously, the lines shouldn't be smaller than the resolution of the camera
    # In addition, having dense lines would make the bessel spread more quickly, meaning that the propagation pattern can be of a less distance
    # (and therefore, more flat)
    # sigma is important to make the diffraction pattern shown. Having small sigma means that the bessel would be wider in general (which is good)
    # but the diffraction grating wouldn't appear.
    phase_mask = slm1.get_lcos_phase_mask()
    phase_mask.save_phase_mask(os.path.join(results_dir, 'slm1_bessel.npz'))

    with VimbaCamera(0, exposure_time) as cam:
        for z in np.arange(0, max_distance, dz):
            print(f'Propagating z={z}')
            put_propagation_pattern(inner_slm2, z, True)
            phase_mask = slm2.get_lcos_phase_mask()
            mask_name = f'slm2_prop={z:.3f}.npz'
            phase_mask.save_phase_mask(os.path.join(results_dir, mask_name))

            plt.pause(0.5)
            pic_name = f'z={z:.3f}.npz'
            cam.save_image(os.path.join(results_dir, pic_name))


if __name__ == '__main__':
    print('Start running')
    initialize()
    propagate_bessel()
    print('!!!DONE!!!')
