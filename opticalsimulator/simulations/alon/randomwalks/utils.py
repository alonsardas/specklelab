from typing import Optional

import matplotlib.axes
import numpy as np
import scipy.special
from matplotlib import pyplot as plt

from opticalsimulator import LightSource, OpticalDevice, FieldGrid
from opticalsimulator.simulations.alon.randomwalks import fitter


class BesselInitialLight(LightSource):
    def __init__(self, alpha: float, sigma=0.0, wavelength=1.0):
        super().__init__(wavelength=wavelength)
        self.alpha = alpha
        self.sigma = sigma

    def get_intensity(self, xs, ys) -> np.ndarray:
        bessel = scipy.special.jv(0, self.alpha * xs)
        if self.sigma != 0:
            gaussian = np.exp(-(xs ** 2 + ys ** 2) / (2 * self.sigma ** 2))
            bessel *= gaussian
        return bessel


class AmplitudeFlattener(OpticalDevice):
    def __init__(self, threshold=0.9):
        self.threshold = threshold

    def prop_through(self, field: FieldGrid):
        intensity = field.Es * field.Es.conjugate()
        max_intensity = np.max(intensity)
        field.Es[intensity > max_intensity * self.threshold] = max_intensity * self.threshold
        # field.Es[intensity > max_intensity * self.threshold] = 0


class FieldCropper(OpticalDevice):
    def __init__(self, borders):
        self.borders = borders

    def prop_through(self, field: FieldGrid):
        xs = field.Xs[0, :]
        ys = field.Ys[:, 0]

        min_x_index = np.where(xs > self.borders.min_x)[0][0]
        max_x_index = np.where(xs < self.borders.max_x)[0][-1]
        min_y_index = np.where(ys > self.borders.min_y)[0][0]
        max_y_index = np.where(ys < self.borders.max_y)[0][-1]
        field.Xs = field.Xs[min_y_index:max_y_index, min_x_index:max_x_index]
        field.Ys = field.Ys[min_y_index:max_y_index, min_x_index:max_x_index]
        field.Es = field.Es[min_y_index:max_y_index, min_x_index:max_x_index]


class FieldCamera(OpticalDevice):
    def __init__(self):
        self.field: Optional[FieldGrid] = None

    def prop_through(self, field: FieldGrid):
        self.field = FieldGrid(field.Xs.copy(), field.Ys.copy(), field.Es.copy())


def analyse_z0_field(cropped_field: FieldGrid, all_field: Optional[FieldGrid], x_bounds, fit_bounds=None):
    if all_field:
        full_intensity = np.sum(np.abs(all_field.Es) ** 2)
        cropped_intensity = np.sum(np.abs(cropped_field.Es) ** 2)
        print(f'intensity ratio={cropped_intensity / full_intensity}')

    field = cropped_field

    # Fit to bessel
    y_pixels, x_pixels = field.Es.shape
    y_row = int(y_pixels / 2)  # Taking the middle row as representative
    xs = field.Xs[y_row, :]
    row_intensity: np.ndarray = np.abs(field.Es[y_row, :]) ** 2
    center_index = row_intensity.argmax()
    row_intensity /= row_intensity[center_index]
    # center_x = xs[center_index]

    relevant_xs = np.logical_and(xs > x_bounds[0], xs < x_bounds[1])
    xs = xs[relevant_xs]
    intensity = row_intensity[relevant_xs]

    if fit_bounds:
        # def fit_func1(x, alpha): return scipy.special.jv(0, alpha * (x - center_x)) ** 2

        def fit_func2(x, alpha, center): return scipy.special.jv(0, alpha * (x - center)) ** 2

        fit_params = fitter.FitParams(fit_func2, xs, intensity)
        fit_params.bounds = fit_bounds
        fit = fitter.FuncFit(fit_params)
        fit.print_results()

        fig, ax = plt.subplots()
        data_line = fit.plot_data(ax, 'intensity')
        data_line.set_marker('*')
        fit.plot_fit(ax, 'fit to bessel', 800)
        ax.legend()
        ax.set_xlabel('x mm')
        ax.set_ylabel('normalized intensity')

    """
    ax.plot(xs, intensity)

    expected_xs = np.linspace(xs.min(), xs.max(), 500)
    # TODO: Make parameters
    alpha = 125
    wavelength = 633e-6
    f = 300
    expected_alpha = alpha / (f * wavelength) / (2 * np.pi)
    expected_ys = scipy.special.jv(0, expected_alpha * (expected_xs - center_x)) ** 2
    ax.plot(expected_xs, expected_ys)
    """

    # Show phases
    # Taking only y=0 field
    phases = np.angle(field.Es[y_row, :])
    phases = np.mod(phases, 2 * np.pi)
    # phases = np.mod(phases, np.pi)
    """
    fig, ax = plt.subplots()
    ax.hist(phases.flatten(), bins=np.linspace(0, 2 * np.pi, 30))
    """

    fig, axes = plt.subplots(1, 2)
    ax = axes[0]
    assert isinstance(ax, matplotlib.axes.Axes)
    phases = phases[row_intensity > 0.02]
    xs = field.Xs[y_row, row_intensity > 0.02]
    # phases = phases[relevant_xs]
    ax.plot(xs, phases, '.')
    ax.set_ylim(0, 2 * np.pi)
    ax.set_xlabel('x [mm]')
    ax.set_ylabel('phase [rad]')

    ax = axes[1]
    assert isinstance(ax, matplotlib.axes.Axes)
    phases = np.mod(phases, np.pi)
    ax.plot(xs, phases, '.')
    # ax.set_ylim(0, np.pi)
    ax.set_ylim(0, 2 * np.pi)
    ax.set_xlabel('x [mm]')
    ax.set_ylabel('phase mod pi [rad]')


# oneliner to turn npz to mat file:
# python -c "import pylab, scipy.io; scipy.io.savemat('tmp.mat', pylab.np.load('diffuser.diffuser'))"
