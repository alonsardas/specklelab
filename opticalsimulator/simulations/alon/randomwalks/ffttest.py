import numpy as np
from matplotlib import pyplot as plt

xs = np.linspace(0, 20, 250)
ys = np.sin(xs) + np.random.random(len(xs))

fig, ax = plt.subplots()
ax.plot(xs, ys, '.')
ax.set_title('data')

fft_values = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(ys)))
fig, ax = plt.subplots()
ax.plot(np.abs(fft_values), '.')
ax.set_title('fft')

dx = xs[1] - xs[0]
print(dx)
shift = 15 * 0.0498
ys = ys.astype('complex128')
ys *= np.exp(1j * 2 * np.pi * xs * shift)
# ys *= np.cos(xs * shift)
fft_values = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(ys)))
fig, ax = plt.subplots()
ax.plot(np.abs(fft_values), '.')
ax.set_title('shifted fft')

freqs = np.fft.fftfreq(len(xs), dx)
print(freqs)
df = freqs[1]-freqs[0]
print(df)

plt.show()
