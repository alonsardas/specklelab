"""
Using Encoder class and SLMController (instead of free-space propagation)
"""
import matplotlib.axes
import matplotlib.figure
import numpy as np
import scipy.special
from matplotlib import pyplot as plt

from opticalsimulator import Borders, BasicCamera, OpticalSystem, \
    GaussianLightSource, FourierLens, AngularMomentumPropagator
from opticalsimulator.simulations.alon.randomwalks.general_function_encoder import Encoder
from opticalsimulator.simulations.alon.randomwalks.utils import FieldCamera, FieldCropper, analyse_z0_field
from opticalsimulator.simulations.alon.slm.basedevice import BaseSLMDevice
from opticalsimulator.simulations.alon.slm.simulationdevice import SimulationSLMDevice
from opticalsimulator.simulations.alon.slm.slmcontroller import SLMController

wavelength = 633e-6

should_analyse_z0_field = True
should_plot_input = False
should_propagate = False


def simulate_using_slm(simulator, bessel_slm_device, propagation_slm_device: BaseSLMDevice,
                       alpha, sigma, encoder: Encoder, encoding_method, f,
                       z_propagation, z0_shift, camera_roi,
                       image_borders=None, fit_bounds=None):
    input_camera = None
    if should_plot_input:
        fig, axes = plt.subplots(1, 3)
        input_axes, z0_ax, propagation_axes = axes
        input_camera = BasicCamera('input', input_axes, live_update=False)
    else:
        fig, axes = plt.subplots(1, 2)
        z0_ax, propagation_axes = axes
    assert isinstance(z0_ax, matplotlib.axes.Axes)
    assert isinstance(propagation_axes, matplotlib.axes.Axes)

    def x_bessel_func(xs, ys):
        return scipy.special.jv(0, alpha * xs) * np.exp(-(xs ** 2 + ys ** 2) / (2 * sigma ** 2))

    encoder.encode_fourier(bessel_slm_device, x_bessel_func, encoding_method, f, wavelength)

    controller = SLMController(propagation_slm_device)
    controller.put_angular_spectrum_method(z_propagation, wavelength, f)
    fig, ax = controller.plot_phase_mask()
    ax.set_title('propagation slm mask')

    fourier_lens = FourierLens(f)

    cropper = FieldCropper(camera_roi)
    z0_field_recorder = FieldCamera()
    z0_camera = BasicCamera('z=0 plane', z0_ax, log_scale_intensity=False, live_update=False)
    z0_ax.set_xlabel('x mm')
    z0_ax.set_ylabel('y mm')

    if should_plot_input:
        simulator.add_device(input_camera)

    simulator.add_device(bessel_slm_device)
    simulator.add_device(propagation_slm_device)
    simulator.add_device(fourier_lens)

    simulator.add_device(cropper)

    # It seems that it takes some distance until the light is as desired
    if z0_shift != 0:
        light_propagator = AngularMomentumPropagator(z0_shift)
        simulator.add_device(light_propagator)

    simulator.add_device(z0_camera)
    simulator.add_device(z0_field_recorder)

    simulator.simulate()

    if should_analyse_z0_field:
        analyse_z0_field(z0_field_recorder.field, None, (image_borders.min_x, image_borders.max_x), fit_bounds)


def simulate():
    slm_width, slm_height = 15.9, 12.8
    slm_x_pixels, slm_y_pixels = 1272, 1024

    # Making large boundaries (in x axis) let us have more resolution in the far field (after Fourier is applied)
    world_width = slm_width * 4
    world_borders = Borders(-world_width / 2, -slm_height, world_width / 2, slm_height)
    world_x_pixels, world_y_pixels = slm_x_pixels * 4, slm_y_pixels

    slm_borders = Borders(-slm_width / 2, -slm_height / 2, slm_width / 2, slm_height / 2)
    bessel_slm_device = SimulationSLMDevice(slm_borders, slm_x_pixels, slm_y_pixels, dark_outside=True)

    slm_shift_x = 5
    propagation_slm_device = SimulationSLMDevice(
        Borders(slm_borders.min_x + slm_shift_x, slm_borders.min_y, slm_borders.max_x + slm_shift_x, slm_borders.max_y),
        slm_x_pixels, slm_y_pixels)

    global should_analyse_z0_field, should_propagate
    should_analyse_z0_field = True
    should_propagate = True

    alpha = 80
    sigma = 0.6

    d = 200e-3
    diffraction_angle = np.pi / 4
    f = 300  # As in the article

    encoder = Encoder()
    encoder.should_plot_debug = False
    encoder.should_plot_phase_mask = True
    encoder.set_diffraction_grating(d, diffraction_angle)
    # encoder.set_general_grating(-d / 0.2, diffraction_angle)
    encoder.global_phase = 1.2

    # camera_roi = Borders(0.3, 0.5, 0.8, 1)
    # camera_roi = Borders(-0.3, -0.5, 2, 2)
    camera_roi = Borders(0.3, 1, 1.1, 1.8)
    z0_shift = 0

    z_propagation = 14
    # z_propagation = 0
    gaussian_sigma = 500
    # gaussian_sigma = 2
    light_source = GaussianLightSource(0, 0, gaussian_sigma, wavelength=wavelength)

    simulator = OpticalSystem(world_borders, world_x_pixels, world_y_pixels, light_source)

    # fit_bounds = ((10, 0.5), (600, 0.8))
    fit_bounds = None
    image_borders = Borders(-2, -1, 2, 1)
    # image_borders = None

    encoding_method = 1

    simulate_using_slm(simulator, bessel_slm_device, propagation_slm_device,
                       alpha, sigma, encoder, encoding_method, f,
                       z_propagation, z0_shift, camera_roi,
                       image_borders, fit_bounds)

    print(f'phase range: [{bessel_slm_device.phase_grid.min()} - {bessel_slm_device.phase_grid.max()}]')


def main():
    simulate()
    # test_beam_center()
    plt.show()


if __name__ == '__main__':
    main()
