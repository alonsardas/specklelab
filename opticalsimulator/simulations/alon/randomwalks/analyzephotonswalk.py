"""
This script analyzes the results obtained from several scans in lab of photons
"""

import os

import cv2
import matplotlib.axes
import matplotlib.figure
import numpy as np
from matplotlib import pyplot as plt

import opticalsimulator
from opticalsimulator import ZScanner, OpticalSystem, get_borders_around_center
from opticalsimulator.results import ScanResult
from opticalsimulator.simulations.alon.randomwalks import walkinlab3
from opticalsimulator.simulations.alon.randomwalks.utils import BesselInitialLight

# results_base_dir = r"C:\Users\Owner\Google Drive\People\Alon"
results_base_dir = os.path.join(opticalsimulator.__path__[0], r'../../Results')
results_base_dir = os.path.abspath(results_base_dir)


class Analyzer(object):
    def __init__(self, paths, distances, max_distance, output_dir, scan_pixels, scan_pixel_size,
                 bessel_alpha, bessel_sigma, light_baseline=None):
        self.distances = distances
        self.max_distance = max_distance
        self.bessel_alpha = bessel_alpha
        self.bessel_sigma = bessel_sigma
        self.results_dir = os.path.abspath(results_base_dir)
        self.output_dir = os.path.join(results_base_dir, output_dir)
        self.scan_pixel_size = scan_pixel_size
        self.scan_pixels = scan_pixels
        self.scan = self._get_total_scan(paths)

        if not light_baseline:
            self.light_baseline = np.min(self.scan)
        else:
            self.light_baseline = light_baseline
        self.scan -= self.light_baseline
        print(f'baseline: {self.light_baseline}')

    def show_propagation(self):
        fig, ax = plt.subplots()
        assert isinstance(ax, matplotlib.axes.Axes)
        ax.set_title('light propagation')
        ax.set_xlabel('x (mm)')
        ax.set_ylabel('propagation (mm)')

        xs = self.xs - np.mean(self.xs)
        zs = self.distances
        # zs.append(self.max_distance)

        im = ax.pcolormesh(xs, zs, self.scan, shading='nearest', vmin=0)
        fig.savefig(os.path.join(self.output_dir, 'propagation.png'))
        fig.colorbar(im, ax=ax)
        fig.savefig(os.path.join(self.output_dir, 'propagation-colorbar.png'))

        fig = plt.figure(frameon=False)
        ax = plt.Axes(fig, [0., 0., 1., 1.])
        ax.set_axis_off()
        fig.add_axes(ax)
        ax.pcolormesh(xs, zs, self.scan, shading='nearest', vmin=0)
        fig.savefig(os.path.join(self.output_dir, 'propagation-no-borders.png'))

    def show_propagation_high_res(self):
        high_res = cv2.resize(self.scan, (1000, 1000))
        fig, ax = plt.subplots()
        assert isinstance(ax, matplotlib.axes.Axes)
        assert isinstance(fig, matplotlib.figure.Figure)
        ax.imshow(high_res, origin='lower')

    def show_theoretical(self):
        fig, ax = plt.subplots()

        width = self.scan_pixels * self.scan_pixel_size
        borders = get_borders_around_center(0, 0, width * 2, 1)
        x_pixels = 1000
        y_pixels = 5

        light_source = BesselInitialLight(alpha=self.bessel_alpha, sigma=self.bessel_sigma,
                                          wavelength=walkinlab3.wavelength)
        z_scanner = ZScanner(self.distances[-1], 1000, ax)

        simulator = OpticalSystem(borders, x_pixels, y_pixels, light_source)
        simulator.add_device(z_scanner)
        simulator.simulate()

        theoretical_scan = z_scanner.get_image()

        ax.set_title('propagation in free-space, simulated')
        ax.set_xlabel('mm')
        ax.set_ylabel('propagation (mm)')
        ax.set_xlim(-width / 4, width / 4)
        fig.savefig(os.path.join(self.output_dir, 'simulated.png'))

        fig = plt.figure(frameon=False)
        ax = plt.Axes(fig, [0., 0., 1., 1.])
        ax.set_axis_off()
        fig.add_axes(ax)

        ax.set_title('')
        extent = (borders.min_x, borders.max_x, 0, self.distances[-1])
        ax.imshow(theoretical_scan, origin='lower', aspect='auto', extent=extent)
        ax.set_xlim(-width / 4, width / 4)

        # fig.savefig(os.path.join(self.output_dir, 'theoretical-no-borders.png'))


        should_convolve = False
        # This takes a lot of time...
        if should_convolve:
            theoretical_scan = cv2.resize(theoretical_scan, (300, 100))

            rect = np.zeros(theoretical_scan.shape)
            rect[50:51, 150:170] = 1

            import scipy.signal

            print('before')
            low_res = scipy.signal.convolve2d(theoretical_scan, rect, 'same')
            print('after')
            fig, ax = plt.subplots()
            assert isinstance(ax, matplotlib.axes.Axes)
            assert isinstance(fig, matplotlib.figure.Figure)
            ax.imshow(low_res, aspect='auto', origin='lower')

        low_res = cv2.resize(theoretical_scan, (80*4, 20), interpolation=cv2.INTER_AREA)
        fig, ax = plt.subplots()
        assert isinstance(ax, matplotlib.axes.Axes)
        assert isinstance(fig, matplotlib.figure.Figure)
        ax.imshow(low_res, extent=extent, aspect='auto', origin='lower')
        ax.set_xlim(-width / 4, width / 4)
        fig.savefig(os.path.join(self.output_dir, 'theoretical-low-res.png'))

    def _get_total_scan(self, paths):
        scan = np.zeros((len(self.distances), self.scan_pixels))
        for i, path in enumerate(paths):
            scan[i, :] = self._get_scan(path)
        return scan

    def _get_scan(self, path) -> ScanResult:
        scan_name = 'bessel.scan'
        scan_path = os.path.join(self.results_dir, path, scan_name)
        results = ScanResult()
        results.loadfrom(scan_path)
        scan = results.coincidences[:, -1]

        self.xs = results.Y
        return scan


def main():
    paths = ['2020-11-13_03-22-42', '2020-11-14_17-06-37', '2020-11-13_16-28-30', '2020-11-13_23-23-51',
             '2020-11-14_02-35-45',
             '2020-11-14_13-00-39']
    distances = [0, 85, 162.5, 205, 265, 350]
    output_dir = '2020-11-14'

    integration_time = 120

    light_baseline = 0.06 * integration_time
    analyzer = Analyzer(paths, distances, 430, output_dir, 80, 0.025, bessel_alpha=30, bessel_sigma=0.6,
                        light_baseline=light_baseline)
    # analyzer.show_propagation()
    analyzer.show_theoretical()
    # analyzer.show_propagation_high_res()

    plt.show()


if __name__ == '__main__':
    main()
