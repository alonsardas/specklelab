"""
This module tries to reconstruct Bessel function, this time using a different approach.
Here we encode the function on the SLM device, Fourier transform the field,
select the 1st diffraction order and then Fourier transform again, to get the desired function.

For more details, see:
V. Arrizón, U. Ruiz, R. Carrada, and L. González,
 "Pixelated phase computer holograms for the accurate encoding of scalar complex fields,"
 J. Opt. Soc. Am. A 24, 3500-3507 (2007).
https://doi.org/10.1364/JOSAA.24.003500
"""

import matplotlib.axes
import numpy as np
import scipy.special
from matplotlib import pyplot as plt

from opticalsimulator import Borders, BasicCamera, ZScanner, OpticalSystem, \
    GaussianLightSource, SLMMask, FourierLens, AngularMomentumPropagator, Mask
from opticalsimulator.simulations.alon.randomwalks import general_function_encoder
from opticalsimulator.simulations.alon.randomwalks.general_function_encoder import encode_function_on_slm
from opticalsimulator.simulations.alon.randomwalks.utils import FieldCamera, FieldCropper, AmplitudeFlattener, \
    analyse_z0_field

wavelength = 633e-6

should_analyse_z0_field = True
should_plot_input = False
should_propagate = True
should_plot_slm = False
debug_focal_plane = False


def simulate_using_slm(world_borders, world_x_pixels, world_y_pixels,
                       slm_borders: Borders, slm_x_pixels, slm_y_pixels,
                       alpha, sigma, d, angle, f,
                       spatial_mask_borders: Borders,
                       z0_shift, z_propagation,
                       gaussian_sigma, image_borders: Borders = None, fit_bounds=None):
    input_camera = None
    if should_plot_input:
        fig, axes = plt.subplots(1, 4)
        input_axes, focal_plane_ax, z0_ax, propagation_axes = axes
        input_camera = BasicCamera('input', input_axes, live_update=False)
    else:
        fig, axes = plt.subplots(1, 3)
        focal_plane_ax, z0_ax, propagation_axes = axes
    assert isinstance(focal_plane_ax, matplotlib.axes.Axes)
    assert isinstance(z0_ax, matplotlib.axes.Axes)
    assert isinstance(propagation_axes, matplotlib.axes.Axes)

    light_source = GaussianLightSource(0, 0, gaussian_sigma, wavelength=wavelength)

    slm_device = SLMMask(slm_borders, slm_x_pixels, slm_y_pixels, dark_outside_borders=True)

    def x_bessel_func(xs, ys):
        return scipy.special.jv(0, alpha * xs) * np.exp(-(xs ** 2 + ys ** 2) / (2 * sigma ** 2))

    beam_center_x = beam_center_y = 0

    center_on_diffraction = False
    encode_function_on_slm(slm_device, x_bessel_func, d, angle, beam_center_x, beam_center_y,
                           center_on_1st_diffraction=center_on_diffraction)

    if should_plot_slm:
        fig, slm_axes = plt.subplots()
        slm_axes.set_title('slm mask')
        np.mod(slm_device.phase_grid + 0.1, 2 * np.pi, out=slm_device.phase_grid)
        slm_device.phase_grid[0, 0] = 100  # For checking where is the corner
        im = slm_axes.imshow(slm_device.phase_grid, vmin=0, vmax=2 * np.pi)
        fig.colorbar(im, ax=slm_axes)

    fourier_lens1 = FourierLens(f)
    focal_plane_camera = BasicCamera('focal plane', focal_plane_ax, live_update=False)
    spatial_mask = Mask(spatial_mask_borders)
    fourier_lens2 = FourierLens(f)

    z0_field_recorder = FieldCamera()
    z0_camera = BasicCamera('z=0 plane', z0_ax, live_update=False)
    z0_ax.set_xlabel('x mm')
    z0_ax.set_ylabel('y mm')

    z_scanner = ZScanner(z_propagation, 100, propagation_axes)
    propagation_axes.set_xlabel('x mm')
    propagation_axes.set_ylabel('z mm - direction of propagation')

    simulator = OpticalSystem(world_borders, world_x_pixels, world_y_pixels, light_source)

    if should_plot_input:
        simulator.add_device(input_camera)
    simulator.add_device(slm_device)
    simulator.add_device(fourier_lens1)

    if debug_focal_plane:
        # For debugging
        # cropped_field = Borders(-0.05, -1, 1, 20)
        # cropped_field = Borders(0.2, 0.1, 20, 10)
        cropped_field = Borders(-10, -10, 10, 10)
        cropper = FieldCropper(cropped_field)
        simulator.add_device(cropper)
        flattener = AmplitudeFlattener(0.0000005)
        simulator.add_device(flattener)
        simulator.add_device(focal_plane_camera)

        simulator.simulate()
        return

    simulator.add_device(spatial_mask)

    shape_center_x, shape_center_y = 3.64, 1.19
    shape_width, shape_height = (11.5 - (-4.1)), (5.75 - (-2.9))
    # cropper = FieldCropper(Borders(-4.1, -2.9, 11.5, 5.75))
    cropper = FieldCropper(
        Borders(shape_center_x - shape_width / 2, shape_center_y - shape_height / 2, shape_center_x + shape_width / 2,
                shape_center_y + shape_height / 2))
    simulator.add_device(cropper)

    simulator.add_device(focal_plane_camera)

    simulator.add_device(fourier_lens2)

    # It seems that it takes some distance until the light is as desired
    if z0_shift != 0:
        light_propagator = AngularMomentumPropagator(z0_shift)
        simulator.add_device(light_propagator)

    simulator.add_device(z0_camera)
    simulator.add_device(z0_field_recorder)

    if should_propagate:
        cropper = FieldCropper(image_borders)
        simulator.add_device(cropper)
        simulator.add_device(z_scanner)
    simulator.simulate()

    if should_analyse_z0_field:
        analyse_z0_field(z0_field_recorder.field, None, (image_borders.min_x, image_borders.max_x), fit_bounds)


def simulate():
    # TODO: Use spec online:
    # https: // holoeye.com / slm - pluto - phase - only /
    slm_width, slm_height = 15.36, 8.64

    # Making large boundaries (in x axis) let us have more resolution in the far field (after Fourier is applied)
    world_width = slm_width * 4
    world_borders = Borders(-world_width / 2, -slm_height, world_width / 2, slm_height)
    world_x_pixels, world_y_pixels = 1920 * 4, 1080

    slm_borders = Borders(-slm_width / 2, -slm_height / 2, slm_width / 2, slm_height / 2)
    slm_x_pixels, slm_y_pixels = 1920, 1080

    global should_analyse_z0_field, should_propagate, should_plot_slm, debug_focal_plane
    should_analyse_z0_field = True
    should_propagate = True
    should_plot_slm = True
    general_function_encoder.should_plot = True
    should_debug_focal_plane = False

    alpha = 30
    sigma = 0.3
    # d = 64e-3  # As in the article
    d = 50e-3
    diffraction_angle = np.pi / 10
    f = 300  # As in the article

    # For d=100e-3
    spatial_mask_borders = Borders(0.38, 0.24, 4.14, 0.96)

    # For d=50e-3
    spatial_mask_borders = Borders(1, 0.4, 6, 1.9)

    # Trying to estimate the sizes in the focal plane
    freq_x = alpha / 8 * (2 * np.pi)  # ~8 is the size of one wave in Bessel function
    freq_y = 1 / sigma
    k_x = freq_x * 2 * np.pi
    k_y = freq_y * 2 * np.pi
    light_k = 2 * np.pi / wavelength
    focal_width = k_x * f / light_k
    focal_height = k_y * f / light_k
    focal_distance = 2 * np.pi / d * f / light_k
    focal_center_x = focal_distance * np.cos(diffraction_angle)
    focal_center_y = focal_distance * np.sin(diffraction_angle)

    print(focal_width, focal_height)
    print(spatial_mask_borders.width, spatial_mask_borders.height)
    print('------------------')
    print(focal_center_x, focal_center_y)
    print(spatial_mask_borders.center_x, spatial_mask_borders.center_y)
    # return
    # beam_center = 2.95
    # max_expand = 0.2

    # Note: Interestingly, the phases in z=0 plane aren't flat, yet the behavior is similar to the expected
    z0_shift = 7.2

    # z_propagation = 25  # As in the article
    z_propagation = 280

    # gaussian_sigma = 5  # Reasonable
    gaussian_sigma = 500

    fit_bounds = ((10, -1), (100, 1))
    # image_borders = Borders(-1, -1, 1, 1)
    # fit_bounds = None
    image_borders = Borders(-2, -1, 2, 1)

    simulate_using_slm(world_borders, world_x_pixels, world_y_pixels,
                       slm_borders, slm_x_pixels, slm_y_pixels,
                       alpha, sigma, d, diffraction_angle, f, spatial_mask_borders, z0_shift, z_propagation,
                       gaussian_sigma,
                       image_borders, fit_bounds)


def main():
    simulate()
    plt.show()


if __name__ == '__main__':
    main()
