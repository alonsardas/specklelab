"""
This module encode a general (complex) function onto a phase-only SLM-Device.
The method used is described here:

J. Davis, D. Cottrell, J. Campos, M. Yzuel, and I. Moreno, "Encoding amplitude information onto phase-only filters,"
 Appl. Opt.  38, 5004-5013 (1999).
 https://www.osapublishing.org/ao/abstract.cfm?uri=ao-38-23-5004

Another article, describing the same method and 2 more is:
V. Arrizón, U. Ruiz, R. Carrada, and L. González,
 "Pixelated phase computer holograms for the accurate encoding of scalar complex fields,"
 J. Opt. Soc. Am. A  24, 3500-3507 (2007).
 https://doi.org/10.1364/JOSAA.24.003500
"""
import matplotlib.axes
import matplotlib.figure
import numpy as np
import scipy.special
from matplotlib import pyplot as plt
from scipy.interpolate import interp1d
from scipy.optimize import fsolve

from opticalsimulator import SLMMask
from opticalsimulator.simulations.alon.slm.basedevice import BaseSLMDevice

should_plot = False


def get_phases_for_slm1(slm_device: SLMMask, func, d=0.0, grating_angle=0.0, beam_center_x=0, beam_center_y=0,
                        center_on_1st_diffraction=False) -> np.ndarray:
    physical_xs = slm_device.active_borders.min_x + slm_device.x_pixel_indexes * slm_device.pixel_size_x
    physical_ys = slm_device.active_borders.min_y + slm_device.y_pixel_indexes * slm_device.pixel_size_y
    values: np.ndarray = func(physical_xs, physical_ys)

    M = np.abs(values)
    phi = np.angle(values)

    add_grating(phi, physical_xs, physical_ys, d, grating_angle)

    np.mod(phi, 2 * np.pi, out=phi)

    # Normalize M
    M /= M.max()

    phases = calc_f_a1(M) * phi
    # phases = M * phi
    # phases = phi

    if center_on_1st_diffraction:
        add_grating(phases, physical_xs, physical_ys, -d, grating_angle)
        phases = np.mod(phases + np.pi, 2 * np.pi) - np.pi

    if should_plot:
        _plot_func_M_phi(M, phi, values)

    return phases


def get_phases_for_slm1_old(slm_device: SLMMask, func, d=0.0, grating_angle=0.0, beam_center_x=0, beam_center_y=0,
                            center_on_1st_diffraction=False) -> np.ndarray:
    physical_xs = slm_device.active_borders.min_x + slm_device.x_pixel_indexes * slm_device.pixel_size_x
    physical_ys = slm_device.active_borders.min_y + slm_device.y_pixel_indexes * slm_device.pixel_size_y
    values: np.ndarray = func(physical_xs, physical_ys)
    values = values.astype('complex128')

    shift_using_fourier(values, slm_device, -beam_center_x, -beam_center_y, physical_xs, physical_ys)

    fft_values = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(values)))

    M = np.abs(fft_values)
    phi = np.angle(fft_values)

    add_grating(phi, physical_xs, physical_ys, d, grating_angle)

    # Normalize M
    M /= M.max()

    phases = calc_f_a1(M) * phi

    if center_on_1st_diffraction:
        add_grating(phases, physical_xs, physical_ys, -d, grating_angle)
        phases = np.mod(phases, 2 * np.pi)

    if should_plot:
        _plot_func_M_phi(M, phi, values)

    return phases


def _plot_func_M_phi(M, phi, values):
    fig, axes = plt.subplots(3, 1)
    func_axes = axes[0]
    M_axes = axes[1]
    phi_axes = axes[2]
    assert isinstance(func_axes, matplotlib.axes.Axes)
    assert isinstance(M_axes, matplotlib.axes.Axes)
    assert isinstance(phi_axes, matplotlib.axes.Axes)
    assert isinstance(fig, matplotlib.figure.Figure)
    func_axes.set_title('function to encode')
    im = func_axes.imshow(np.abs(values))
    fig.colorbar(im, ax=func_axes)

    M_axes.set_title('M')
    im = M_axes.imshow(M, vmin=0)
    fig.colorbar(im, ax=M_axes)

    phi_axes.set_title('phi')
    im = phi_axes.imshow(np.mod(phi, np.pi * 2), vmin=0, vmax=2 * np.pi)
    fig.colorbar(im, ax=phi_axes)


def _plot_phase_mask(phase_mask):
    fig, slm_axes = plt.subplots()
    slm_axes.set_title('slm mask')
    im = slm_axes.imshow(phase_mask, cmap='gray', vmin=0, vmax=2 * np.pi)
    fig.colorbar(im, ax=slm_axes)


def get_phases_for_slm4(slm_device: SLMMask, func, d=0.0, grating_angle=0.0, beam_center_x=0, beam_center_y=0,
                        center_on_1st_diffraction=False) -> np.ndarray:
    physical_xs = slm_device.active_borders.min_x + slm_device.x_pixel_indexes * slm_device.pixel_size_x
    physical_ys = slm_device.active_borders.min_y + slm_device.y_pixel_indexes * slm_device.pixel_size_y
    values: np.ndarray = func(physical_xs, physical_ys)
    values = values.astype('complex128')

    shift_using_fourier(values, slm_device, beam_center_x, beam_center_y, physical_xs, physical_ys)

    fft_values = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(values)))

    M = np.abs(fft_values)
    phi = np.angle(fft_values)

    # Normalize M
    M /= M.max()

    add_grating(phi, physical_xs, physical_ys, d, grating_angle)
    np.mod(phi, 2 * np.pi, out=phi)

    # phases = M * phi
    phases = calc_f_a1(M) * phi
    # phases /= phases.max() * 2 * np.pi
    # phases = np.mod(phases, 2 * np.pi)

    # Is this what they mean in phase depth???
    # eps = 1e-9
    # phases = np.mod(phases, 2 * np.pi * M + eps)

    if center_on_1st_diffraction:
        add_grating(phases, physical_xs, physical_ys, -d, grating_angle)
        phases = np.mod(phases, 2 * np.pi)

    if should_plot:
        _plot_func_M_phi(M, phi, values)

    return phases


def add_grating(phi, xs, ys, d, grating_angle):
    # Adding linear phase to select diffraction order
    if d:
        phi += 2 * np.pi / d * (xs * np.cos(grating_angle) + ys * np.sin(grating_angle))


def shift_using_fourier(values, slm_device, beam_center_x, beam_center_y, xs, ys):
    # Add shift in the Fourier, by adding modulation
    # This describes what is the difference in frequencies between adjacent pixels,
    #  based on fftfreq
    freq_unit_x = 1 / (slm_device.pixels_x * slm_device.pixel_size_x)
    freq_unit_y = 1 / (slm_device.pixels_y * slm_device.pixel_size_y)

    # slm_center_x = slm_device.borders.center_x
    # slm_center_y = slm_device.borders.center_y
    slm_center_x = 0
    slm_center_y = 0

    shift_x = (beam_center_x - slm_center_x) / slm_device.pixel_size_x * freq_unit_x
    shift_y = (beam_center_y - slm_center_y) / slm_device.pixel_size_x * freq_unit_y
    values *= np.exp(-1j * 2 * np.pi * (shift_x * xs + shift_y * ys))


interpolated_inv_sinc = None


def calc_f_a1(a):
    global interpolated_inv_sinc
    if interpolated_inv_sinc is None:
        interpolated_inv_sinc = _create_interpolated_inv_sinc()
    return interpolated_inv_sinc(a)


def _create_interpolated_inv_sinc():
    xs = np.linspace(0, 1, 150)
    ys = np.zeros(len(xs))
    for i, x in enumerate(xs[1:]):
        def eq(f_a):
            # See Eq. 11
            LHS = _sinc(1 - f_a)
            RHS = x
            return LHS - RHS

        ys[i + 1] = fsolve(eq, x)
    ys[0] = 0

    return interp1d(xs, ys, kind='cubic')


def _sinc(x):
    if abs(x) < 1e-6:
        return 1
    return np.sin(np.pi * x) / (np.pi * x)


interpolated_inv_J1 = None
A = 0.5819  # Based on the "Pixelated phase computer holograms..." paper


def calc_f_a3(a):
    global interpolated_inv_J1
    if interpolated_inv_J1 is None:
        interpolated_inv_J1 = _interpolated_inv_J1()
    return interpolated_inv_J1(a)


def _interpolated_inv_J1():
    xs = np.linspace(0, 1, 300)
    ys = np.zeros(len(xs))
    for i, x in enumerate(xs[:-1]):
        def eq(f_a):
            # See Eq. 18
            LHS = scipy.special.jv(1, f_a)
            RHS = A * x
            return LHS - RHS

        ys[i] = fsolve(eq, x)
    ys[-1] = 1.841

    return interp1d(xs, ys, kind='cubic')


def encode_function_on_slm(slm_device: SLMMask, func, d=0.0, grating_angle=0.0, beam_center_x=0, beam_center_y=0,
                           center_on_1st_diffraction=False):
    phases = get_phases_for_slm4(slm_device, func, d, grating_angle, beam_center_x, beam_center_y,
                                 center_on_1st_diffraction)
    slm_device.update_phase(phases)


class Encoder(object):
    def __init__(self):
        self.diffraction_grating_d = 0
        self.diffraction_grating_angle = 0

        self.general_grating_d = 0
        self.general_grating_angle = 0

        self.beam_center_x = 0
        self.beam_center_y = 0

        self.global_phase = 0

        self._methods_dict = {0: self.psi0,
                              1: self.psi1,
                              3: self.psi3}

        self.should_plot_debug = False
        self.should_plot_phase_mask = False

    def set_diffraction_grating(self, d, angle):
        self.diffraction_grating_d = d
        self.diffraction_grating_angle = angle

    def set_general_grating(self, d, angle):
        self.general_grating_d = d
        self.general_grating_angle = angle

    def set_beam_center(self, beam_center_x, beam_center_y):
        self.beam_center_x = beam_center_x
        self.beam_center_y = beam_center_y

    def encode_func(self, slm_device: SLMMask, func, method):
        slm_borders = slm_device.active_borders
        physical_xs = slm_borders.min_x + slm_device.x_pixel_indexes * slm_device.pixel_size_x
        physical_ys = slm_borders.min_y + slm_device.y_pixel_indexes * slm_device.pixel_size_y

        physical_xs += slm_borders.center_x - self.beam_center_x
        physical_ys += slm_borders.center_y - self.beam_center_y

        values: np.ndarray = func(physical_xs, physical_ys)

        self.encode_values(slm_device, physical_xs, physical_ys, values, method)

    def encode_fourier(self, slm_device: BaseSLMDevice, func, method, f, wavelength):

        physical_xs = -slm_device.width / 2 + slm_device.x_pixel_indexes * slm_device.pixel_size_x
        physical_ys = -slm_device.height / 2 + slm_device.y_pixel_indexes * slm_device.pixel_size_y

        light_k = 2 * np.pi / wavelength

        freq_x = np.fft.fftfreq(slm_device.phase_grid.shape[1], d=slm_device.pixel_size_x)
        freq_y = np.fft.fftfreq(slm_device.phase_grid.shape[0], d=slm_device.pixel_size_y)

        freq_x = np.fft.fftshift(freq_x)
        freq_y = np.fft.fftshift(freq_y)

        freq_xs, freq_ys = np.meshgrid(freq_x, freq_y)

        k_xs = freq_xs * 2 * np.pi
        k_ys = freq_ys * 2 * np.pi

        focal_plane_xs = k_xs * f / light_k
        focal_plane_ys = k_ys * f / light_k

        fft_values: np.ndarray = func(focal_plane_xs, focal_plane_ys)
        fft_values = fft_values.astype('complex128')

        shift_using_fourier(fft_values, slm_device, self.beam_center_x, self.beam_center_y, physical_xs, physical_ys)

        if self.should_plot_debug:
            fig, axes = plt.subplots(1, 2)
            M_axes, phi_axes = axes
            assert isinstance(M_axes, matplotlib.axes.Axes)
            assert isinstance(phi_axes, matplotlib.axes.Axes)
            assert isinstance(fig, matplotlib.figure.Figure)

            M_axes.set_title('function amplitude')
            im = M_axes.imshow(np.abs(fft_values), vmin=0)
            fig.colorbar(im, ax=M_axes)

            phi_axes.set_title('phi')
            im = phi_axes.imshow(np.angle(fft_values) + np.pi, vmin=0, vmax=2 * np.pi)
            fig.colorbar(im, ax=phi_axes)

        values = np.fft.fftshift(np.fft.ifft2(np.fft.ifftshift(fft_values)))
        self.encode_values(slm_device, physical_xs, physical_ys, values, method)

    def encode_values(self, slm_device, physical_xs, physical_ys, values, method):
        M = np.abs(values)
        phi = np.angle(values)
        # Normalize M
        M /= M.max()
        add_grating(phi, physical_xs, physical_ys, self.diffraction_grating_d, self.diffraction_grating_angle)
        phi = np.mod(phi + np.pi, 2 * np.pi, out=phi) - np.pi
        slm_device.phase_grid = self._methods_dict[method](phi, M)
        add_grating(slm_device.phase_grid, physical_xs, physical_ys, self.general_grating_d, self.general_grating_angle)
        slm_device.phase_grid += self.global_phase
        np.mod(slm_device.phase_grid, 2 * np.pi, out=slm_device.phase_grid)

        if self.should_plot_debug:
            _plot_func_M_phi(M, phi, values)

        if self.should_plot_phase_mask:
            _plot_phase_mask(slm_device.phase_grid)

    def psi0(self, phi, a):
        return a * phi

    def psi1(self, phi, a):
        return calc_f_a1(a) * phi

    def psi3(self, phi, a):
        return calc_f_a3(a) * np.sin(phi)


def test_f_a3():
    fig, ax = plt.subplots()
    xs = np.linspace(0, 1, 400)
    ys = calc_f_a3(xs)
    ax.plot(xs, ys)
    plt.show()


if __name__ == '__main__':
    test_f_a3()
