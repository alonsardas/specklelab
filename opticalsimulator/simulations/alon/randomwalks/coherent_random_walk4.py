"""
This time, using the Encoder class
"""
import matplotlib.axes
import numpy as np
import scipy.special
from matplotlib import pyplot as plt

from opticalsimulator import Borders, BasicCamera, ZScanner, OpticalSystem, \
    GaussianLightSource, SLMMask, FourierLens, AngularMomentumPropagator
from opticalsimulator.simulations.alon.randomwalks.general_function_encoder import Encoder
from opticalsimulator.simulations.alon.randomwalks.utils import FieldCamera, FieldCropper, analyse_z0_field
from opticalsimulator.simulations.alon.slm.simulationdevice import SimulationSLMDevice

wavelength = 633e-6

should_analyse_z0_field = True
should_plot_input = False
should_propagate = False


def simulate_using_slm(simulator, slm_device,
                       alpha, sigma, encoder: Encoder, encoding_method, f,
                       camera_roi, z0_shift, z_propagation,
                       image_borders=None, fit_bounds=None):
    input_camera = None
    if should_plot_input:
        fig, axes = plt.subplots(1, 3)
        input_axes, z0_ax, propagation_axes = axes
        input_camera = BasicCamera('input', input_axes, live_update=False)
    else:
        fig, axes = plt.subplots(1, 2)
        z0_ax, propagation_axes = axes
    assert isinstance(z0_ax, matplotlib.axes.Axes)
    assert isinstance(propagation_axes, matplotlib.axes.Axes)

    def x_bessel_func(xs, ys):
        # return np.ones(xs.shape)
        return scipy.special.jv(0, alpha * xs) * np.exp(-(xs ** 2 + ys ** 2) / (2 * sigma ** 2))

    encoder.encode_fourier(slm_device, x_bessel_func, encoding_method, f, wavelength)

    fourier_lens = FourierLens(f)

    cropper = FieldCropper(camera_roi)
    z0_field_recorder = FieldCamera()
    z0_camera = BasicCamera('z=0 plane', z0_ax, camera_roi, live_update=False)
    z0_ax.set_xlabel('x mm')
    z0_ax.set_ylabel('y mm')

    z_scanner = ZScanner(z_propagation, 100, propagation_axes)
    propagation_axes.set_xlabel('x mm')
    propagation_axes.set_ylabel('z mm - direction of propagation')

    if should_plot_input:
        simulator.add_device(input_camera)

    simulator.add_device(slm_device)
    simulator.add_device(fourier_lens)

    simulator.add_device(cropper)

    # It seems that it takes some distance until the light is as desired
    if z0_shift != 0:
        light_propagator = AngularMomentumPropagator(z0_shift)
        simulator.add_device(light_propagator)

    simulator.add_device(z0_camera)
    simulator.add_device(z0_field_recorder)

    if should_propagate:
        cropper = FieldCropper(image_borders)
        simulator.add_device(cropper)
        simulator.add_device(z_scanner)
    simulator.simulate()

    if should_analyse_z0_field:
        analyse_z0_field(z0_field_recorder.field, None, (image_borders.min_x, image_borders.max_x), fit_bounds)


def simulate():
    slm_width, slm_height = 15.9, 12.8
    slm_x_pixels, slm_y_pixels = 1272, 1024

    # Making large boundaries (in x axis) let us have more resolution in the far field (after Fourier is applied)
    world_width = slm_width * 4
    world_borders = Borders(-world_width / 2, -slm_height, world_width / 2, slm_height)
    world_x_pixels, world_y_pixels = slm_x_pixels * 4, slm_y_pixels

    slm_borders = Borders(-slm_width / 2, -slm_height / 2, slm_width / 2, slm_height / 2)
    slm_device = SimulationSLMDevice(slm_borders, slm_x_pixels, slm_y_pixels, dark_outside=True)

    global should_analyse_z0_field, should_propagate
    should_analyse_z0_field = True
    should_propagate = True

    alpha = 80
    sigma = 0.6

    d = 200e-3
    diffraction_angle = np.pi / 4
    f = 300  # As in the article

    encoder = Encoder()
    encoder.should_plot_debug = True
    encoder.should_plot_phase_mask = True
    encoder.set_diffraction_grating(d, diffraction_angle)
    encoder.set_general_grating(-d / 0.2, diffraction_angle)
    encoder.global_phase = 1.2

    camera_size = 1
    center_x = center_y = 0.0
    # camera_roi = get_borders_around_center(center_x, center_y, camera_size, camera_size)
    # camera_roi = world_borders
    # camera_roi = Borders(0.0, 0.0, 7, 7)
    camera_roi = Borders(0.3, 0.2, 0.8, 1)
    z0_shift = 0

    z_propagation = 25  # As in the article

    gaussian_sigma = 500
    # gaussian_sigma = 2
    light_source = GaussianLightSource(0, 0, gaussian_sigma, wavelength=wavelength)

    simulator = OpticalSystem(world_borders, world_x_pixels, world_y_pixels, light_source)

    fit_bounds = ((10, 0.5), (600, 0.6))
    image_borders = Borders(-2, -1, 2, 1)

    encoding_method = 1

    simulate_using_slm(simulator, slm_device,
                       alpha, sigma, encoder, encoding_method, f,
                       camera_roi, z0_shift, z_propagation,
                       image_borders, fit_bounds)

    print(f'phase range: [{slm_device.phase_grid.min()} - {slm_device.phase_grid.max()}]')


def test_beam_center():
    slm_width = 15.9
    slm_height = 12.8

    slm_borders = Borders(-slm_width / 2, -slm_height / 2, slm_width / 2, slm_height / 2)
    # slm_x_pixels = 1200
    slm_x_pixels = 1272
    slm_y_pixels = 1024
    slm_device = SLMMask(slm_borders, slm_x_pixels, slm_y_pixels)

    encoder = Encoder()
    encoder.set_beam_center(1, 3)
    encoder.should_plot_phase_mask = True
    encoder.should_plot_debug = True
    encoder.set_diffraction_grating(0.1, 0)
    encoder.global_phase = 0.1

    encoder.encode_fourier(slm_device, lambda xs, ys: 1e9 * np.exp(- (xs / 10) ** 2 - (ys / 10) ** 2), 1, 1e5, 100e-6)


def main():
    simulate()
    # test_beam_center()
    plt.show()


if __name__ == '__main__':
    main()
