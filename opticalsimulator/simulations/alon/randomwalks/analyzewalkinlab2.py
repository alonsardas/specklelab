import os

import numpy as np
import scipy.special
from matplotlib import pyplot as plt

import opticalsimulator
from opticalsimulator import Borders, ZScanner, OpticalSystem, get_borders_around_center
from opticalsimulator.simulations.alon.randomwalks import walkinlab2, fitter
from opticalsimulator.simulations.alon.randomwalks.utils import BesselInitialLight
from opticalsimulator.simulations.alon.slm import labslm, lcosphasemask

# results_base_dir = r"C:\Users\Owner\Google Drive\People\Alon"


results_base_dir = os.path.join(opticalsimulator.__path__[0], r'../../Results')


class Analyzer(object):
    PIXEL_TO_MILLIMETER = 4.8e-3

    def __init__(self, basedir, max_distance, dz,
                 relevant_borders=Borders(170, 81, 214, 293), bessel_alpha=30, bessel_sigma=0.4):
        self.dir = basedir

        mask = labslm.configs[1]['active_mask_slice']
        self.slm1_borders = Borders(mask[1].start, mask[0].start, mask[1].stop, mask[0].stop)

        mask = labslm.configs[2]['active_mask_slice']
        self.slm2_borders = Borders(mask[1].start, mask[0].start, mask[1].stop, mask[0].stop)

        results_dir = os.path.join(results_base_dir, self.dir)
        # results_dir = '/run/user/1000/gvfs/google-drive:host=mail.huji.ac.il,user=complexphoton,prefix=%2F1seHVFR3jDlEaksVLq3uPrJq5Tt4pOYdm%2F1iiMca-5cVEqOBBjL22c4mG17G8S6_J_n%2F1Rc3u2U90XTQKAloHU9xBL2qnPKL-TKPH%2F17qS3w2YcnsDWlPgbf_Bqv98GJ7mGrgaR/1seHVFR3jDlEaksVLq3uPrJq5Tt4pOYdm/1iiMca-5cVEqOBBjL22c4mG17G8S6_J_n/1uGR-pgBr0Gf-a_ECHhLsjfesKdPRrGlz'

        self.results_dir = os.path.abspath(results_dir)
        self.output_dir = os.path.join(self.results_dir, 'analyzed')
        if not os.path.exists(self.output_dir):
            os.mkdir(self.output_dir)

        self.max_distance = max_distance
        self.dz = dz
        self.relevant_borders = relevant_borders
        self.zs = np.arange(0, max_distance, dz)

        self.bessel_alpha = bessel_alpha
        self.bessel_sigma = bessel_sigma

    def save_slm1_bessel(self):
        mask = lcosphasemask.load(os.path.join(self.results_dir, 'slm1_bessel.npz'))
        fig, ax = mask.plot()
        fig.savefig(os.path.join(self.output_dir, 'slm1_bessel.png'))
        mask.crop(self.slm1_borders)
        fig, ax = mask.plot()
        fig.savefig(os.path.join(self.output_dir, 'slm1_bessel-cropped.png'))

    def save_slm2_masks(self):
        for z in self.zs:
            mask_name = f'slm2_prop={z:.3f}.npz'
            mask = lcosphasemask.load(os.path.join(self.results_dir, mask_name))
            fig, ax = mask.plot()
            # fig.savefig(os.path.join(self.output_dir, f'slm2_prop={int(z)}.png'))
            plt.close(fig)
            mask.crop(self.slm2_borders)
            fig, ax = mask.plot()
            fig.savefig(os.path.join(self.output_dir, f'slm2_prop={z:.3f}-cropped.png'))
            plt.close(fig)

    def save_images(self):
        image_dir = os.path.join(self.output_dir, 'images')
        if not os.path.exists(image_dir):
            os.mkdir(image_dir)
        for z in self.zs:
            output_img_path = os.path.join(image_dir, f'z={z:.3f}.png')
            if os.path.exists(output_img_path):
                print(f'image {output_img_path} found')
                continue

            image_name = f'z={z:.3f}.npz'

            image = np.load(os.path.join(self.results_dir, image_name))['image']
            fig, ax = plt.subplots()
            im = ax.imshow(image)
            ax.set_title(f'z={z:.3f}')
            fig.colorbar(im, ax=ax)
            fig.savefig(output_img_path)
            plt.close(fig)

    def show_example_image(self, z=0):
        image = np.load(os.path.join(self.results_dir, f'z={z:.3f}.npz'))['image']
        fig, ax = plt.subplots()
        im = ax.imshow(image)
        fig.colorbar(im, ax=ax)
        plt.show()

    def show_propagation(self):
        propagation = np.zeros((len(self.zs), self.relevant_borders.height))
        for i, z in enumerate(self.zs):
            relevant = self._get_relevant_image(z)
            averaged = np.mean(relevant, axis=1)
            propagation[i, :] = averaged
        fig, ax = plt.subplots()
        ax.set_title('light propagation')
        ax.set_xlabel('pixels')
        ax.set_ylabel('propagation (mm)')
        im = ax.imshow(propagation, origin='lower', aspect='auto',
                       extent=(
                           self.relevant_borders.min_x, self.relevant_borders.max_x, self.zs[0], self.zs[-1] + self.dz))
        fig.savefig(os.path.join(self.output_dir, 'propagation-pixels-no-colorbar.png'))

        # Now with fixed units
        ax.set_xlabel('mm')
        width = self.relevant_borders.height * self.PIXEL_TO_MILLIMETER
        im.set_extent((-width / 2, width / 2, self.zs[0], self.zs[-1] + self.dz))
        fig.savefig(os.path.join(self.output_dir, 'propagation.png'))

    def show_theoretical(self):
        fig, ax = plt.subplots()

        width = self.relevant_borders.height * self.PIXEL_TO_MILLIMETER
        borders = get_borders_around_center(0, 0, width * 2, 1)
        x_pixels = 1000
        y_pixels = 5

        alpha_factor = 1  # Weirdly, alpha isn't what it's supposed to be

        light_source = BesselInitialLight(alpha=self.bessel_alpha * alpha_factor, sigma=self.bessel_sigma,
                                          wavelength=walkinlab2.wavelength)
        z_scanner = ZScanner(self.max_distance, 1000, ax)

        simulator = OpticalSystem(borders, x_pixels, y_pixels, light_source)
        simulator.add_device(z_scanner)
        simulator.simulate()

        ax.set_title('propagation in free-space, simulated')
        ax.set_xlabel('mm')
        ax.set_ylabel('propagation (mm)')
        ax.set_xlim(-width / 2, width / 2)
        fig.savefig(os.path.join(self.output_dir, 'simulated.png'))

    def fit_to_bessel(self, z=0):
        relevant = self._get_relevant_image(z)
        bessel = np.mean(relevant, axis=1)
        intensity = bessel / bessel.max()
        xs = self.PIXEL_TO_MILLIMETER * (np.arange(0, len(intensity)) - len(intensity) / 2)

        def func(x, alpha, sigma, center): return (scipy.special.jv(0, alpha * (x - center)) * np.exp(
            -x ** 2 / (2 * sigma ** 2))) ** 2

        relevant_xs = np.logical_and(xs > 0.01, xs<0.4)
        # relevant_xs = np.logical_and(relevant_xs, intensity < 0.4)  # To avoid taking values where the intensity is in saturation
        # xs = xs[relevant_xs]
        # intensity = intensity[relevant_xs]
        fit_params = fitter.FitParams(func, xs, intensity)
        # fit_params.bounds = ((5, 0.1, -0.5), (60, 2, 0.5))
        fit_params.bounds = ((5, 0.1, -0.5), (100, 2, 0.5))
        fit = fitter.FuncFit(fit_params)
        fit.print_results()
        fit.print_results_latex()

        fig, ax = plt.subplots()
        data_line = fit.plot_data(ax, 'intensity')
        data_line.set_marker('*')
        fit.plot_fit(ax, 'fit to bessel', 600)
        ax.legend()
        ax.set_xlabel('x mm')
        ax.set_ylabel('normalized intensity')
        fig.savefig(os.path.join(self.output_dir, f'fit_to_bessel-z={z:.3f}.png'))

    def show_relevant_area(self, z=0):
        relevant = self._get_relevant_image(z)
        fig, ax = plt.subplots()
        horizontal = relevant.swapaxes(0, 1)
        im = ax.imshow(horizontal)
        fig.colorbar(im, ax=ax)
        fig.savefig(os.path.join(self.output_dir, 'relevant_area.png'))

    def _get_relevant_image(self, z) -> np.ndarray:
        image_name = f'z={z:.3f}.npz'
        # image_name = f'z={int(z)}.npz'
        image = np.load(os.path.join(self.results_dir, image_name), allow_pickle=True)['image']
        return image[self.relevant_borders.min_y:self.relevant_borders.max_y,
               self.relevant_borders.min_x:self.relevant_borders.max_x]

    """
    expression = r'^slm2_prop=(\d+)\.npz$'
    for basename in os.listdir('/media/Windows/Users/Alon/Documents/Uni/Amirim/Results/2020_10_08_01_06_29')
        match =re.match(expression, basename)
        if not match:
            continue
        z = match.group(1)
        mask = lcosphasemask.load(os.path.join(results_dir, basename))
        fig, ax = mask.plot()
        fig.savefig(os.path.join(output_dir, 'slm1_bessel.png'))
        mask.crop(slm1_borders)
        fig, ax = mask.plot()
        fig.savefig(os.path.join(output_dir, 'slm1_bessel-cropped.png'))
    """


dir1 = '2020_10_08_01_06_29'
dir2 = '2020_10_08_01_18_35'
dir3 = '2020_10_08_02_51_04-bessel-ver1'
dir4 = '2020_10_09_16_04_00'
dir5 = '2020_10_09_16_20_10'
dir6 = '2020_10_09_19_30_50'
dir7 = '2020_10_09_20_09_27'
dir8 = '2020_10_09_20_24_05'
dir9 = '2020_10_09_21_40_27'
dir10 = '2020_10_12_12_03_30'
dir11 = '2020-10-12_12-30-17'
dir12 = '2020-10-14_23-10-44'
dir13 = '2020-10-15_02-09-05'


def main():
    # relevant_borders = Borders(185, 81, 196, 293)
    relevant_borders = Borders(329, 21, 343, 506)
    analyzer = Analyzer(dir13, 400, 5, bessel_alpha=30, bessel_sigma=0.6, relevant_borders=relevant_borders)
    # analyzer.show_propagation()
    # analyzer.show_theoretical()
    # analyzer.fit_to_bessel(z=0)
    # analyzer.show_relevant_area(z=0)
    analyzer.show_example_image(z=80)
    # analyzer.save_images()
    # analyzer.save_slm2_masks()
    # analyzer.save_slm1_bessel()
    plt.show()


if __name__ == '__main__':
    main()
