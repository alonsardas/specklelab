"""
This time, using the Encoder class
"""
import matplotlib.axes
import numpy as np
import scipy.special
from matplotlib import pyplot as plt

from opticalsimulator import Borders, BasicCamera, ZScanner, OpticalSystem, \
    GaussianLightSource, SLMMask, FourierLens, AngularMomentumPropagator, Mask
from opticalsimulator.simulations.alon.randomwalks.general_function_encoder import Encoder
from opticalsimulator.simulations.alon.randomwalks.utils import FieldCamera, FieldCropper, AmplitudeFlattener, \
    analyse_z0_field

wavelength = 633e-6

should_analyse_z0_field = True
should_plot_input = False
should_propagate = True
debug_focal_plane = False


def simulate_using_slm(simulator, slm_device,
                       alpha, sigma, encoder: Encoder, encoding_method, f, spatial_mask_borders, z0_shift,
                       z_propagation,
                       image_borders=None, fit_bounds=None):
    input_camera = None
    if should_plot_input:
        fig, axes = plt.subplots(1, 4)
        input_axes, focal_plane_ax, z0_ax, propagation_axes = axes
        input_camera = BasicCamera('input', input_axes, live_update=False)
    else:
        fig, axes = plt.subplots(1, 3)
        focal_plane_ax, z0_ax, propagation_axes = axes
    assert isinstance(focal_plane_ax, matplotlib.axes.Axes)
    assert isinstance(z0_ax, matplotlib.axes.Axes)
    assert isinstance(propagation_axes, matplotlib.axes.Axes)

    def x_bessel_func(xs, ys):
        return scipy.special.jv(0, alpha * xs) * np.exp(-(xs ** 2 + ys ** 2) / (2 * sigma ** 2))

    encoder.encode_func(slm_device, x_bessel_func, encoding_method)

    fourier_lens1 = FourierLens(f)
    focal_plane_camera = BasicCamera('focal plane', focal_plane_ax, live_update=False)
    spatial_mask = Mask(spatial_mask_borders)
    fourier_lens2 = FourierLens(f)

    z0_field_recorder = FieldCamera()
    z0_camera = BasicCamera('z=0 plane', z0_ax, live_update=False)
    z0_ax.set_xlabel('x mm')
    z0_ax.set_ylabel('y mm')

    z_scanner = ZScanner(z_propagation, 100, propagation_axes)
    propagation_axes.set_xlabel('x mm')
    propagation_axes.set_ylabel('z mm - direction of propagation')

    if should_plot_input:
        simulator.add_device(input_camera)

    simulator.add_device(slm_device)
    simulator.add_device(fourier_lens1)

    if debug_focal_plane:
        # For debugging
        # cropped_field = Borders(-0.05, -1, 1, 20)
        # cropped_field = Borders(0.2, 0.1, 20, 10)
        cropped_field = Borders(-10, -10, 10, 10)
        cropper = FieldCropper(cropped_field)
        simulator.add_device(cropper)
        flattener = AmplitudeFlattener(0.00000005)
        simulator.add_device(flattener)
        simulator.add_device(focal_plane_camera)

        simulator.simulate()
        return

    simulator.add_device(spatial_mask)

    # This is used to center on the image in the focal plane,
    # Without it, the propagation doesn't go along the z axis

    # Here we assume that the center of the shape is in the center of the mask
    shape_center_x, shape_center_y = spatial_mask.active_borders.center_x, spatial_mask.active_borders.center_y
    shape_width, shape_height = (7.56 - shape_center_x) * 2, (3.72 - shape_center_y) * 2
    cropper = FieldCropper(
        Borders(shape_center_x - shape_width / 2, shape_center_y - shape_height / 2,
                shape_center_x + shape_width / 2, shape_center_y + shape_height / 2))
    simulator.add_device(cropper)

    simulator.add_device(focal_plane_camera)

    simulator.add_device(fourier_lens2)

    # It seems that it takes some distance until the light is as desired
    if z0_shift != 0:
        light_propagator = AngularMomentumPropagator(z0_shift)
        simulator.add_device(light_propagator)

    simulator.add_device(z0_camera)
    simulator.add_device(z0_field_recorder)

    if should_propagate:
        cropper = FieldCropper(image_borders)
        simulator.add_device(cropper)
        simulator.add_device(z_scanner)
    simulator.simulate()

    if should_analyse_z0_field:
        analyse_z0_field(z0_field_recorder.field, None, (image_borders.min_x, image_borders.max_x), fit_bounds)


def simulate():
    slm_width, slm_height = 15.9, 12.8
    slm_x_pixels, slm_y_pixels = 1272, 1024

    # Making large boundaries (in x axis) let us have more resolution in the far field (after Fourier is applied)
    world_width = slm_width * 4
    world_borders = Borders(-world_width / 2, -slm_height, world_width / 2, slm_height)
    world_x_pixels, world_y_pixels = slm_x_pixels * 4, slm_y_pixels

    slm_borders = Borders(-slm_width / 2, -slm_height / 2, slm_width / 2, slm_height / 2)
    slm_device = SLMMask(slm_borders, slm_x_pixels, slm_y_pixels, dark_outside_borders=False)

    global should_analyse_z0_field, should_propagate, debug_focal_plane
    should_analyse_z0_field = True
    should_propagate = True
    debug_focal_plane = False

    alpha = 20
    # alpha = 125
    # sigma = 0.6
    sigma = 40

    # d = 64e-3  # As in the article
    d = 100e-3
    diffraction_angle = np.pi / 25
    f = 300  # As in the article

    encoder = Encoder()
    encoder.should_plot_debug = False
    encoder.should_plot_phase_mask = True
    encoder.set_diffraction_grating(d, diffraction_angle)
    encoder.global_phase = np.pi * 0.6

    # For d=50e-3
    spatial_mask_borders = Borders(1.065, 0.1188, 2.715, 0.355)

    print_focal_plane_info(alpha, d, diffraction_angle, f, sigma, spatial_mask_borders)
    # return
    # beam_center = 2.95
    # max_expand = 0.2

    # Note: Interestingly, the phases in z=0 plane aren't flat, yet the behavior is similar to the expected
    # z0_shift = 7.2
    z0_shift = 0

    # z_propagation = 25  # As in the article
    z_propagation = 650

    gaussian_sigma = 500
    # gaussian_sigma = 2
    light_source = GaussianLightSource(0, 0, gaussian_sigma, wavelength=wavelength)

    simulator = OpticalSystem(world_borders, world_x_pixels, world_y_pixels, light_source)

    fit_bounds = ((10, -1), (100, 1))
    # image_borders = Borders(-1, -1, 1, 1)
    # fit_bounds = None
    image_borders = Borders(-2, -1, 2, 1)

    encoding_method = 3

    simulate_using_slm(simulator, slm_device,
                       alpha, sigma, encoder, encoding_method, f, spatial_mask_borders, z0_shift, z_propagation,
                       image_borders, fit_bounds)

    print(f'phase range: [{slm_device.phase_grid.min()} - {slm_device.phase_grid.max()}]')


def print_focal_plane_info(alpha, d, diffraction_angle, f, sigma, spatial_mask_borders):
    # Trying to estimate the sizes in the focal plane
    freq_x = alpha / 8 * (2 * np.pi)  # ~8 is the size of one wave in Bessel function
    freq_y = 1 / sigma
    k_x = freq_x * 2 * np.pi
    k_y = freq_y * 2 * np.pi
    light_k = 2 * np.pi / wavelength
    focal_width = k_x * f / light_k
    focal_height = k_y * f / light_k
    focal_distance = 2 * np.pi / d * f / light_k
    focal_center_x = focal_distance * np.cos(diffraction_angle)
    focal_center_y = focal_distance * np.sin(diffraction_angle)
    print('comparing the size')
    print(focal_width, focal_height)
    print(spatial_mask_borders.width, spatial_mask_borders.height)
    print('------------------')
    print('estimating the center: ')
    print(focal_center_x, focal_center_y)
    print(spatial_mask_borders.center_x, spatial_mask_borders.center_y)
    expected_borders = Borders(focal_center_x - focal_width / 2, focal_center_y - focal_height / 2,
                               focal_center_x + focal_width / 2, focal_center_y + focal_height / 2)
    print(f'expected borders: {expected_borders}')


def test_beam_center():
    slm_width = 15.9
    slm_height = 12.8

    slm_borders = Borders(-slm_width / 2, -slm_height / 2, slm_width / 2, slm_height / 2)
    # slm_x_pixels = 1200
    slm_x_pixels = 1272
    slm_y_pixels = 1024
    slm_device = SLMMask(slm_borders, slm_x_pixels, slm_y_pixels, dark_outside_borders=True)

    encoder = Encoder()
    encoder.set_beam_center(6, -4)

    encoder.should_plot_debug = True
    encoder.should_plot_phase_mask = True

    encoder.encode_func(slm_device, lambda xs, ys: 1 / (1 + xs ** 2 + ys ** 2), 1)


def main():
    simulate()
    plt.show()


if __name__ == '__main__':
    main()
