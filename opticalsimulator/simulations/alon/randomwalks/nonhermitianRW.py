import matplotlib.axes
import numpy as np
from matplotlib import pyplot as plt

x = np.linspace(0, 100, 1000)
omega = 1

base_amp = 1
amp_ratio = 1

gaussian_sigma = 10
gaussian = np.exp(-(x - 50) ** 2 / (gaussian_sigma ** 2))

zs = np.linspace(0, 10, 10)

walk_image = np.zeros((len(zs), len(x)))


fig, ax = plt.subplots()
ax.legend()

for i, z in enumerate(zs):
    amp2 = base_amp * np.sqrt(amp_ratio)
    amp1 = base_amp / np.sqrt(amp_ratio)

    phases = ((np.sin(omega * x) >= 0) * amp1 + (np.sin(omega * x) < 0) * amp2) * np.cos(omega * x) * z
    ax.plot(x, phases, label=f'z={z}')
    start_field = gaussian * np.exp(1j * phases)
    field = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(start_field)))
    walk_image[i, :] = np.abs(field)**2

fig, ax = plt.subplots()
assert isinstance(ax, matplotlib.axes.Axes)
ax.imshow(walk_image, origin='lower', aspect='auto')
plt.show()
