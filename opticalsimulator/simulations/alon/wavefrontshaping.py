"""
This module uses SLM to compensate light scattering and gain focus in small area

The algorithm used of the optimization is called Random  partitioning  algorithm,
and it is described here:
I. Vellekoop, "Feedback-based wavefront shaping," Opt. Express 23, 12189-12206 (2015).
"""
import time

import matplotlib.axes
import numpy as np
from matplotlib import pyplot as plt
from scipy import ndimage

from opticalsimulator import RealSLMDevice, Borders, Camera, FieldGrid, OpticalSystem, VimbaCamera, FourierLens, \
    GaussianLightSource
from oldslmdevice import SpeckleSLMDevice, SLMDevice

class SpeckleFixerSLMDevice(SLMDevice):
    def __init__(self, borders: Borders, speckle_size, ax: matplotlib.axes.Axes):
        self.borders = borders
        self.speckle_size = speckle_size
        x_speckles = int(np.ceil((self.borders.max_x - self.borders.min_x) / speckle_size))
        y_speckles = int(np.ceil((self.borders.max_y - self.borders.min_y) / speckle_size))
        self.phase_map = np.zeros((y_speckles, x_speckles))
        self.ax = ax

    def get_phases(self, xs, ys):
        x_indexes = xs / self.speckle_size
        y_indexes = ys / self.speckle_size
        x_indexes: np.ndarray = x_indexes.astype('int')
        y_indexes: np.ndarray = y_indexes.astype('int')
        return self.phase_map[x_indexes, y_indexes]

    def put_phase_map(self, new_phase_map):
        self.phase_map = np.mod(new_phase_map, 2 * np.pi)
        self.ax.clear()
        self.ax.imshow(self.phase_map, cmap='gray', vmin=0, vmax=2 * np.pi)


class RealSLMDeviceAdapter(object):
    def __init__(self):
        self.device = RealSLMDevice()
        self.phase_map = np.zeros((1024, 1272))
        self.diffusion_phase_map = self.create_diffusive_phase_mask()

    def create_diffusive_phase_mask(self):
        super_pixel_size = 5
        diffusion = np.random.random((int(1024 / super_pixel_size), int(1272 / super_pixel_size))) * 2 * np.pi
        zoom = np.array(self.phase_map.shape) / np.array(diffusion.shape)
        return ndimage.zoom(diffusion, zoom, order=0)

    def put_phase_map(self, new_phase_map):
        # Fixing the sizes
        zoom = np.array(self.phase_map.shape) / np.array(new_phase_map.shape)
        self.phase_map = ndimage.zoom(new_phase_map, zoom, self.phase_map, order=0)

    def apply(self, f):
        self.device.update_phase(self.phase_map + self.diffusion_phase_map)
        time.sleep(0.2)


class SpecklesCamera(Camera):
    def __init__(self, name, ax: matplotlib.axes.Axes):
        self.name = name
        self.ax = ax
        self.last_picture = None

    def show(self, f: FieldGrid):
        intensity = np.abs(f.Es) ** 2
        # intensity *= 1e-50
        # intensity = cv2.resize(intensity, (640, 480))
        self.last_picture = intensity
        # self.ax.pcolormesh(f.Xs, f.Ys, intensity, shading='nearest')
        # self.ax.imshow(intensity[95:125, 95:125])

        # Note: Using imshow without clear causes memory leak
        self.ax.clear()
        self.ax.imshow(intensity, vmin=0, vmax=1000)

    def get_last_picture(self):
        return self.last_picture


class RealCameraAdapter(SpecklesCamera):
    def __init__(self, ax: matplotlib.axes.Axes):
        self.camera = VimbaCamera(1, exposure_time=50e3)
        self.ax = ax

    def show(self, f: FieldGrid):
        im = self.camera.get_image()
        self.ax.clear()
        self.ax.imshow(im)
        self.last_picture = im

    def get_last_picture(self):
        return self.last_picture


class OptimizationSummary(object):
    def __init__(self, iteration, intensities):
        self.iteration = iteration
        self.intensities = intensities

    def plot_intensity_vs_iteration(self, ax: matplotlib.axes.Axes):
        ax.plot(self.iteration, self.intensities, '.')
        ax.set_title('intensity in desired area vs iteration number')
        ax.set_xlabel('iteration')
        ax.set_ylabel('intensity')


class SpecklesOptimizer(object):
    PHASES_TO_CHECK = 10

    def __init__(self, borders: Borders, roi: Borders, pixel_size, simulator: OpticalSystem,
                 slm_device: SpeckleFixerSLMDevice,
                 camera: SpecklesCamera, fig):
        self.borders = borders
        self.roi = roi
        self.pixel_size = pixel_size
        self.x_pixels = int((self.borders.max_x - self.borders.min_x) / pixel_size) + 1
        self.y_pixels = int((self.borders.max_y - self.borders.min_y) / pixel_size) + 1
        self.phase_map:np.ndarray = np.zeros((self.y_pixels, self.x_pixels))
        self.simulator = simulator
        self.slm_device = slm_device
        self.camera = camera
        self.last_intensity = 0
        self.fig = fig

    def partitioning_optimize(self, iterations=100) -> OptimizationSummary:
        intensity_per_iteration = np.zeros(iterations)
        for i in range(iterations):
            print(f'iteration {i}')
            self.fig.canvas.draw()
            self.fig.canvas.flush_events()

            partition_indexes = np.random.choice([False, True], self.phase_map.shape)
            best_phase = self._find_best_phase(partition_indexes)
            print(f'new intensity {self.last_intensity}')
            intensity_per_iteration[i] = self.last_intensity
            self.phase_map[partition_indexes] += best_phase
            self.phase_map = np.mod(self.phase_map, 2 * np.pi)
        return OptimizationSummary(np.arange(1, iterations + 1), intensity_per_iteration)

    def single_pixel_optimize(self):
        intensity_per_iteration = np.zeros(self.x_pixels*self.y_pixels)
        for x_index in range(self.x_pixels):
            for y_index in range(self.y_pixels):
                print(f'optimizing {x_index}, {y_index}')
                self.fig.canvas.draw()
                self.fig.canvas.flush_events()

                partition_indexes = np.zeros(self.phase_map.shape, 'bool')
                partition_indexes[x_index, y_index] = True
                best_phase = self._find_best_phase(partition_indexes)
                print(f'new intensity {self.last_intensity}')
                intensity_per_iteration[x_index*self.y_pixels + y_index] = self.last_intensity
                self.phase_map[partition_indexes] += best_phase
                self.phase_map = np.mod(self.phase_map, 2 * np.pi)
        return OptimizationSummary(np.arange(1, len(intensity_per_iteration) + 1), intensity_per_iteration)

    def _find_best_phase(self, pixels_to_set):
        phases = np.linspace(0, np.pi * 2, self.PHASES_TO_CHECK + 1)
        # Removing first phase, since it is 0, and we already checked it previously
        # Removing last phase because it is 2*pi, which is equivalent to 0
        phases = phases[1:-1]
        d_phase = phases[1] - phases[0]
        intensities = np.zeros(len(phases))
        for i, phase in enumerate(phases):
            self.phase_map[pixels_to_set] += d_phase
            self.slm_device.put_phase_map(self.phase_map)
            self.simulator.simulate()
            # self.fig.canvas.draw()
            # self.fig.canvas.flush_events()
            picture = self.camera.last_picture
            intensities[i] = self._calc_intensity(picture)

        # Complete a while loop - adding d_phase again mean adding in total 2*pi to the phase map,
        # meaning that we didn't change anything
        self.phase_map[pixels_to_set] += d_phase
        phases = np.append(0, phases)
        intensities = np.append(self.last_intensity, intensities)
        new_phase = self._find_best_phase_by_intensities(phases, intensities)

        # Calculate the actual intensity
        self.simulator.simulate()
        picture = self.camera.last_picture
        new_intensity = self._calc_intensity(picture)

        if np.all(new_intensity >= intensities):
            self.last_intensity = new_intensity
            return new_phase
        else:
            # If the maximum intensity was actually found in one of the checked phases
            max_index = np.argmax(intensities)
            self.last_intensity = intensities[max_index]
            return phases[max_index]

    def _calc_intensity(self, picture):
        return np.sum(picture[self.roi.min_y:self.roi.max_y, self.roi.min_x:self.roi.max_x])

    def _find_best_phase_by_intensities(self, phases: np.ndarray, intensities: np.ndarray):
        print(repr(phases), repr(intensities))
        A = np.sum(intensities * np.exp(1j * phases))
        return np.angle(A)


def simulate_focus_speckles():
    fig, axes = plt.subplots(1, 3)
    slm_ax, before_ax, after_ax = axes

    slm_ax.set_title('SLM Device')

    borders = Borders(-1, -1, 1, 1)

    x_pixels = 500
    y_pixels = 500

    diffuser_pixel_size = 0.01
    slm_pixel_size = 0.2

    light_source = GaussianLightSource(sigma=0.03)
    # input_camera = BasicCamera('input', axes[0])
    diffuser = SpeckleSLMDevice(borders, diffuser_pixel_size)
    slm_fixer = SpeckleFixerSLMDevice(borders, slm_pixel_size, axes[0])
    fourier_lens = FourierLens()
    output_camera = SpecklesCamera('output', None)

    simulator = OpticalSystem(borders, x_pixels, y_pixels, light_source)
    simulator.add_device(diffuser)
    simulator.add_device(slm_fixer)
    simulator.add_device(fourier_lens)
    simulator.add_device(output_camera)

    # Creating 'before' (original) image
    output_camera.ax = before_ax
    simulator.simulate()
    output_camera.ax = after_ax

    plt.show(block=False)
    roi = Borders(300, 150, 303, 153)  # In pixels...
    optimizer = SpecklesOptimizer(borders, roi, slm_pixel_size, simulator, slm_fixer, output_camera, fig)
    # summary = optimizer.partitioning_optimize(150)
    summary = optimizer.single_pixel_optimize()

    fig, ax = plt.subplots()
    summary.plot_intensity_vs_iteration(ax)

    plt.show()


class MockOpticalSystem(OpticalSystem):
    def __init__(self):
        self.devices = []

    def simulate(self):
        for device in self.devices:
            device.prop_through(None)


def focus_speckles_in_lab():
    fig, axes = plt.subplots(1, 2)
    before_ax, after_ax = axes

    slm_fixer = RealSLMDeviceAdapter()
    output_camera = RealCameraAdapter(None)

    simulator = MockOpticalSystem()
    simulator.add_device(slm_fixer)
    simulator.add_device(output_camera)

    # Creating 'before' (original) image
    output_camera.ax = before_ax
    print('before simulating')
    simulator.simulate()
    print('after simulating')
    output_camera.ax = after_ax

    slm_fixer.put_phase_map(np.zeros((1, 1)))

    fig.show()
    roi = Borders(500, 200, 503, 203)  # In pixels...
    super_pixel_size = 5
    borders = Borders(0, 0, 1272 / super_pixel_size, 1024 / super_pixel_size)
    optimizer = SpecklesOptimizer(borders, roi, super_pixel_size, simulator, slm_fixer, output_camera, fig)
    summary = optimizer.optimize(40)

    fig, ax = plt.subplots()
    summary.plot_intensity_vs_iteration(ax)

    plt.show()


def tests():
    fig, ax = plt.subplots()
    output_camera = RealCameraAdapter(ax)
    slm_fixer = RealSLMDeviceAdapter()
    slm_fixer.put_phase_map(np.zeros((1, 1)))

    # import time
    # time.sleep(1)

    output_camera.prop_through(None)
    plt.show()


def test_response_time():
    fig, axes = plt.subplots(5, 2)

    output_camera = RealCameraAdapter(None)
    slm_fixer = RealSLMDeviceAdapter()
    slm_fixer.put_phase_map(np.zeros((1, 1)))

    simulator = MockOpticalSystem()
    simulator.add_device(slm_fixer)
    simulator.add_device(output_camera)

    start_time = time.time()
    use_diffuser = False
    for ax in axes.flat:
        output_camera.ax = ax
        if use_diffuser:
            slm_fixer.put_phase_map(np.zeros((1, 1)))
            simulator.simulate()
        else:
            slm_fixer.put_phase_map(-slm_fixer.diffusion_phase_map)
            simulator.simulate()
        use_diffuser = not use_diffuser

    end_time = time.time()
    print(f'total time: {end_time - start_time}s')

    plt.show()


def main():
    simulate_focus_speckles()
    # focus_speckles_in_lab()
    # tests()
    # test_response_time()


if __name__ == '__main__':
    main()
