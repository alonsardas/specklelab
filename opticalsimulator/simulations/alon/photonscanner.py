import datetime
import logging.handlers
import os
import time

import matplotlib.pyplot as plt
import numpy as np

from opticalsimulator import Borders
from opticalsimulator.lab_games.motor import ThorLabsMotors
from opticalsimulator.lab_games.photon_counter import PhotonCounter
from opticalsimulator.results import ScanResult
from opticalsimulator.simulations.alon import runutils
from opticalsimulator.simulations.ronen.misc_utils import my_mesh

LOGS_DIR = "C:\\temp"
RESULTS_BASE_DIR = r"C:\Users\Owner\Google Drive\People\Alon"

logger = logging.getLogger('lab.photon_scanner')


class PhotonScanner(object):
    best_x = 5.85
    best_y = 9.75

    def __init__(self, integration_time, start_x, start_y, x_pixels, y_pixels, pixel_size_x, pixel_size_y,
                 should_normalize_counts=False, run_name='_scan', results_dir=None):
        self.start_x = start_x or self.best_x - ((x_pixels - 1) * pixel_size_x) / 2
        self.start_y = start_y or self.best_y - ((y_pixels - 1) * pixel_size_y) / 2

        self.x_pixels = x_pixels
        self.y_pixels = y_pixels
        self.pixel_size_x = pixel_size_x
        self.pixel_size_y = pixel_size_y

        self.integration_time = integration_time

        self.should_normalize_counts = should_normalize_counts

        self.X = np.linspace(self.start_x, self.start_x + (self.x_pixels - 1) * self.pixel_size_x, self.x_pixels)
        self.Y = np.linspace(self.start_y, self.start_y + (self.y_pixels - 1) * self.pixel_size_y, self.y_pixels)
        self.Y = self.Y[::-1]

        self.single1s = np.zeros((self.y_pixels, self.x_pixels))
        self.single2s = np.zeros((self.y_pixels, self.x_pixels))
        self.coincidences = np.zeros((self.y_pixels, self.x_pixels))

        self.result = ScanResult(X=self.X, Y=self.Y, integration_time=self.integration_time)
        self.timestamp = datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')

        self.run_name = run_name
        self.results_dir = results_dir or LOGS_DIR
        self.scan_path = os.path.join(self.results_dir, self.run_name)

        scanning_borders = Borders(self.start_x, self.start_y,
                                   self.start_x + self.pixel_size_x * self.x_pixels,
                                   self.start_y + self.pixel_size_y * self.y_pixels)
        logger.debug(f'Scanning area: {scanning_borders}')
        logger.debug(f'pixel size ({self.pixel_size_x}, {self.pixel_size_y})')
        logger.debug(f'pixels: {self.x_pixels}X{self.y_pixels}')
        logger.debug(f'Integration time: {self.integration_time}secs')
        logger.debug(f'normalized counts: {self.should_normalize_counts}')
        logger.debug(f'scan results path: {self.scan_path}')

    def scan(self):
        logger.info('getting motors...')
        ms = ThorLabsMotors()

        logger.info('getting photon counter...')
        ph = PhotonCounter(integration_time=self.integration_time)

        logger.info('Moving to starting position...')
        ms.move_x_absolute(self.start_x)
        ms.move_y_absolute(self.start_y)

        try:
            logger.info('starting scan')
            count = 0
            for i in range(self.y_pixels):
                for j in range(self.x_pixels):
                    ms.move_x_relative(self.pixel_size_x)
                    s1, s2, coincidence = ph.read_interesting()

                    if not self.should_normalize_counts:
                        s1, s2, coincidence = \
                            s1 * self.integration_time, s2 * self.integration_time, coincidence * self.integration_time
                    self.single1s[i, j], self.single2s[i, j], self.coincidences[i, j] = s1, s2, coincidence
                    logger.debug(f'pix: {i}, {j}. Singles1: {self.single1s[i, j]:.3f}. '
                                 f'Singles2: {self.single2s[i, j]:.3f}. Coincidence: {self.coincidences[i, j]:.3f}.')

                    count += 1
                    if count % 5 == 0:
                        self.result.coincidences = self.coincidences
                        self.result.single1s = self.single1s
                        self.result.single2s = self.single2s
                        self._save_result()

                    plt.pause(0.03)  # Have plot interact a bit on every move

                ms.move_y_relative(self.pixel_size_y)
                ms.move_x_absolute(self.start_x)

            self.result.coincidences = self.coincidences
            self.result.single1s = self.single1s
            self.result.single2s = self.single2s
            self._save_result(final=True)

        finally:
            logger.debug('closing motors')
            ms.close()
            logger.debug('closing photon counter')
            ph.close()

        return self.single1s, self.single2s, self.coincidences

    def _save_result(self, final=False):
        logger.debug(f'saving result final={final}')
        self.result.saveto(self.scan_path)

    def plot(self, mat):
        fig, ax = plt.subplots()
        my_mesh(self.X, self.Y, mat, ax)
        ax.set_title('Coincidence')

    def plot_coincidence(self, name):
        plt.close(1338)
        fig, ax = plt.subplots()
        my_mesh(self.X, self.Y, self.coincidences, ax)
        # ax.invert_yaxis()
        fig.show()
        fig.savefig(f"C:\\temp\\{time.time()}{name}.png")

    def plot_singles(self):
        fig, axes = plt.subplots(1, 2)
        xx, yy = np.meshgrid(self.X, self.Y)
        im0 = axes[0].pcolormesh(xx, yy, self.single1s, shading='nearest', vmin=0)
        im1 = axes[1].pcolormesh(xx, yy, self.single2s, shading='nearest', vmin=0)
        fig.suptitle('single counts')

        fig.colorbar(im0, ax=axes[0])
        fig.colorbar(im1, ax=axes[1])
        # axes[0].invert_yaxis()
        # axes[1].invert_yaxis()
        fig.show()


def run_script():
    best_x = 5.85
    best_y = 9.75


    best_x = 5.95
    best_y = 9.75
    # best_x = 7.7
    # best_y = 7.8
    # best_x = 7.025
    # best_y = 8.6

    x_pixels, y_pixels = 12, 12
    pixel_size_x, pixel_size_y = 0.025, 0.025

    integration_time = 8

    start_x = best_x - pixel_size_x * x_pixels / 2
    start_y = best_y - pixel_size_y * y_pixels / 2

    total_seconds = x_pixels * y_pixels * integration_time
    runutils.show_running_time_message(total_seconds)

    results_dir = runutils.create_results_folder('focus-scan')

    runutils.initialize_logger(results_dir)

    name = 'focus.scan'

    scanner = PhotonScanner(integration_time, start_x, start_y, x_pixels, y_pixels, pixel_size_x, pixel_size_y,
                            run_name=name, results_dir=results_dir)

    single1s, single2s, coincidences = scanner.scan()
    scanner.plot_coincidence(name)
    plt.show()


if __name__ == '__main__':
    run_script()
