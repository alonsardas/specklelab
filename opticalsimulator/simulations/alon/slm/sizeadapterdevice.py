import cv2

from opticalsimulator import Borders
from opticalsimulator.simulations.alon.slm.basedevice import BaseSLMDevice


class SizeAdapterDevice(BaseSLMDevice):
    def __init__(self, outer_device: BaseSLMDevice, borders: Borders,
                 pixels_x=-1, pixels_y=-1, interpolation=cv2.INTER_AREA):
        """
        :param outer_device: The device within this device is shown
        :param borders: The area this device occupies. borders are in pixels units, relative to the outer device
        :param pixels_x: How many pixels this device has. Default is 1:1 ratio
        :param pixels_y: How many pixels this device has. Default is 1:1 ratio
        :param interpolation: Interpolation method if needed
        """
        if pixels_x == -1:
            pixels_x = borders.width
        if pixels_y == -1:
            pixels_y = borders.height
        pixel_ratio_x = borders.width / pixels_x
        pixel_ratio_y = borders.height / pixels_y

        pixel_size_x = outer_device.pixel_size_x * pixel_ratio_x
        pixel_size_y = outer_device.pixel_size_y * pixel_ratio_y

        super().__init__(pixels_x, pixels_y, pixel_size_x, pixel_size_y)
        self.outer_device = outer_device
        self.borders = borders
        self.interpolation = interpolation

    def update(self):
        dst = self.outer_device.phase_grid[self.borders.min_y:self.borders.max_y, self.borders.min_x:self.borders.max_x]
        cv2.resize(self.phase_grid, (self.borders.width, self.borders.height),
                   dst=dst, interpolation=self.interpolation)
        self.outer_device.update()
