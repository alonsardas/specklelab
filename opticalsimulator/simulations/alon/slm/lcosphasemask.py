import matplotlib.axes
import matplotlib.figure
import numpy as np
from matplotlib import pyplot as plt

from opticalsimulator import Borders


class LCoSPhaseMask(object):
    def __init__(self, phase_grid):
        self.phase_grid = phase_grid

    def plot(self):
        fig, ax = plt.subplots()
        assert isinstance(fig, matplotlib.figure.Figure)
        assert isinstance(ax, matplotlib.axes.Axes)

        im = ax.imshow(self.phase_grid, vmin=0, vmax=255, cmap='gray')
        fig.colorbar(im, ax=ax)
        return fig, ax

    def crop(self, borders: Borders):
        self.phase_grid = self.phase_grid[borders.min_y:borders.max_y, borders.min_x:borders.max_x]

    def save_phase_mask(self, path):
        np.savez(path, phase_grid=self.phase_grid)


def load(path):
    phase_grid = np.load(path)['phase_grid']
    return LCoSPhaseMask(phase_grid)
