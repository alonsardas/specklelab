import logging

from matplotlib import pyplot as plt
from rpyc.core import SlaveService
from rpyc.utils.server import OneShotServer

from opticalsimulator.simulations.alon.slm.labslm import get_slm_device

PIXELS = (1272, 1024)
MONITOR1_POSITION = (10, 30)
MONITOR2_POSITION = (103, 30)


# MONITOR1_POSITION = (1913, -30)
# MONITOR2_POSITION = (3193, -30)
# image_writer1 = MonitorImageWriter(PIXELS[0], PIXELS[1], MONITOR1_POSITION[0], MONITOR1_POSITION[1], 'slm1')
# image_writer2 = MonitorImageWriter(PIXELS[0], PIXELS[1], MONITOR2_POSITION[0], MONITOR2_POSITION[1], 'slm2')
image_writer1, slm1, grating1, inner_slm1 = get_slm_device(1)
image_writer2, slm2, grating2, inner_slm2 = get_slm_device(2)

# noinspection PyMethodMayBeStatic
class LabService(SlaveService):
    def on_connect(self, conn):
        super(LabService, self).on_connect(conn)
        # self.image_writer2, self.slm2, self.grating2, self.inner_slm2 = get_slm_device(2)
        # self.image_writer1, self.slm1, self.grating1, self.inner_slm1 = get_slm_device(1)
        self.image_writer1, self.slm1 = image_writer1, slm1
        self.image_writer2, self.slm2 = image_writer2, slm2


        # self.pause()

    def on_disconnect(self, conn):
        super(LabService, self).on_disconnect(conn)
        self.image_writer1.close()
        self.image_writer2.close()

    def pause(self, time=0.05):
        plt.pause(time)


if __name__ == "__main__":

    logging.basicConfig()
    rpyc_logger = logging.getLogger('LAB/18861')
    rpyc_logger.setLevel('DEBUG')
    import threading
    def serve():
        while True:
            # Using OneShotServer means that matplotlib is running on main thread. Otherwise, things don't work
            t = OneShotServer(LabService, port=18861)
            t.start()
    t = threading.Thread(target=serve)
    t.start()
    plt.show()
