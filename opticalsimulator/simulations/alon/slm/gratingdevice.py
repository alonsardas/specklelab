import numpy as np

from opticalsimulator.simulations.alon.slm.basedevice import BaseSLMDevice


class GratingDevice(BaseSLMDevice):
    """
    Add grating to the inner controlled device
    """

    def __init__(self, inner_device: BaseSLMDevice):
        super().__init__(inner_device.pixels_x, inner_device.pixels_y, inner_device.pixel_size_x,
                         inner_device.pixel_size_y)
        self.inner_device = inner_device
        self.grating_m = 0
        self.grating_angle = 0

    def set_grating(self, m, angle):
        self.grating_m = m
        self.grating_angle = angle

    def update(self):
        grating = self.grating_m * self.inner_device.x_pixel_indexes * np.cos(self.grating_angle) + \
                  self.grating_m * self.inner_device.y_pixel_indexes * np.sin(self.grating_angle)
        self.inner_device.phase_grid = self.phase_grid + grating
        self.inner_device.update()
