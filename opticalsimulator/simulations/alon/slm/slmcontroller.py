import cv2
import matplotlib.axes
import matplotlib.figure
import numpy as np
from matplotlib import pyplot as plt

from opticalsimulator.simulations.alon.slm.basedevice import BaseSLMDevice


class SLMController(object):
    def __init__(self, device: BaseSLMDevice):
        self.device = device

    def normal(self):
        self.device.phase_grid.fill(0)
        self._update()

    def pi_step_x(self, x):
        self.device.phase_grid.fill(0)
        self.device.phase_grid[:, x:] = np.pi
        self._update()

    def pi_step_y(self, y):
        self.device.phase_grid.fill(0)
        self.device.phase_grid[y:, :] = np.pi
        self._update()

    def put_angular_spectrum_method(self, distance, wavelength, f, axis='xy', keep_center_fixed=True):
        light_k = 2 * np.pi / wavelength

        xs = self.device.x_pixel_indexes * self.device.pixel_size_x - self.device.width / 2
        ys = self.device.y_pixel_indexes * self.device.pixel_size_y - self.device.height / 2

        k_x = xs / f * light_k
        k_y = ys / f * light_k

        if axis == 'xy':
            k_z_sqr = light_k ** 2 - (k_x ** 2 + k_y ** 2)
        elif axis == 'x':
            k_z_sqr = light_k ** 2 - k_x ** 2
        elif axis == 'y':
            k_z_sqr = light_k ** 2 - k_y ** 2
        else:
            raise ValueError(f'Unknown axis option {axis}')

        # Remove all the negative component, as they represent evanescent waves,
        # See Fourier Optics page 58
        np.maximum(k_z_sqr, 0, out=k_z_sqr)
        k_z = np.sqrt(k_z_sqr)

        self.device.phase_grid = distance * k_z

        if keep_center_fixed:
            center_x, center_y = int(self.device.pixels_x / 2), int(self.device.pixels_y / 2)
            center_phase = self.device.phase_grid[center_y, center_x]
            self.device.phase_grid -= center_phase
            self.device.phase_grid += 0.95 * 2 * np.pi

        self._update()

    def plot_phase_mask(self, allow_wavelength_overflow=False):
        fig, ax = plt.subplots()
        assert isinstance(fig, matplotlib.figure.Figure)
        assert isinstance(ax, matplotlib.axes.Axes)

        if not allow_wavelength_overflow:
            phases = np.mod(self.device.phase_grid, 2 * np.pi)
            im = ax.imshow(phases, vmin=0, vmax=2 * np.pi)
            cbar = fig.colorbar(im, ax=ax, ticks=[0, np.pi / 2, np.pi, 3 / 2 * np.pi, 2 * np.pi])
            cbar.ax.set_yticklabels(['0',
                                     r'$ \frac{1}{2} \pi $',
                                     r'$ \pi $',
                                     r'$ \frac{3}{2} \pi $',
                                     r'$ 2 \pi $'])
        else:
            im = ax.imshow(self.device.phase_grid)
            fig.colorbar(im, ax=ax)
        return fig, ax

    def save_phase_mask(self, path):
        save_phase_mask(path, self.device.phase_grid)

    def load_phase_mask(self, path):
        phase_grid = load_phase_mask(path)
        self.device.phase_grid = phase_grid
        self._update()
        return phase_grid

    def resize_and_put(self, mask, interpolation=cv2.INTER_AREA):
        cv2.resize(mask, (self.device.pixels_x, self.device.pixels_y),
                   dst=self.device.phase_grid, interpolation=interpolation)
        self.device.update()

    def _update(self):
        self.device.update()


def save_phase_mask(path, mask):
    np.savez(path, phase_grid=mask)


def load_phase_mask(path):
    return np.load(path)['phase_grid']
