import numpy as np

from opticalsimulator.simulations.alon.slm.basedevice import BaseSLMDevice
from opticalsimulator.simulations.alon.slm.imagewriter import ImageWriter
from opticalsimulator.simulations.alon.slm.lcosphasemask import LCoSPhaseMask


class LCoSSLMDevice(BaseSLMDevice):
    """
    This device is in charge of adding the correction pattern, and adapting the output to
    the expected output format (e.g. phase in units of 0-alpha)
    """

    def __init__(self, image_writer: ImageWriter, pixel_size_x, pixel_size_y, alpha, correction_pattern=None,
                 allow_wavelength_overflow=False, ):
        super().__init__(image_writer.pixels_x, image_writer.pixels_y, pixel_size_x, pixel_size_y)
        self.image_writer = image_writer
        self.allow_wavelength_overflow = allow_wavelength_overflow
        self.alpha = alpha

        if correction_pattern is None:
            self.correction = np.zeros(self.phase_grid.shape)
        else:
            self.correction = correction_pattern

    def update(self):
        phase = self._calc_phase()
        self.image_writer.update_image(phase)

    def _calc_phase(self):
        # Calculating image to send to the SLM (see data sheet for an example)
        phase = self.phase_grid * 255 / (2 * np.pi) + self.correction
        if not self.allow_wavelength_overflow:
            # This is the suggested behavior, where all pixels are in range 0-alpha
            np.mod(phase, 256, out=phase)
            phase = phase * self.alpha / 255
        else:
            phase = phase * self.alpha / 255
            alphas_mod = np.floor(255 / self.alpha) * self.alpha
            np.mod(phase, alphas_mod, out=phase)
            # np.mod(phase, self.alpha, out=phase, where=phase >= 256)
        phase = np.uint8(phase)
        return phase

    def get_lcos_phase_mask(self) -> LCoSPhaseMask:
        self._calc_phase()
        return LCoSPhaseMask(self._calc_phase())
