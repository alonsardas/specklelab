import numpy as np
from matplotlib import pyplot as plt

from opticalsimulator import Borders
from opticalsimulator.simulations.alon.slm.gratingdevice import GratingDevice
from opticalsimulator.simulations.alon.slm.lcosdevice import LCoSSLMDevice
from opticalsimulator.simulations.alon.slm.monitorimagewriter import MonitorImageWriter
from opticalsimulator.simulations.alon.slm.sizeadapterdevice import SizeAdapterDevice

configs = {
    1: {  # SLM no. 1 with 404nm
        'correction_path': r'F:\SLM-x13138-05\deformation_correction_pattern\CAL_LSH0801946_400nm.bmp',
        'alpha': 215,  # for 404nm, taken from the data sheet
        'pixels': (1272, 1024),
        'pixel_size': 12.5e-3,
        'monitor_position': (1913, -30),
        'monitor': 0,  # If using slmpy
        'active_mask_slice': np.index_exp[240:510, 630:780]
    },
    2: {  # SLM no. 2 with 404nm
        'correction_path': r'F:\SLM-x13138-01\deformation_correction_pattern\CAL_LSH0801676_400nm.bmp',
        'alpha': 95,  # for 404nm, taken from the data sheet
        'pixels': (1272, 1024),
        'pixel_size': 12.5e-3,
        'monitor_position': (3193, -30),
        'monitor': 2,  # If using slmpy
        'active_mask_slice': np.index_exp[312:582, 323:473]
    },
    3: {  # SLM no. 2 with 808nm
        'correction_path': r'F:\SLM-x13138-01\deformation_correction_pattern\CAL_LSH0801676_700nm.bmp',
        'alpha': 130,  # About half the alpha needed for 404nm
        'pixels': (1272, 1024),
        'pixel_size': 12.5e-3,
        'monitor_position': (3193, -30),
        'monitor': 2,  # If using slmpy
        'active_mask_slice': np.index_exp[312:582, 323:473]
    },
    4: {  # SLM no. 2 with 808nm
        'correction_path': r'F:\SLM-x13138-01\deformation_correction_pattern\CAL_LSH0801676_700nm.bmp',
        'alpha': 270,  # for 808nm, extrapolated from the data sheet.
        'pixels': (1272, 1024),
        'pixel_size': 12.5e-3,
        'monitor_position': (3193, -30),
        'monitor': 2,  # If using slmpy
        'active_mask_slice': np.index_exp[312:582, 323:473]
    }
}


def get_slm_device(config_num) -> (MonitorImageWriter, LCoSSLMDevice, GratingDevice, SizeAdapterDevice):
    config = configs[config_num]
    correction_pattern = load_correction_pattern(config['correction_path'])

    pixels_x, pixels_y = config['pixels']
    pixel_size = config['pixel_size']
    alpha = config['alpha']
    monitor_position_x, monitor_position_y = config['monitor_position']
    image_writer = MonitorImageWriter(pixels_x, pixels_y, monitor_position_x, monitor_position_y, f'SLM{config_num}')
    device = LCoSSLMDevice(image_writer, pixel_size, pixel_size, alpha, correction_pattern)

    grating_device = GratingDevice(device)

    mask = config['active_mask_slice']
    # borders = Borders(mask[1].start, mask[0].start, mask[1].stop, mask[0].stop)
    # Note: The mask given in the config is very tight (and probably too tight, meaning that there is more area available)
    # Here we add margin, but iis still fine because the important thing is to be in the right location
    borders = Borders(mask[1].start - 100, mask[0].start - 100, mask[1].stop + 100, mask[0].stop + 100)
    inner_device = SizeAdapterDevice(grating_device, borders)
    return image_writer, device, grating_device, inner_device


def load_correction_pattern(path):
    return plt.imread(path)
