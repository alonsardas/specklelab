import numpy as np


class ImageWriter(object):
    def __init__(self, pixels_x, pixels_y):
        self._pixels_x = pixels_x
        self._pixels_y = pixels_y

    def update_image(self, image: np.ndarray):
        raise NotImplementedError()

    @property
    def pixels_x(self):
        return self._pixels_x

    @property
    def pixels_y(self):
        return self._pixels_y
