import numpy as np

from opticalsimulator import OpticalDevice, FieldGrid, Borders, Mask
from opticalsimulator.simulations.alon.slm.basedevice import BaseSLMDevice


class SimulationSLMDevice(BaseSLMDevice, OpticalDevice):
    def __init__(self, active_borders: Borders, pixels_x, pixels_y, dark_outside=True):
        super().__init__(pixels_x, pixels_y, active_borders.width / pixels_x, active_borders.height / pixels_y)
        self.borders = active_borders
        self.dark_outside = dark_outside

    def update(self):
        pass

    def prop_through(self, field: FieldGrid):
        if self.dark_outside:
            mask = Mask(active_borders=self.borders)
            mask.prop_through(field)
        phases = self._coords_to_phase(field)
        field.Es *= np.exp(1j * phases)

    def _coords_to_phase(self, field_grid: FieldGrid):
        # Think of Xs as x coord and Ys as y coord. it works
        moved_Xs = field_grid.Xs - self.borders.min_x
        moved_Ys = field_grid.Ys - self.borders.min_y
        x_pix_index = moved_Xs / self.pixel_size_x
        y_pix_index = moved_Ys / self.pixel_size_y

        x_pix_index = x_pix_index.astype('int')
        y_pix_index = y_pix_index.astype('int')

        to_zero_x = x_pix_index >= self.pixels_x - 1
        to_zero_x = np.logical_or(to_zero_x, x_pix_index < 0)

        to_zero_y = y_pix_index >= self.pixels_y - 1
        to_zero_y = np.logical_or(to_zero_y, y_pix_index < 0)

        x_pix_index[to_zero_x] = 0
        y_pix_index[to_zero_y] = 0

        phases = self.phase_grid[y_pix_index, x_pix_index]
        phases[to_zero_x] = 0
        phases[to_zero_y] = 0

        return phases
