import warnings

import matplotlib as mpl
import matplotlib.figure
import matplotlib.pyplot as plt
import numpy as np

from opticalsimulator.simulations.alon.slm.imagewriter import ImageWriter


class MonitorImageWriter(ImageWriter):
    """
    This object assumes that the SLM device is connected as another monitor to the computer.
    Here we use matplotlib to show the image on that monitor
    """

    def __init__(self, pixels_x, pixels_y, monitor_position_x, monitor_position_y, fig_name):
        super().__init__(pixels_x, pixels_y)

        self.fig: matplotlib.figure.Figure = plt.figure(f'{fig_name}-Figure', frameon=False)
        self.axes = self.fig.add_axes([0., 0., 1., 1., ])

        if mpl.get_backend() != 'TkAgg':
            warnings.warn("You are not using 'TkAgg' as the backend of matplotlib. "
                          "Therefore, slm device figure will not be as needed")
        else:
            self.fig.canvas.toolbar.pack_forget()

        # Move the figure to the desired monitor
        # Geometry for example: '1272x1024+1913+-30'
        self._monitor_geometry = f'{pixels_x}x{pixels_y}+{monitor_position_x}+{monitor_position_y}'
        # This pause is necessary to make sure the location of the windows is actually changed when using TeamViewer
        plt.pause(0.1)
        self.restore_position()
        self.axes.set_axis_off()
        self.ax_image = self.axes.imshow(np.zeros((pixels_y, pixels_x)), cmap='gray', vmin=0, vmax=255)
        self.fig.canvas.draw()
        self.fig.show()

        self.background = self.fig.canvas.copy_from_bbox(self.axes.bbox)

    def update_image(self, image: np.ndarray):
        # restore background
        self.fig.canvas.restore_region(self.background)
        self.ax_image.set_data(image)

        # redraw just the points
        self.axes.draw_artist(self.ax_image)

        # fill in the axes rectangle
        self.fig.canvas.blit(self.axes.bbox)

        plt.pause(0.001)

    def restore_position(self):
        if mpl.get_backend() == 'TkAgg':
            self.fig.canvas.manager.window.geometry(self._monitor_geometry)

    def close(self):
        plt.close(self.fig)
