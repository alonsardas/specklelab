import numpy as np


class BaseSLMDevice(object):
    def __init__(self, pixels_x, pixels_y, pixel_size_x, pixel_size_y):
        self._pixels_x = pixels_x
        self._pixels_y = pixels_y
        self._pixel_size_x = pixel_size_x
        self._pixel_size_y = pixel_size_y

        x = np.arange(0, pixels_x)
        y = np.arange(0, pixels_y)
        self._x_pixel_indexes, self._y_pixel_indexes = np.meshgrid(x, y)

        self._phase_grid = np.zeros((pixels_y, pixels_x))

    def update(self):
        raise NotImplementedError()

    @property
    def pixels_x(self):
        return self._pixels_x

    @property
    def pixels_y(self):
        return self._pixels_y

    @property
    def pixel_size_x(self):
        return self._pixel_size_x

    @property
    def pixel_size_y(self):
        return self._pixel_size_y

    @property
    def x_pixel_indexes(self):
        return self._x_pixel_indexes

    @property
    def y_pixel_indexes(self):
        return self._y_pixel_indexes

    @property
    def width(self):
        return self._pixels_x * self._pixel_size_x

    @property
    def height(self):
        return self._pixels_y * self._pixel_size_y

    @property
    def phase_grid(self):
        return self._phase_grid

    @phase_grid.setter
    def phase_grid(self, phases: np.ndarray):
        if phases.shape != self._phase_grid.shape:
            msg = f'The new phase grid must have the same shape. Got {phases.shape}, ' \
                  f'while expecting {self._phase_grid.shape}'
            raise ValueError(msg)

        self._phase_grid = phases
