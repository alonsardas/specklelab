import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

from opticalsimulator import RealSLMDevice, VimbaCamera
from opticalsimulator.results import show_phase_mask
from opticalsimulator.simulations.ronen.misc_utils import my_mesh
from opticalsimulator.simulations.ronen.new_simulations.propagate import prop_angular_spectrum_paraxial, \
    prop_angular_spectrum, prop_farfield_fft, prop_phase_mask, prop_lense_mask
from opticalsimulator.simulations.ronen.new_simulations.diffuser_generator import get_mask_diffuser_macro, \
    get_ift_diffuser_mask
from opticalsimulator.simulations.ronen.new_simulations.field import Field


def get_speckles_macro_pix_sim(macro_pixels_x, macro_pixels_y=None):
    macro_pixels_y = macro_pixels_y or macro_pixels_x

    # From culimator
    sigma_0 = 10e-3
    x, y = Field.gen_x_y(Nx=512, Ny=512, Dx=sigma_0*20, Dy=sigma_0*20)
    f = Field(x, y, wavelength=404e-9)
    f.set_gausian(sigma_0)

    # f.show_power('before')
    # print(f'before: {f.total_power()}')

    # Propagation
    A = get_mask_diffuser_macro(f.Nx, macro_pixels_x, f.Ny, macro_pixels_y)
    f2 = prop_phase_mask(f, A)

    f3 = prop_farfield_fft(f2, 0.020)
    # f3.show_power('farfield')
    # print(f'farfield: {f3.total_power()}')
    # show_phase_mask(A)
    return f3


def get_ift_speckles_sim(sigma):

    # From culimator
    sigma_0 = 10e-3
    x, y = Field.gen_x_y(Nx=512, Ny=512, Dx=sigma_0*20, Dy=sigma_0*20)
    f = Field(x, y, wavelength=404e-9)
    f.set_gausian(sigma_0)

    # Propagation
    A = get_ift_diffuser_mask(f, sigma)
    f2 = prop_phase_mask(f, A)

    f3 = prop_farfield_fft(f2, 0.020)
    return f3


def get_speckles_macro_pix_lab(cam, diffuser_slm, macro_pixels_x, macro_pixels_y=None):
    assert isinstance(diffuser_slm, RealSLMDevice)
    macro_pixels_y = macro_pixels_y or macro_pixels_x
    A = 2*np.pi*np.random.rand(macro_pixels_y, macro_pixels_x)
    diffuser_slm.update_phase_in_active(A)
    powers = cam.get_image()
    return powers.astype('float64')


def show_power(I, x, y, title='', clim=None):
    fig, ax = plt.subplots()
    im = my_mesh(x, y, I, ax, clim=clim)
    fig.suptitle(title)
    fig.show()


def sum_speckles_to_gaussian_sim(N, opt='ift'):
    if opt == 'macro_pix':
        f3 = get_speckles_macro_pix_sim(50, 50)
    elif opt == 'ift':
        f3 = get_ift_speckles_sim(np.pi/8)
    else:
        raise
    power_sum = f3.power

    f3.show_power(title='sample speckles')

    for i in range(N):
        if i % 10 == 0:
            print(f'{i}/{N}')
        f = get_speckles_macro_pix_sim(50, 50)
        power_sum = power_sum + f.power

    show_power(power_sum, f3.x, f3.y, title='sum over 100')

    plt.show()


def sum_speckles_to_gaussian_lab(N):
    slm1 = RealSLMDevice(1)
    diffuser_slm = RealSLMDevice(2, use_mirror=True)
    cam = VimbaCamera(0, exposure_time=4000)
    power_sum = get_speckles_macro_pix_lab(cam, diffuser_slm, 30, 30)

    cam.show_image(power_sum, title='sample speckles')

    for i in range(N):
        if i % 10 == 0:
            print(f'{i}/{N}')
        power = get_speckles_macro_pix_lab(cam, diffuser_slm, 30, 30)
        power_sum += power

    cam.show_image(power_sum, title=f'sum over {N}')

    plt.show()


def speckles_power_spectrum(N, opt='ift'):
    if opt == 'macro_pix':
        f3 = get_speckles_macro_pix_sim(50, 50)
    elif opt == 'ift':
        f3 = get_ift_speckles_sim(np.pi/8)
    else:
        raise

    powers = f3.power.flatten()

    for i in range(N):
        if i % 10 == 0:
            print(f'{i}/{N}')

        f3 = get_speckles_macro_pix_sim(50, 50)
        powers = np.append(powers, f3.power[250, 250].flatten())

    fig, ax = plt.subplots()

    # ax.set_ylim(0, 3000)
    x_min = 0.001
    x_max = 0.015
    n, bins, patches = ax.hist(powers, bins=1000, range=(x_min, x_max))

    popt, pcov = curve_fit(lambda x, A, b: A*np.exp(-b*x), bins[:-1], n)
    print(popt)
    A, b = popt
    X = np.linspace(0.001, 0.015, 2000)
    Y = A*np.exp(-b*X)
    ax.plot(X, Y, '--')
    fig.show()


if __name__ == "__main__":
    # sum_speckles_to_gaussian_sim(200)
    speckles_power_spectrum(1000)
    # sum_speckles_to_gaussian_lab(200)
    plt.show()
