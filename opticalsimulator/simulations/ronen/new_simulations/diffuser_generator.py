import cv2
import numpy as np

from simulations.ronen.new_simulations.ft import ift2


def get_mask_diffuser_macro(micro_pixels_x, macro_pixels_x, micro_pixels_y=None, macro_pixels_y=None):
    micro_pixels_y = micro_pixels_y or micro_pixels_x
    macro_pixels_y = macro_pixels_y or macro_pixels_x
    A = 2*np.pi*np.random.rand(macro_pixels_x, macro_pixels_y)
    A2 = cv2.resize(A, (micro_pixels_x, micro_pixels_y), interpolation=cv2.INTER_NEAREST)

    return A2


def get_ift_diffuser_mask(f, theta):
    f_x = np.fft.fftshift(np.fft.fftfreq(f.Nx, f.dx))
    f_y = np.fft.fftshift(np.fft.fftfreq(f.Ny, f.dy))
    f_Xs, f_Ys = np.meshgrid(f_x, f_y)

    sigma = theta * f.k / (2 * np.pi)

    PSD = np.exp(-(f_Xs ** 2 + f_Ys ** 2) / (2 * sigma ** 2))
    PSD[int(f.Ny / 2), int(f.Nx / 2)] = 0  # removing the zero freq term
    # print(f_Xs[int(N_y / 2), int(N_x / 2)], f_Ys[int(N_y / 2), int(N_x / 2)])
    rand_spectrum = (np.random.randn(f.Ny, f.Nx) + 1j * np.random.randn(f.Ny, f.Nx)) * np.sqrt(PSD)
    ifft, _, _ = ift2(rand_spectrum, f_x, f_y)
    return np.angle(ifft)
