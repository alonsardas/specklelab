import numpy as np
import matplotlib.pyplot as plt

from opticalsimulator.simulations.ronen.misc_utils import my_mesh


class Field(object):
    def __init__(self, x, y, wavelength):
        self.x = x
        self.y = y
        self.wavelength = wavelength  # [m]
        self.E = np.zeros((self.Ny, self.Nx)).astype('complex128')

    @classmethod
    def gen_x_y(cls, Nx, Ny, Dx, Dy):
        dx = Dx / Nx
        dy = Dy / Ny
        x = np.arange(-Nx / 2, Nx / 2) * dx
        y = np.arange(-Ny / 2, Ny / 2) * dy

        return x, y

    @property
    def Nx(self):
        return len(self.x)

    @property
    def Ny(self):
        return len(self.y)

    @property
    def dx(self):
        return self.x[1] - self.x[0]

    @property
    def dy(self):
        return self.y[1] - self.y[0]

    @property
    def Dx(self):
        return self.Nx * self.dx

    @property
    def Dy(self):
        return self.Ny * self.dy

    @property
    def X(self):
        X, _ = np.meshgrid(self.x, self.y)
        return X

    @property
    def Y(self):
        _, Y = np.meshgrid(self.x, self.y)
        return Y

    @property
    def k(self):
        return 2*np.pi / self.wavelength  # [2*pi/m]

    @property
    def power(self):
        return np.abs(self.E)**2

    def set_gausian(self, sigma_0):
        self.E = np.exp(-(self.X ** 2 + self.Y ** 2) / sigma_0 ** 2)
        # self.E = self.E / (self.E.sum() * (self.dx ** 2))  # normalization

    def show_power(self, title='', clim=None):
        fig, ax = plt.subplots()
        I = np.abs(self.E)**2
        im = my_mesh(self.x, self.y, I, ax, clim=clim)
        fig.suptitle(title)
        fig.show()

    def total_power(self):
        I = np.abs(self.E)**2
        tot_I = I.sum()
        normalized_I = tot_I / (self.dx * self.dy)
        return normalized_I
