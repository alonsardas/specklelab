import numpy as np


def ft2(g, x, y):
    """g(x, y) -> G(f_X, f_y) """

    assert x[len(x)//2] == 0, "x convention not right"
    assert y[len(y)//2] == 0, "y convention not right"

    N_x = len(x)
    N_y = len(y)
    dx = x[1] - x[0]
    dy = y[1] - y[0]

    df_x = 1/(N_x*dx)
    df_y = 1/(N_y*dy)
    f_x = np.arange(-N_x/2, N_x/2) * df_x
    f_y = np.arange(-N_y/2, N_y/2) * df_y

    # Mul my dx*dy for parseval
    G = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(g)))*dx*dy
    return G, f_x, f_y


def ift2(G, f_x, f_y):
    """ G(f_x, f_y) -> g(x, y) """
    assert f_x[len(f_x)//2] == 0, "f_x convention not right"
    assert f_y[len(f_y)//2] == 0, "f_y convention not right"

    N_fx = len(f_x)
    N_fy = len(f_y)
    df_x = f_x[1] - f_x[0]
    df_y = f_y[1] - f_y[0]

    x = np.fft.fftshift(np.fft.fftfreq(N_fx, df_x))
    y = np.fft.fftshift(np.fft.fftfreq(N_fy, df_y))

    # mul by df_x*df_y for parseval, and by N_fx, N_fy because of numpy's factor in ifft
    g = np.fft.fftshift(np.fft.ifft2(np.fft.ifftshift(G))) * df_x * df_y * N_fx * N_fy

    return g, x, y
