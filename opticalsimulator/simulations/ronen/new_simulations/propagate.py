import numpy as np
import copy
from opticalsimulator.simulations.ronen.new_simulations.ft import ft2, ift2
from opticalsimulator.simulations.ronen.new_simulations.field import Field


def prop_angular_spectrum_paraxial(f: Field, L):
    """ This uses paraxial approximation, and excludes the global L*k phase """
    f2 = copy.deepcopy(f)
    G, fx, fy = ft2(f.E, f.x, f.y)
    Fx, Fy = np.meshgrid(fx, fy)
    G2 = G * np.exp(-1j * ((2*np.pi*Fx)**2 + (2*np.pi*Fy)**2) * L / (2 * f.k))

    f2.E, f2.x, f2.y = ift2(G2, fx, fy)
    return f2


def prop_angular_spectrum(f: Field, L):
    f2 = copy.deepcopy(f)

    G, f_x, f_y = ft2(f.E, f.x, f.y)

    freq_Xs, freq_Ys = np.meshgrid(f_x, f_y)

    k_x = freq_Xs * 2 * np.pi
    k_y = freq_Ys * 2 * np.pi

    k_z_sqr = f.k**2 - (k_x**2 + k_y**2)
    # Remove all the negative component, as they represent evanescent waves,
    # See Fourier Optics page 58
    np.maximum(k_z_sqr, 0, out=k_z_sqr)
    k_z = np.sqrt(k_z_sqr)

    # Propagate light by adding the phase,
    # see Fourier Optics page 74
    G *= np.exp(1j * k_z * L)

    f2.E, f2.x, f2.y = ift2(G, f_x, f_y)
    return f2


def prop_farfield_fft(f: Field, fl):
    """ fl: focal_length of lense"""
    f2 = copy.deepcopy(f)

    f2.E, f_x, f_y = ft2(f2.E, f2.x, f2.y)
    k_x = f_x * 2*np.pi
    k_y = f_y * 2*np.pi

    new_x = k_x * (fl/f2.k)
    new_y = k_y * (fl/f2.k)

    f2.x = new_x
    f2.y = new_y

    return f2


def prop_phase_mask(f: Field, phase_mask):
    """ f is field, mask in radians """
    new_f = copy.deepcopy(f)
    new_f.E = new_f.E * np.exp(1j * phase_mask)
    return new_f


def prop_lense_mask(f, fl):
    f2 = copy.deepcopy(f)
    f2.E = f.E * np.exp(1j * (f.X**2+f.Y**2) * f.k/(2*fl))
    return f2
