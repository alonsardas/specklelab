import numpy as np
from scipy.stats import norm


def get_kolmogorov(N_x, N_y, pixel_size_x, pixel_size_y, cn2=1e-16, L=100, l_o=10, l_i=0.5e-2, r0=None, is_test=False):
    """
    is_test is for comparing phase generation to matlab. use also:
    phase = np.round(slm1.set_kolmogorov(cn2=1e-16, L=3800, is_test=True), 4)
    mat_phase = np.round(loadmat("C:\\Users\\Owner\\Google Drive\\People\\Ronen\\kolmogorov\\Kolmogorov_code\\phase.mat")['phase'], 4).T
    """
    k_spdc = 2 * np.pi / 808e-9
    # L = 100  # m
    # cn2 = 1e-16  # 1e-17, 1e-16, 1e-15 for easy, moderate and hard torbulence
    r0 = r0 or (0.4229 * (k_spdc ** 2) * L * cn2) ** (-3 / 5)  # m, for the SPDC wavelength

    print("r0", r0)

    # l_o *= 1e-3
    # l_i *= 1e-3
    # r0 *= 1e-3

    if is_test:
        N_x = 128
        N_y = 128
    else:
        N_x = N_x
        N_y = N_y

    dx = pixel_size_x
    dy = pixel_size_y

    # The spacing between neighboring cells in the frequency grid
    df_x = 1 / (N_x * dx)
    df_y = 1 / (N_y * dy)

    f_x = np.fft.fftshift(np.fft.fftfreq(N_x, dx))
    f_y = np.fft.fftshift(np.fft.fftfreq(N_y, dy))
    f_Xs, f_Ys = np.meshgrid(f_x, f_y)

    # Power spectrum (von Karman)
    fm = (5.92 / l_i) / (2 * np.pi)  # inner scale frequency[1 / m]
    fo = 1 / l_o  # outer scale frequency[1 / m]
    PSD = 0.023 * (r0 ** (-5 / 3)) * np.exp(-((f_Xs ** 2 + f_Ys ** 2) / fm ** 2)) * (
                f_Xs ** 2 + f_Ys ** 2 + fo ** 2) ** (-11 / 6)
    PSD[N_y // 2, N_x // 2] = 0  # removing the zero freq term

    if is_test:
        np.random.seed(1)
        A = norm.ppf(np.random.rand(N_y, N_x))
        B = norm.ppf(np.random.rand(N_y, N_x))
    else:
        A = np.random.randn(N_y, N_x)
        B = np.random.randn(N_y, N_x)

    rand_spectrum = (A + 1j * B) * np.sqrt(PSD / (df_x * df_y))
    phase_screen = np.fft.fftshift(np.fft.ifft2(np.fft.ifftshift(rand_spectrum))) * df_x * df_y * N_x * N_y

    phase_screen = np.real(phase_screen)

    return phase_screen
