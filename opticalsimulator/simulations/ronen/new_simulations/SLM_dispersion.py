import numpy as np
import matplotlib.pyplot as plt
from opticalsimulator.simulations.ronen.misc_utils import my_mesh
from opticalsimulator.simulations.ronen.new_simulations.propagate import prop_angular_spectrum_paraxial, \
    prop_angular_spectrum, prop_phase_mask
from opticalsimulator.simulations.ronen.new_simulations.field import Field
from simulations.ronen.new_simulations.get_kolmogorov import get_kolmogorov


def kolmogorov_prop_single_screen(cn2=1e-15, L=1e3):
    plt.rcParams["figure.figsize"] = (9, 7)

    # From culimator
    sigma_0 = 10e-2
    Nx = 1024
    Ny = 1024
    Dx=sigma_0 * 10
    Dy=sigma_0 * 10
    dx = Dx/Nx
    dy = Dy/Ny

    x, y = Field.gen_x_y(Nx=Nx, Ny=Ny, Dx=Dx, Dy=Dy)
    f = Field(x, y, wavelength=808e-9)
    f.set_gausian(sigma_0)

    # Propagation
    A = get_kolmogorov(Nx, Ny, dx, dy, cn2=cn2, L=L, l_o=10, l_i=0.5e-2, is_test=False)
    A = np.mod(A, 2*np.pi)

    f_diffused = prop_phase_mask(f, A)

    # A2 = (2*np.pi) - A
    A2 = -A
    f_real_fixed = prop_phase_mask(f_diffused, A2)

    f_fixed_with_dispersion = prop_phase_mask(f_diffused, A2*270/190) # A2*(190/270))

    f_end_diffused = prop_angular_spectrum(f_diffused, L)
    f_end_fixed = prop_angular_spectrum(f_real_fixed, L)
    f_end_fixed_with_dispersion = prop_angular_spectrum(f_fixed_with_dispersion, L)

    fig, axes = plt.subplots(2, 2, constrained_layout=True)

    I1 = np.abs(f.E) ** 2
    my_mesh(f.x, f.y, I1, axes[0, 0])
    axes[0, 0].set_title(f'Original beam')

    my_mesh(f_end_diffused.x, f_end_diffused.y, np.abs(f_end_diffused.E)**2, axes[0, 1])
    axes[0, 1].set_title(f'far-field without fix')

    my_mesh(f_end_fixed.x, f_end_fixed.y, np.abs(f_end_fixed.E)**2, axes[1, 0])
    axes[1, 0].set_title(f'far-field with fix')

    my_mesh(f_end_fixed_with_dispersion.x, f_end_fixed_with_dispersion.y, np.abs(f_end_fixed_with_dispersion.E)**2, axes[1, 1])
    axes[1, 1].set_title(f'far-field with fix with dispersion of 190/270')


if __name__ == "__main__":
    kolmogorov_prop_single_screen()
    plt.show()


# In reality we would have wanted to put alpha = 270
# In practice with QM math we effectively put alpha = 190

# real WL independant phase is now 0-255
# to_slm_phase = phase * self.alpha / 255
# So if instead of wanted alpha=270 we put actually alpha=190, we effectively put a phase of wanted_phase * 190/270
