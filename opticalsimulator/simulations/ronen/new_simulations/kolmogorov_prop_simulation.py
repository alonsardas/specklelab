import numpy as np
import matplotlib.pyplot as plt
from opticalsimulator.simulations.ronen.misc_utils import my_mesh
from opticalsimulator.simulations.ronen.new_simulations.propagate import prop_angular_spectrum_paraxial, \
    prop_angular_spectrum, prop_phase_mask
from opticalsimulator.simulations.ronen.new_simulations.field import Field
from simulations.ronen.new_simulations.get_kolmogorov import get_kolmogorov


def kolmogorov_prop_single_screen(cn2=10e-15, L=1e3):
    plt.rcParams["figure.figsize"] = (9, 2.5)

    # From culimator
    sigma_0 = 10e-2
    Nx = 1024
    Ny = 1024
    Dx=sigma_0 * 10
    Dy=sigma_0 * 10
    dx = Dx/Nx
    dy = Dy/Ny

    x, y = Field.gen_x_y(Nx=Nx, Ny=Ny, Dx=Dx, Dy=Dy)
    f = Field(x, y, wavelength=808e-9)
    f.set_gausian(sigma_0)

    # Propagation
    A = get_kolmogorov(Nx, Ny, dx, dy, cn2=cn2, L=L, l_o=10, l_i=0.5e-2, is_test=False)

    f2 = prop_phase_mask(f, A)

    f3 = prop_angular_spectrum(f2, L)

    fig, axes = plt.subplots(1, 3, constrained_layout=True)
    # fig.suptitle(f'Propagation through Kolmogorov')

    I1 = np.abs(f.E) ** 2
    my_mesh(f.x, f.y, I1, axes[0])
    axes[0].set_title(f'Original beam')

    im = axes[1].imshow(A, cmap='gray')
    fig.colorbar(im, ax=axes[1])
    axes[1].set_title(f'Phase mask cn2={cn2}, L={L}')

    I3 = np.abs(f3.E) ** 2
    my_mesh(f3.x, f3.y, I3, axes[2])
    axes[2].set_title(f'speckle at far-field')

    return f3


def kolmogorov_prop_double_screen(cn2=10e-15, L=1e3):
    plt.rcParams["figure.figsize"] = (11.6, 2.5)

    # From culimator
    sigma_0 = 10e-2
    Nx = 1024
    Ny = 1024
    Dx=sigma_0 * 10
    Dy=sigma_0 * 10
    dx = Dx/Nx
    dy = Dy/Ny

    x, y = Field.gen_x_y(Nx=Nx, Ny=Ny, Dx=Dx, Dy=Dy)
    f = Field(x, y, wavelength=808e-9)
    f.set_gausian(sigma_0)

    k_light = 2 * np.pi / f.wavelength
    r0 = (0.4229 * (k_light ** 2) * L * cn2) ** (-3 / 5)
    r1 = 1.5775 * r0
    r2 = 1.4600 * r0

    # Propagation
    A1 = get_kolmogorov(Nx, Ny, dx, dy, cn2=cn2, L=L, l_o=10, l_i=0.5e-2, r0=r1, is_test=False)
    A2 = get_kolmogorov(Nx, Ny, dx, dy, cn2=cn2, L=L, l_o=10, l_i=0.5e-2, r0=r2, is_test=False)

    f2 = prop_angular_spectrum(f, L/3)
    f3 = prop_phase_mask(f2, A1)
    f4 = prop_angular_spectrum(f3, L/3)
    f5 = prop_phase_mask(f4, A2)
    f6 = prop_angular_spectrum(f5, L/3)

    fig, axes = plt.subplots(1, 4, constrained_layout=True)
    fig.suptitle(f'Propagation through Kolmogorov cn2={cn2}, L={L}')

    I1 = np.abs(f.E) ** 2
    my_mesh(f.x, f.y, I1, axes[0])
    axes[0].set_title(f'Original beam')

    im = axes[1].imshow(A1, cmap='gray')
    fig.colorbar(im, ax=axes[1])
    axes[1].set_title(f'first Phase mask')

    im = axes[2].imshow(A2, cmap='gray')
    fig.colorbar(im, ax=axes[2])
    axes[2].set_title(f'second Phase mask')

    I3 = np.abs(f6.E) ** 2
    my_mesh(f3.x, f3.y, I3, axes[3])
    axes[3].set_title(f'speckle at far-field')

    return f3


if __name__ == "__main__":
    # kolmogorov_prop_single_screen(cn2=1e-13, L=1e3)
    kolmogorov_prop_double_screen(cn2=1e-15, L=1e3)
    plt.show()
