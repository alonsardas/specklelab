import os
import glob
import numpy as np
import matplotlib.pyplot as plt
import cv2
from skimage import color
from skimage import io

from opticalsimulator import RealSLMDevice, VimbaCamera

IS_LAB = True


def gerchberg_saxton(input_intensity, path, iterations):
    target = color.rgb2gray(color.rgba2rgb((io.imread(path))))
    target = np.double(target)
    target = 1 - target
    target = cv2.resize(target, (input_intensity.shape[1], input_intensity.shape[0]), interpolation=cv2.INTER_AREA)

    A = np.fft.fftshift(np.fft.ifft2(np.fft.fftshift(target)))

    for i in range(iterations):
        B = np.abs(input_intensity) * np.exp(1j * np.angle(A))
        C = np.fft.fftshift(np.fft.fft2(np.fft.fftshift(B)))
        D = np.abs(target)*np.exp(1j * np.angle(C))
        A = np.fft.fftshift(np.fft.ifft2(np.fft.fftshift(D)))

    phase_mask = np.angle(A)

    return phase_mask


def main():
    root = os.path.abspath("data\\drawings\\*")
    print(root)
    drawings = glob.glob(root)
    for ind, drawing in enumerate(drawings):
        print(f'{ind}: {drawing}')
    i = int(input('Which image do you want? '))  # pick 6
    path = drawings[i]
    input_intensity = np.ones((1024, 1272))
    phases = gerchberg_saxton(input_intensity, path, 5)
    start_field = input_intensity * np.exp(1j*phases)
    end_field = np.fft.fftshift(np.fft.fft2(np.fft.fftshift(start_field)))
    end_power = np.abs(end_field)**2
    fig, ax = plt.subplots()
    ax.imshow(end_power)
    ax.set_title('Simulation')
    fig.show()

    if IS_LAB:
        slm = RealSLMDevice(1)
        slm.update_phase(phases)
        cam = VimbaCamera(1, exposure_time=5000)
        im = cam.get_image()
        fig, ax = plt.subplots()
        ax.imshow(np.flipud(im))
        fig.show()


if __name__ == '__main__':
    main()
    plt.show()
