import numpy as np
import time
from opticalsimulator.lab_games.photon_counter import PhotonCounter
from opticalsimulator.lab_games.motor import ThorLabsMotors
from opticalsimulator.simulations.ronen.optimize import Optimizer
from opticalsimulator.results import DynamicDiffuserResult


class MovingAtmosphereExp(object):

    result_path = fr'C:\Users\Owner\Google Drive\People\Ronen\results\temp\\{int(time.time())}dynamic_diffuser.ddr'
    optimizer_path = fr'C:\Users\Owner\Google Drive\People\Ronen\results\temp\\{int(time.time())}dynamic_diffuser.optimizer'

    def __init__(self, N1=100, N2=4000, N3=1500, integration_time=3, scan_mode=True):
        # TODO: check in lab comp. the params
        self.N1 = N1
        self.N2 = N2
        self.N3 = N3
        self.integration_time = integration_time
        self.scan_mode = scan_mode

        self.start_time = time.time()

        print("Getting Motors...")
        self.motors = ThorLabsMotors()
        print("Getting Optimizer...")
        self.optimizer = self.get_optimizer()
        print("Getting photon counter...")
        self.ph = PhotonCounter(integration_time=integration_time)
        self.result = DynamicDiffuserResult(N1=5, N2=5, N3=5)

    def get_time(self):
        return int(time.time() - self.start_time)

    def set_location(self):
        print("Setting Best location...")
        best_x = 5.84
        best_y = 9.35
        self.motors.move_x_absolute(best_x)
        self.motors.move_y_absolute(best_y)

    def get_optimizer(self):
        exposure_time = 4000
        slm_macro_pixels = 30

        sleep_period = 0.01
        a = 333
        b = 460

        # This is just a placeholder. actuall diffuser is changing, so this is kind of redundant...
        diffuser_mask = 2*np.pi*np.random.rand(3, 3)
        o = Optimizer(is_simulation=False, slices_of_interest=np.index_exp[a:a + 2, b:b + 2],
                      diffuser_mask=diffuser_mask,
                      macro_pixels_x_slm=slm_macro_pixels, macro_pixels_y_slm=slm_macro_pixels,
                      sleep_period=sleep_period, exposure_time=exposure_time,
                      run_name='kolmogorov_fixer', saveto_path=self.optimizer_path)

        return o

    def get_blue_power(self):
        try:
            im = self.optimizer.get_image()
            power = self.optimizer.power_at_target(im) * self.optimizer.power_scaling_factor
        except Exception as e:
            print(e)
            power = -1

        return power

    def get_red_power(self):
        if not self.scan_mode:
            _, _, power = self.ph.read_interesting()
            return power
        else:
            best_x = 5.87
            best_y = 9.32
            start_x = 5.8
            start_y = 9.275
            x_pixels = 3
            y_pixels = 3
            pixel_size_x = 0.025
            pixel_size_y = 0.025

            self.motors.move_x_absolute(start_x)
            self.motors.move_y_absolute(start_y)
            tot_power = 0

            for i in range(y_pixels):
                for j in range(x_pixels):
                    self.motors.move_x_relative(pixel_size_x)
                    _, _, power = self.ph.read_interesting()
                    tot_power += power

                self.motors.move_y_relative(pixel_size_y)
                self.motors.move_x_absolute(start_x)

            return tot_power

    def do_iter(self, also_red):
        self.result.blue_powers.append((self.get_time(), self.get_blue_power()))
        if also_red:
            self.result.red_powers.append((self.get_time(), self.get_red_power()))
        else:
            # Sleep some so when we don't scan red (which takes approx. 9X(integration_time+1)) we won't measure 100
            # points per second
            time.sleep(self.integration_time)
        self.result.saveto(self.result_path)
        self.optimizer.slm.restore_position()

    def run(self):
        if not self.scan_mode:
            self.set_location()

        self.result.red_powers = []
        self.result.blue_powers = []

        g = self.optimizer.optimize(method=Optimizer.PARTITIONING,
                                    iterations=(self.optimizer.macro_pixels_x_slm ** 2) * 200)

        RED_SAMPLE_DIST = 20

        for i in range(self.N1):
            print(f"N1: {i}")
            also_red = i % RED_SAMPLE_DIST == 0
            self.do_iter(also_red=also_red)

        self.result.N1 = self.get_time()

        for i in range(self.N2):
            print(f"N2: {i}")
            next(g)  # Does the optimize step
            also_red = i % RED_SAMPLE_DIST == 0
            self.do_iter(also_red=also_red)

        self.result.N2 = self.get_time()

        for i in range(self.N3):
            print(f"N3: {i}")
            also_red = i % RED_SAMPLE_DIST == 0
            self.do_iter(also_red=also_red)

        self.result.N3 = self.get_time()

    def finish(self):
        self.motors.close()


if __name__ == "__main__":
    exp = MovingAtmosphereExp()
    exp.run()
    exp.finish()
