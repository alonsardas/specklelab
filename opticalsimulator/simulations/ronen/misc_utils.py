import numpy as np
import matplotlib.pyplot as plt


# From https://stackoverflow.com/questions/398299/looping-in-a-spiral/1555236
def spiral_motor(X, Y):
    x = y = 0
    dx = 0
    dy = -1
    for i in range(max(X, Y) ** 2):
        if (-X / 2 < x <= X / 2) and (-Y / 2 < y <= Y / 2):
            yield x, y

        if x == y or (x < 0 and x == -y) or (x > 0 and x == 1 - y):
            dx, dy = -dy, dx
        x, y = x + dx, y + dy


# Same source but slightly different
def spiral(X, Y):
    x = y = 0
    dx = 0
    dy = -1
    for i in range(max(X, Y)**2):
        if (-X/2 < x <= X/2) and (-Y/2 < y <= Y/2):
            xx = (x + round(X / 2 - 0.01)) % X
            yy = (y + round(Y / 2 - 0.01)) % Y

            yield xx, yy

        if x == y or (x < 0 and x == -y) or (x > 0 and x == 1-y):
            dx, dy = -dy, dx
        x, y = x+dx, y+dy


def test_spiral():
    N = 5
    M = 10
    A = np.zeros([N, M])
    q = 1
    for a, b in spiral(N, M):
        A[a, b] = q
        q += 1
        print(A, '\n')


def my_mesh(X, Y, C, ax=None, clim=None, c_label=None):
    if ax is None:
        fig, ax = plt.subplots()
    if len(X) >= 2 and len(Y) >= 2:
        dx = (X[1] - X[0]) / 2
        dy = (Y[1] - Y[0]) / 2
        extent = (X[0]-dx, X[-1]+dx, Y[0]-dy, Y[-1]+dy)
        im = ax.imshow(C, extent=extent)
    else:
        im = ax.imshow(C)
    if clim:
        im.set_clim(*clim)
    cbar = ax.figure.colorbar(im, ax=ax)
    if c_label:
        cbar.set_label(c_label)
    return im


def save_phase(path, phase_grid):
    f = open(path, 'wb')
    np.savez(f, phase_grid=phase_grid)
    f.close()


def load_phase(path):
    phase_grid = np.load(path)['phase_grid']
    return phase_grid


if __name__ == '__main__':
    test_spiral()