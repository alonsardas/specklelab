import time
from opticalsimulator.elements.realslm import RealSLMDevice
from opticalsimulator.simulations.ronen.new_simulations.get_kolmogorov import get_kolmogorov

# Take into account both optimization iteration time and integration time of photon counter...
ITER_TIME = 3 + 2
ITERS_PER_PIXEL_MOTION = 10
# SLEEP_BETWEEN = ITER_TIME * ITERS_PER_PIXEL_MOTION
SLEEP_BETWEEN = 250


def main():
    # Diffuser
    slm2 = RealSLMDevice(2, use_mirror=True)

    MUL_X = 10
    MUL_Y = 10

    cn2 = 1e-15
    L = 1e3

    print("Initiating big phase mask")
    A = get_kolmogorov(N_x=slm2.active_x_pixels * MUL_X, N_y=slm2.active_y_pixels * MUL_Y,
                       pixel_size_x=slm2.pixel_size_x,
                       pixel_size_y=slm2.pixel_size_y,
                       cn2=cn2, L=L)

    i = 0
    while True:
        print(f'moving for the {i}th time')
        phase = A[i: i+slm2.active_y_pixels, i: i+slm2.active_x_pixels]
        slm2.update_phase_in_active(phase)
        slm2.restore_position()
        time.sleep(SLEEP_BETWEEN)
        i += 1


if __name__ == "__main__":
    main()
