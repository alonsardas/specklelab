import os
import cv2
import datetime
import numpy as np

from opticalsimulator.elements.realslm import RealSLMDevice
from opticalsimulator.elements.realcamera import VimbaCamera
from opticalsimulator.results import FullOptimizerExperimentResult
from opticalsimulator.simulations.ronen.optimize import Optimizer
from opticalsimulator.simulations.ronen.photon_scan import PhotonScanner
from opticalsimulator.results import OptimizerResult

LOGS_DIR = "C:\\temp"


class OptimizingExperiment(object):
    def __init__(self, config):

        self.slm1 = None
        self.slm2 = None
        self._init_slms()

        self.result = FullOptimizerExperimentResult()

        self.diffuser_mask = config['diffuser_mask']
        self.fixer_mask = config['fixer_mask']
        self.do_pre_red = config['do_pre_red']
        self.do_speckles_red = config['do_speckles_red']
        self.do_second_mode = config['do_second_mode']
        self.integration_pre = config['integration_pre']
        self.integration_speckles = config['integration_speckles']
        self.integration_post = config['integration_post']
        self.integration_second_mode = config['integration_second_mode']
        self.slm_macro_pixels = config['slm_macro_pixels']
        self.take_picture_exposure = config['take_picture_exposure']
        self.optimize_exposure = config['optimize_exposure']
        self.info = config['info']

        self.result.diffuser = self.diffuser_mask

        self._init_red_area()
        self.dir_path = self._init_results_dir()

    def _init_results_dir(self):
        timestamp = datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
        dir_path = fr"C:\Users\Owner\Google Drive\People\Ronen\results\temp\full_{timestamp}"
        os.mkdir(dir_path)
        print(f"Results will be saved in Here: {dir_path}")

        diffuser_path = os.path.join(dir_path, f'diffuser.diffuser')
        f = open(diffuser_path, 'wb')
        np.savez(f, diffuser=self.diffuser_mask)
        f.close()

        info_path = os.path.join(dir_path, f'info.txt')
        open(info_path, 'w').write(self.info)
        return dir_path

    def _init_slms(self):
        self.slm1 = RealSLMDevice(1)
        self.slm2 = RealSLMDevice(2, use_mirror=True)

    def _init_red_area(self):
        self.start_x = 5.5
        self.start_y = 9.075
        self.x_pixels = 28
        self.y_pixels = 22
        self.pixel_size_x = 0.025
        self.pixel_size_y = 0.025

        self.red_X = np.linspace(self.start_x, self.start_x+(self.x_pixels-1)*self.pixel_size_x, self.x_pixels)
        self.red_Y = np.linspace(self.start_y, self.start_y+(self.y_pixels-1)*self.pixel_size_y, self.y_pixels)
        self.red_Y = self.red_Y[::-1]

        self.result.red_X = self.red_X
        self.result.red_Y = self.red_Y

    def run(self):
        self.pre_measurements()
        self.speckle_measurements()
        self.fix()
        self.post_measurements()
        if self.do_second_mode:
            self.post_second_mode()

    def pre_measurements(self):
        print('--> Pre measurments <--')
        self.slm2.normal()
        self.slm1.normal()
        self.result.blue_orig = self.take_picture(name='blue_before.cam')
        self._save_result()

        if self.do_pre_red:
            self.result.red_orig = self.scan(name='red_before.scan', integration_time=self.integration_pre, quick=False)
            self._save_result()

    def speckle_measurements(self, with_red=True):
        print('--> Speckle Measurements <--')
        self.slm1.normal()
        self.slm2.update_phase_in_active(self.diffuser_mask)
        self.result.blue_speckles = self.take_picture(name='blue_speckles.cam')
        self.take_picture(name='blue_speckles_exp=2000.cam', exposure_time=2000)
        self._save_result()

        if self.do_speckles_red:
            self.result.red_speckles = self.scan(name='red_speckles.scan', integration_time=self.integration_speckles,
                                                 quick=False)
            self._save_result()

    def fix(self):
        print('--> Getting Fixer Phase <--')
        self.fixer_mask = self.get_fixing_mask()
        self.result.fixer = self.fixer_mask
        self._save_result()

    def post_measurements(self):
        print('--> Post Measurements <--')
        self.slm2.update_phase_in_active(self.diffuser_mask)
        self.slm1.update_phase_in_active(self.fixer_mask)
        self.result.blue_fixed = self.take_picture(name='blue_fixed.cam')
        self._save_result()

        self.result.red_fixed = self.scan(name='red_fixed.scan', integration_time=self.integration_post, quick=False)
        self._save_result()

    def post_second_mode(self):
        print('--> Post Measurements - Second Mode <--')
        self.slm2.update_phase_in_active(self.diffuser_mask)

        mask = self._get_second_mode_mask()
        self.slm1.update_phase_in_active(mask)
        self.result.blue_second_mode = self.take_picture(name='blue_second_mode.cam')
        self._save_result()

        self.result.red_second_mode = self.scan(name='red_second_mode.scan',
                                                integration_time=self.integration_second_mode, quick=False)
        self._save_result()

    def _get_second_mode_mask(self):
        mask = self.fixer_mask.copy()
        mask = cv2.resize(mask, (self.slm1.active_x_pixels, self.slm1.active_y_pixels), interpolation=cv2.INTER_AREA)

        x = int(self.slm1.active_x_pixels/2)
        pi_phase = np.zeros(mask.shape)
        pi_phase[:, x:] = np.pi

        return mask + pi_phase

    def _save_result(self):
        saveto_path = os.path.join(self.dir_path, 'full_result.foe')
        self.result.saveto(saveto_path)

    def close_slms(self):
        self.slm1.close()
        self.slm2.close()

    def get_fixing_mask(self):
        if self.fixer_mask is None:
            self.slm1.close()
            self.slm2.close()
            fixer = self.optimize()
            self._init_slms()
            return fixer
        else:
            # Flipping because SLMs stand opposite each other
            return self.fixer_mask

    def scan(self, name, integration_time, quick=False):
        x_pixels = self.x_pixels
        y_pixels = self.y_pixels
        pixel_size_x = self.pixel_size_x
        pixel_size_y = self.pixel_size_y

        if quick:
            x_pixels = 14
            y_pixels = 14
            pixel_size_x = 0.050
            pixel_size_y = 0.050

        saveto_path = os.path.join(self.dir_path, name)

        scanner = PhotonScanner(integration_time, self.start_x, self.start_y, x_pixels, y_pixels,
                                pixel_size_x, pixel_size_y, saveto_path=saveto_path)
        scanner.scan()
        return scanner.coincidences

    def take_picture(self, name, exposure_time=None):
        exposure_time = exposure_time or self.take_picture_exposure
        cam = VimbaCamera(0, exposure_time=exposure_time)
        saveto_path = os.path.join(self.dir_path, name)
        # TODO: make_save image make a label with the exposure time
        im = cam.save_image(saveto_path)
        cam.close()
        return im

    def optimize(self):
        print("Optimizing...")

        sleep_period = 0.1
        a = 334
        b = 460

        saveto_path = os.path.join(self.dir_path, 'optimization.optimizer')

        o = Optimizer(is_simulation=False, slices_of_interest=np.index_exp[a:a+2, b:b+2],
                      diffuser_mask=self.diffuser_mask,
                      macro_pixels_x_slm=self.slm_macro_pixels, macro_pixels_y_slm=self.slm_macro_pixels,
                      sleep_period=sleep_period, exposure_time=self.optimize_exposure,
                      run_name='foo', saveto_path=saveto_path)
        g = o.optimize(method=Optimizer.CONTINUOUS, iterations=int((self.slm_macro_pixels**2)))
        # g = o.optimize(method=Optimizer.PARTITIONING, iterations=(self.slm_macro_pixels**2)*2)

        for _ in g:
            pass

        o.slm.close()
        o.diffuser.close()

        return o.result.best_slm_phase_grid

    def finish(self):
        self.close_slms()
        f = open(fr"C:\Users\Owner\Google Drive\People\Ronen\results\done.bin", 'wb')
        f.write(b'foo')
        f.close()


if __name__ == "__main__":
    diffuser_path = "C:\\temp\\cn2=1e-15,L=1e3.b12.clouds"
    diffuser_mask = np.load(diffuser_path)['diffuser']

    fixer_opt = 2
    if fixer_opt == 0:
        fixer_mask = None
    elif fixer_opt == 1:
        fixer_mask = 2*np.pi - np.fliplr(diffuser_mask)
    elif fixer_opt == 2:
        optimize_path = r'C:\Users\Owner\Google Drive\People\Ronen\results\temp\part_30X30_cn2=1e-15L=1e3.e1.optimizer'
        o = OptimizerResult()
        o.loadfrom(optimize_path)
        fixer_mask = o.best_slm_phase_grid
        diffuser_mask = o.diffuser_phase_grid
    else:
        raise Exception()

    info = 'with partitioning optimization 30X30, cn2=1e-15 L=1e3 diffuser integration 5 30 10 20, cam exp 200'

    config = {
        'diffuser_mask': diffuser_mask,
        'fixer_mask': fixer_mask,
        'info': info,

        'do_pre_red': True,
        'do_speckles_red': True,
        'do_second_mode': True,

        'integration_pre': 5,
        'integration_speckles': 30,
        'integration_post': 10,
        'integration_second_mode': 20,
        'take_picture_exposure': 200,

        'slm_macro_pixels': 35,
        'optimize_exposure': 3000,
    }

    oe = OptimizingExperiment(config=config)

    if fixer_opt == 2:
        opt_path = os.path.join(oe.dir_path, f'optimization.optimizer')
        o.saveto(opt_path)

    oe.run()
    oe.finish()
