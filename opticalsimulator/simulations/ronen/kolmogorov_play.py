import numpy as np


def FT2(X, Y, g):
    """ G(fx, fy) = FT[g(X, Y)]
        Ohad style function
    """

    assert g.ndim == 2
    assert g[len(X)/2 + 1] == 0, 'Must follow Ohad convention X(N/2+1)=0'
    assert g[len(Y)/2 + 1] == 0, 'Must follow Ohad convention Y(N/2+1)=0'

    Nx = len(X)
    Ny = len(Y)
    dx = X[1] - X[0]
    dy = Y[1] - Y[0]

    G = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(g))) * dx**2
    dfx = 1/(Nx*dx)
    dfy = 1/(Ny*dy)

    fx = np.fft.ifftshift(np.fft.fftfreq(Nx, dx))
    fy = np.fft.ifftshift(np.fft.fftfreq(Ny, dy))

    fx2 = np.arange(-Nx/2, Nx/2) * dfx
    fy2 = np.arange(-Ny/2, Ny/2) * dfy

    return fx2, fy2, G

'''

    f_Xs, f_Ys = np.meshgrid(f_x, f_y)

    # Power spectrum (von Karman) (What?)
    PSD = np.exp(-(f_Xs ** 2 + f_Ys ** 2) / (2 * sigma ** 2))
    PSD[int(N_y / 2), int(N_x / 2)] = 0  # removing the zero freq term
    # print(f_Xs[int(N_y / 2), int(N_x / 2)], f_Ys[int(N_y / 2), int(N_x / 2)])
    rand_spectrum = (np.random.randn(N_y, N_x) + 1j * np.random.randn(N_y, N_x)) * np.sqrt(PSD)
    ifft = np.fft.fftshift(np.fft.ifft2(np.fft.ifftshift(rand_spectrum), norm='ortho'))
    slm_device.phase_grid = np.angle(ifft)

'''

'''
# Random # 
# Matlab 
rand('twister', 1337)
A = rand(5, 5)

# Python
np.random.seed(1337)
A = np.random.random((5, 5))
A = A.T.copy()

'''