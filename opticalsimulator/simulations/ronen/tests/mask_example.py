"""
just showing that the mask class works
"""

from opticalsimulator import *
import matplotlib.pyplot as plt


def main():
    fig, axes = plt.subplots(1, 2)

    borders = Borders(-1, -1, 1, 1)
    light_source = GaussianLightSource(sigma=0.3)
    field = create_field_grid_by_light_source(borders, 600, 600, light_source)

    input_camera = BasicCamera('input', axes[0])
    input_camera.prop_through(field)

    mask_border = Borders(-0.2, -0.2, 0.5, 0.5)
    mask = Mask(mask_border)
    field = mask.prop_through(field)
    output_camera = BasicCamera('output', axes[1])
    output_camera.prop_through(field)

    plt.show()


if __name__ == '__main__':
    main()
