"""
This is for checking the logic of finding pixels in _coords_to_phase in slmmask.py is correct
"""

import numpy as np

np.set_printoptions(threshold=10000, linewidth=130)
# This is the phase mask grid on SLM
pixels_x = 10
pixels_y = 10
A, B = np.meshgrid(np.linspace(1,10,pixels_x) + 0.1, np.linspace(1,10,pixels_y) + 0.1)
phase_mask = A + 0.1*B

# these give the physical location of SLM
min_x = 2
max_x = 4
min_y = 2
max_y = 5

pixel_size_x = (max_x - min_x) / pixels_x
pixel_size_y = (max_y - min_y) / pixels_y


# these are some locatino the beam is at
# Notice that some "miss" the SLM location
X = np.linspace(1, 8, 7)
Y = np.linspace(1, 6, 7)
Xs, Ys = np.meshgrid(X, Y)


moved_Xs = Xs - min_x
moved_Ys = Ys - min_y
x_pix_index = moved_Xs / pixel_size_x
y_pix_index = moved_Ys / pixel_size_y

x_pix_index = x_pix_index.astype('int')
y_pix_index = y_pix_index.astype('int')

to_zero_x = x_pix_index > pixels_x
to_zero_x = np.logical_or(to_zero_x, x_pix_index < 0)

to_zero_y = y_pix_index >= pixels_y
to_zero_y = np.logical_or(to_zero_y, y_pix_index < 0)

x_pix_index[to_zero_x] = -1
y_pix_index[to_zero_y] = -1

phases = phase_mask[x_pix_index, y_pix_index]
phases[to_zero_x] = 0
phases[to_zero_y] = 0

print(f'min_x: {min_x} max_x: {max_x}')
print(f'min_y: {min_y} max_y: {max_y}')
print(phases)
print(Xs)
print(Ys)
print(phase_mask)
