import numpy as np
import matplotlib.pyplot as plt
import cv2
from PIL import Image

macro_pixels_x = 24
macro_pixels_y = 24
micro_pixels_x = 1272
micro_pixels_y = 1024

fig, axes = plt.subplots(1,3)

phase_mask = 2 * np.pi * np.random.rand(macro_pixels_y, macro_pixels_x)
axes[0].imshow(phase_mask, cmap='gray', vmin=0, vmax=2*np.pi)


phase_mask2 = np.array(Image.fromarray(phase_mask).resize([micro_pixels_y, micro_pixels_x]))
axes[1].imshow(phase_mask2, cmap='gray', vmin=0, vmax=2*np.pi)

# interpolation does interesting things
phase_mask3 = cv2.resize(phase_mask, (micro_pixels_y, micro_pixels_x), interpolation=cv2.INTER_NEAREST)
axes[2].imshow(phase_mask3, cmap='gray', vmin=0, vmax=2*np.pi)

fig.show()
