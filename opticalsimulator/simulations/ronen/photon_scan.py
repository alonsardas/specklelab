import time
import datetime

from opticalsimulator import RealSLMDevice
from opticalsimulator.lab_games.motor import ThorLabsMotors
from opticalsimulator.lab_games.photon_counter import PhotonCounter

import numpy as np
import matplotlib.pyplot as plt

from opticalsimulator.results import ScanResult
from opticalsimulator.simulations.ronen.misc_utils import my_mesh
LOGS_DIR = "C:\\temp"


class PhotonScanner(object):
    best_x = 9.1
    best_y = 8.9

    def __init__(self, integration_time, start_x, start_y, x_pixels, y_pixels, pixel_size_x, pixel_size_y,
                 run_name='_scan', saveto_path=None):
        self.start_x = start_x or self.best_x - ((x_pixels-1)*pixel_size_x) / 2
        self.start_y = start_y or self.best_y - ((y_pixels-1)*pixel_size_y) / 2

        self.x_pixels = x_pixels
        self.y_pixels = y_pixels
        self.pixel_size_x = pixel_size_x
        self.pixel_size_y = pixel_size_y

        self.integration_time = integration_time
        self.run_name = run_name
        self.saveto_path = saveto_path

        self.X = np.linspace(self.start_x, self.start_x+(self.x_pixels-1)*self.pixel_size_x, self.x_pixels)
        self.Y = np.linspace(self.start_y, self.start_y+(self.y_pixels-1)*self.pixel_size_y, self.y_pixels)
        self.Y = self.Y[::-1]

        self.single1s = np.zeros((self.y_pixels, self.x_pixels))
        self.single2s = np.zeros((self.y_pixels, self.x_pixels))
        self.coincidences = np.zeros((self.y_pixels, self.x_pixels))

        self.result = ScanResult(X=self.X, Y=self.Y, integration_time=self.integration_time)
        self.timestamp = datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')

    def scan(self):
        print('getting motors...')
        ms = ThorLabsMotors()

        print('getting photon counter...')
        ph = PhotonCounter(integration_time=self.integration_time)

        print('Moving to starting position...')
        ms.move_x_absolute(self.start_x)
        ms.move_y_absolute(self.start_y)

        try:
            print('starting scan')
            count = 0
            start_time = time.time()
            for i in range(self.y_pixels):
                for j in range(self.x_pixels):
                    ms.move_x_relative(self.pixel_size_x)
                    self.single1s[i, j], self.single2s[i, j], self.coincidences[i, j] = ph.read_interesting()
                    duration_till_now = time.time() - start_time
                    print(f'dur: {int(duration_till_now)}. pix: {i}, {j}. Singles1: {self.single1s[i, j]}. '
                          f'Singles2: {self.single2s[i, j]}. Coincidence: {self.coincidences[i, j]}.')

                    count += 1
                    if count % 5 == 0:
                        self.result.coincidences = self.coincidences
                        self.result.single1s = self.single1s
                        self.result.single2s = self.single2s
                        self._save_result()

                    # In case you want it tp flash once in a while... Better to just look at result file with
                    # another ipython so it will be interactive and not "not responding"...
                    if count % 40000 == 0:
                        plt.close(1338)
                        fig, ax = plt.subplots(num=1338)
                        my_mesh(self.X, self.Y, self.coincidences, ax)
                        fig.show()
                        fig.savefig(f"C:\\temp\\{self.timestamp}coincidence.png")
                    plt.pause(0.03)  # Have plot interact a bit on every move

                ms.move_y_relative(self.pixel_size_y)
                ms.move_x_absolute(self.start_x)

            self.result.coincidences = self.coincidences
            self.result.single1s = self.single1s
            self.result.single2s = self.single2s
            self._save_result(final=True)

        except Exception as e:
            print('Exception occurred')
            print(e)
            import traceback
            traceback.print_exc()
        ms.close()
        ph.close()

        return self.single1s, self.single2s, self.coincidences

    def _save_result(self, final=False):
        BAK = ''  # if final else '.BAK'
        saveto_path = self.saveto_path or f"{LOGS_DIR}\\{self.timestamp}_scan{self.run_name}{BAK}.scan"
        print(f'saving result final={final}, path = {saveto_path}')
        self.result.saveto(saveto_path)

    def plot(self, mat):
        fig, ax = plt.subplots()
        my_mesh(self.X, self.Y, mat, ax)
        ax.set_title('Coincidence')

    def plot_coincidence(self, name):
        plt.close(1338)
        fig, ax = plt.subplots()
        my_mesh(self.X, self.Y, self.coincidences, ax)
        # ax.invert_yaxis()
        fig.show()
        fig.savefig(f"C:\\temp\\{time.time()}{name}.png")

    def plot_singles(self):
        fig, axes = plt.subplots(1, 2)
        xx, yy = np.meshgrid(self.X, self.Y)
        im0 = axes[0].pcolormesh(xx, yy, self.single1s, shading='nearest', vmin=0)
        im1 = axes[1].pcolormesh(xx, yy, self.single2s, shading='nearest', vmin=0)
        fig.suptitle('single counts')

        fig.colorbar(im0, ax=axes[0])
        fig.colorbar(im1, ax=axes[1])
        # axes[0].invert_yaxis()
        # axes[1].invert_yaxis()
        fig.show()


def scan_alphas_pi_step():
    best_x = 5.85
    best_y = 9.75
    best_z = 10  # Not very accurate, but seems OK

    integration_time = 10
    start_x = 5.825
    start_y = 9.5
    x_pixels = 2
    y_pixels = 20
    pixel_size_x = 0.025
    pixel_size_y = 0.025

    slm1 = RealSLMDevice(1)
    slm2 = RealSLMDevice(2)
    for alpha in np.linspace(110, 140, 16):
        slm2.config['alpha'] = alpha
        slm2.pi_step_y(440)
        scanner = PhotonScanner(integration_time, start_x, start_y, x_pixels, y_pixels, pixel_size_x, pixel_size_y,
                                run_name=f'_pi_step_alpha={alpha}')
        single1s, single2s, coincidences = scanner.scan()
        scanner.plot_coincidence(f'_pi_step_alpha={alpha}')

    plt.show()


def normal_area_scan(name='normal_area', integration_time=1):
    start_x = 5.5
    start_y = 9
    x_pixels = 28
    y_pixels = 28
    pixel_size_x = 0.025
    pixel_size_y = 0.025

    scanner = PhotonScanner(integration_time, start_x, start_y, x_pixels, y_pixels, pixel_size_x, pixel_size_y,
                            run_name=name)
    single1s, single2s, coincidences = scanner.scan()
    scanner.plot_coincidence(name)


def only_beam_scan(name='only_beam', integration_time=1, saveto_path=None):
    start_x = 5.7
    start_y = 9.25
    x_pixels = 12
    y_pixels = 8
    pixel_size_x = 0.025
    pixel_size_y = 0.025

    scanner = PhotonScanner(integration_time, start_x, start_y, x_pixels, y_pixels, pixel_size_x, pixel_size_y,
                            run_name=name, saveto_path=saveto_path)
    single1s, single2s, coincidences = scanner.scan()
    # scanner.plot_coincidence(name)


def minimal_beam_center_scan(name='minimal', integration_time=1):
    start_x = 5.8
    start_y = 9.325
    x_pixels = 5
    y_pixels = 3
    pixel_size_x = 0.025
    pixel_size_y = 0.025

    scanner = PhotonScanner(integration_time, start_x, start_y, x_pixels, y_pixels, pixel_size_x, pixel_size_y,
                            run_name=name)
    single1s, single2s, coincidences = scanner.scan()
    scanner.plot_coincidence(name)


if __name__ == '__main__':
    best_x = 5.85
    best_y = 9.75
    best_z = 10  # Not very accurate, but seems OK

    only_beam_scan('red_with_blue_fix')

    plt.show()
