from opticalsimulator.elements import *
import matplotlib.pyplot as plt
from skimage import filters
from skimage.measure import regionprops
import time


# Results:     ( x ,  y )
# SLM1 Center: (706, 373) +-5
# SLM2 Center: (395, 445) +-8

# SLM1 beam:
# rows 220 <--> 520
# cols 630 <--> 790

# SLM2 beam:
# rows 300 <--> 600
# cols 310 <--> 470


def get_center_of_mass(image):
    # From https://stackoverflow.com/questions/48888239/finding-the-center-of-mass-in-an-image
    threshold_value = filters.threshold_otsu(image)
    labeled_foreground = (image > threshold_value).astype(int)
    properties = regionprops(labeled_foreground, image)
    center_of_mass = properties[0].centroid
    # weighted_center_of_mass = properties[0].weighted_centroid
    return center_of_mass


def find_center_x(slm, cam, x_range=None):
    fig, ax = plt.subplots()
    ax.set_title('Find Center X')
    if not x_range:
        # x_range = np.linspace(705, 715, 11)  # SLM1
        x_range = np.linspace(390, 400, 11)   # SLM2

    for x in x_range:
        print(f'x:{x}')
        slm.pi_step(int(x))
        # slm1.pi_step_y(int(x))
        time.sleep(0.5)

        im = cam.get_image()
        row_c, col_c = get_center_of_mass(im)
        row_c = int(row_c)
        col_c = int(col_c)
        width = 12
        row_slice = im[row_c, col_c-width:col_c+width]
        ax.plot(row_slice, label=x)
        fig.show()
    ax.legend()


def find_center_y(slm, cam, y_range=None):
    fig, ax = plt.subplots()
    ax.set_title('Find Center Y')

    if not y_range:
        # y_range = np.linspace(368, 378, 11)  # SLM1
        y_range = np.linspace(440, 450, 11)  # SLM2

    for y in y_range:
        print(f'y:{y}')
        slm.pi_step_y(int(y))
        # slm1.pi_step_y(int(y))
        time.sleep(0.5)

        im = cam.get_image()
        row_c, col_c = get_center_of_mass(im)
        row_c = int(row_c)
        col_c = int(col_c)
        width = 12
        col_slice = im[row_c-width:row_c+width, col_c]
        ax.plot(col_slice, label=y)
        fig.show()
    ax.legend()


def get_mirror_phase(m, angle, sizex, sizey):
    X = np.linspace(0, m * np.pi, sizex)
    Y = np.linspace(0, m * np.pi, sizey)
    Xs, Ys = np.meshgrid(X, Y)
    phase = np.sin(angle) * Xs + np.cos(angle) * Ys
    return phase


def centers():
    slm1 = RealSLMDevice(1)
    slm2 = RealSLMDevice(2)
    cam = VimbaCamera(1, exposure_time=150)  # micro-s

    # find_center_x(slm1, cam)
    find_center_y(slm2, cam)


def show_real_area_slm1():
    # Watch in vimba viewer and see that things don't change
    slm1 = RealSLMDevice(1)
    slm2 = RealSLMDevice(2)
    angle = np.pi / 4
    m = 250

    while True:
        time.sleep(1)
        phase = get_mirror_phase(m, angle, slm1.correction.shape[1], slm1.correction.shape[0])

        phase[0:220, :] = 0
        phase[520:, :] = 0
        phase[:, 0:630] = 0
        phase[:, 790:] = 0

        slm1.update_phase(phase)

        time.sleep(1)
        phase = get_mirror_phase(m, angle, slm1.correction.shape[1], slm1.correction.shape[0])
        slm1.update_phase(phase)

        time.sleep(1)
        slm1.normal()


def show_real_area_slm2():
    # Watch in vimba viewer and see that things don't change
    slm1 = RealSLMDevice(1)
    slm2 = RealSLMDevice(2)
    angle = np.pi / 4
    m = 250

    while True:
        time.sleep(1)
        phase = get_mirror_phase(m, angle, slm1.correction.shape[1], slm1.correction.shape[0])
        # np.index_exp[295:595, 255:415]
        phase[0:295, :] = 0
        phase[595:, :] = 0
        phase[:, 0:310] = 0
        phase[:, 470:] = 0

        slm2.update_phase(phase)

        time.sleep(1)
        phase = get_mirror_phase(m, angle, slm1.correction.shape[1], slm1.correction.shape[0])
        slm2.update_phase(phase)

        time.sleep(1)
        slm2.normal()


if __name__ == '__main__':
    # centers()
    show_real_area_slm2()
    plt.show()
