"""
We tried to measure ared  pi_step, and got pretty lame results. see:
C:\\Users\Owner\Google Drive\People\Ronen\\results\\2020-10-4\pi_step_search_alpha
But - Maybe that is the best resolution we can get given the 50um fiber?
Let's check!

Run and see
Conclusion:
"""

import numpy as np
import matplotlib.pyplot as plt

from opticalsimulator.results import ScanResult

BLUE_PIXEL_SIZE = 4.8  # um - size of pixel of Mako U510B camera
EFFECTIVE_PIXEL_SIZE = BLUE_PIXEL_SIZE * 2  # Because red signal is two times larger
RED_PIXEL_SIZE = 50  # um - size of step in photon_scan
RECT_LEN = 5


def get_blue_vec():
    cam_path = r"C:\Users\Owner\Google Drive\People\Ronen\results\2020-10-5\pi_step + mirror 2\check_conv\blue_pi_step_y=430.cam"
    im = np.load(cam_path)['image']
    blue_vec = im[310:350, 460]
    return blue_vec


def get_red_vec(red_scan_path):
    r = ScanResult()
    r.loadfrom(red_scan_path)
    red_vec = r.coincidences[:, 1]
    return red_vec


def get_rect(blue_vec):
    rect = np.zeros(blue_vec.shape)
    middle = len(rect) / 2
    start = int(middle - RECT_LEN/2)
    # from consts above - fiber size is ~3 pixels
    rect[start:start+RECT_LEN] = 1
    return rect


def plot_rect(rect):
    rfig, ax = plt.subplots()
    ax.set_title("rect")
    ax.plot(rect, '*')
    rfig.show()


def plot_blue(blue_vec, rect):
    fig, ax = plt.subplots()
    ax.set_title("Blue Signal")
    ax.plot(blue_vec, '*--', label='blue before convolution')
    fig.show()

    blue_convolved = np.convolve(blue_vec, rect, mode='same')
    blue_convolved = blue_convolved / RECT_LEN  # Normalize by amount of points summed
    ax.plot(blue_convolved, '*--', label='blue after convolution')
    ax.legend()
    fig.show()


def plot_red(red_scan_path):
    red_vec = get_red_vec(red_scan_path)
    fig, ax = plt.subplots()
    ax.plot(red_vec, '*--')
    ax.set_title("Red signal")
    fig.show()


blue_vec = get_blue_vec()
rect = get_rect(blue_vec)
plot_blue(blue_vec, rect)

# plot_rect(rect)
red_scan_path = r"C:\Users\Owner\Google Drive\People\Ronen\results\2020-10-5\pi_step + mirror 2\check_conv\red_pi_step_alpha=135.npz"
plot_red(red_scan_path)

plt.show()
