import os
import time
from opticalsimulator.elements.realslm import RealSLMDevice
from opticalsimulator.elements.realcamera import VimbaCamera

BASE_PATH = r'C:\Users\Owner\Google Drive\People\Ronen\results\temp'

slm1 = RealSLMDevice(1)
slm2 = RealSLMDevice(2, use_mirror=True)
cam = VimbaCamera(0, exposure_time=150)

slm2.set_diffuser2(1.5e3)
slm2.save_diffuser(slm2.phase_grid, os.path.join(BASE_PATH, "gauss_diffuser_phase.diffuser"))
time.sleep(2)
cam.save_image(os.path.join(BASE_PATH, "gauss_diffuser_cam.cam"))

time.sleep(2)
slm2.set_kolmogorov(cn2=1e-15, L=1e3)
slm2.save_diffuser(slm2.phase_grid, os.path.join(BASE_PATH, "gauss_diffuser_phase"))
time.sleep(2)
cam.save_image(os.path.join(BASE_PATH, "gauss_diffuser_cam.cam"))
