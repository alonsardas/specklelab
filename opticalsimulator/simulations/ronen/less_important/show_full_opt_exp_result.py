import matplotlib.pyplot as plt
from opticalsimulator.results import FullOptimizerExperimentResult

import os
import sys

import matplotlib as mpl
# mpl.use('QT5Agg')

path = sys.argv[1]
name = os.path.basename(path)

r = FullOptimizerExperimentResult()
r.loadfrom(path)
r.show()
if r.red_second_mode.shape != (2, 2):
    r.show_second_mode()

e = r.enhancement
e2 = r.pump_enhancement

plt.show()
