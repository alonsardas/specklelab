import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from opticalsimulator.results import show_phase_mask

import matplotlib as mpl

path = sys.argv[1]
name = os.path.basename(path)
A = np.load(path)['diffuser']

show_phase_mask(A, title=name)

plt.show()
