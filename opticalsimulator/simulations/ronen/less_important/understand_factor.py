import os
import datetime
import numpy as np
from opticalsimulator import RealSLMDevice
from opticalsimulator.simulations.ronen.photon_scan import only_beam_scan, PhotonScanner


def different_methods(slm1, slm2, A):

    slm1.alpha = 215  # Normal
    slm2.alpha = 95  # Normal
    slm1.normal()
    slm2.normal()
    slm1.restore_position(); slm2.restore_position()
    only_beam_scan(saveto_path=os.path.join(dir_path, "normal.scan"))

    slm1.alpha = 215
    slm2.alpha = 95
    slm1.normal()
    slm2.update_phase_in_active(A)
    slm1.restore_position(); slm2.restore_position()
    only_beam_scan(saveto_path=os.path.join(dir_path, "speckles.scan"))

    slm1.alpha = 215
    slm2.alpha = 95
    slm1.update_phase_in_active(np.fliplr(-A))
    slm2.update_phase_in_active(A)
    slm1.restore_position(); slm2.restore_position()
    only_beam_scan(saveto_path=os.path.join(dir_path, "blue_fix.scan"), integration_time=3)

    slm1.alpha = 215
    slm2.alpha = 135
    slm1.update_phase_in_active(np.fliplr(-A))
    slm2.update_phase_in_active(A)
    slm1.restore_position(); slm2.restore_position()
    only_beam_scan(saveto_path=os.path.join(dir_path, "diffuser_135_fix.scan"), integration_time=3)

    slm1.alpha = 215
    slm2.alpha = 95
    slm1.update_phase_in_active(np.fliplr(-A)*(95/135))
    slm2.update_phase_in_active(A)
    slm1.restore_position(); slm2.restore_position()
    only_beam_scan(saveto_path=os.path.join(dir_path, "slm_factor_fix.scan"), integration_time=3)

    slm1.alpha = 151
    slm2.alpha = 95
    slm1.update_phase_in_active(np.fliplr(-A))
    slm2.update_phase_in_active(A)
    slm1.restore_position(); slm2.restore_position()
    only_beam_scan(saveto_path=os.path.join(dir_path, "slm_alpha_fix.scan"), integration_time=3)

    slm1.alpha = 215
    slm2.alpha = 95
    slm1.update_phase_in_active(np.fliplr(-A)*(135/95))
    slm2.update_phase_in_active(A)
    slm1.restore_position(); slm2.restore_position()
    only_beam_scan(saveto_path=os.path.join(dir_path, "slm_wrong_factor_fix.scan"), integration_time=3)


def find_slm1_alpha(slm1, slm2, A):
    for alpha1 in [190, 200, 210, 220, 230, 240]:
        slm1.alpha = alpha1
        slm2.alpha = 95
        slm1.update_phase_in_active(np.fliplr(-A))
        slm2.update_phase_in_active(A)
        slm1.restore_position(); slm2.restore_position()
        only_beam_scan(saveto_path=os.path.join(dir_path, f"fix_alpha={alpha1}.scan"), integration_time=5)


def find_slm1_factor(slm1, slm2, A):
    for factor in [0.6, 0.7, 0.8, 0.85, 0.9, 1, 1.1, 1.2, 1.3]:
        slm1.update_phase_in_active(np.fliplr(-A)*factor)
        slm2.update_phase_in_active(A)
        slm1.restore_position(); slm2.restore_position()
        only_beam_scan(saveto_path=os.path.join(dir_path, f"fix_factor={factor}.scan"), integration_time=5)


def orig_beam_scan(name='only_beam', integration_time=1, saveto_path=None):
    start_x = 5.65
    start_y = 9.325
    x_pixels = 16
    y_pixels = 2
    pixel_size_x = 0.025
    pixel_size_y = 0.025

    scanner = PhotonScanner(integration_time, start_x, start_y, x_pixels, y_pixels, pixel_size_x, pixel_size_y,
                            run_name=name, saveto_path=saveto_path)
    single1s, single2s, coincidences = scanner.scan()


def after_mirror_beam_scan(name='only_beam', integration_time=1, saveto_path=None):
    start_x = 6.175
    start_y = 9.325
    x_pixels = 16
    y_pixels = 2
    pixel_size_x = 0.025
    pixel_size_y = 0.025

    scanner = PhotonScanner(integration_time, start_x, start_y, x_pixels, y_pixels, pixel_size_x, pixel_size_y,
                            run_name=name, saveto_path=saveto_path)
    single1s, single2s, coincidences = scanner.scan()


def find_factor_with_mirrors(slm1, slm2):
    # Using mirrors since grating that isn't exactly 2*np.pi won't change the angle, would rather add diffraction orders
    assert isinstance(slm1, RealSLMDevice)
    mirror = slm1._get_mirror_phase(m=8, angle=np.pi/2)

    slm2.alpha = 95  # Regular
    # slm2.alpha = 135  # for red
    slm2.update_phase_in_active(mirror)
    # slm2.normal()
    slm1.alpha = 215  # Regular
    slm1.normal()
    after_mirror_beam_scan(saveto_path=os.path.join(dir_path, f"mirror_m=8_alpha2=95.scan"), integration_time=1)

    # slm2.alpha = 95  # Regular
    slm2.alpha = 135  # for red
    slm2.update_phase_in_active(mirror)
    slm1.alpha = 215  # Regular
    slm1.normal()
    after_mirror_beam_scan(saveto_path=os.path.join(dir_path, f"mirror_m=8_alpha2=135.scan"), integration_time=1)


timestamp = datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
dir_path = fr"C:\Users\Owner\Google Drive\People\Ronen\results\2020-10-14\mirrors\{timestamp}"
os.mkdir(dir_path)

slm1 = RealSLMDevice(1)
slm2 = RealSLMDevice(2, use_mirror=True)

A = 2 * np.pi * np.random.rand(9, 9)
# different_methods(slm1, slm2, A)
# find_slm1_factor(slm1, slm2, A)
find_factor_with_mirrors(slm1, slm2)

# slm1.update_phase_in_active(np.fliplr(-mirror))
