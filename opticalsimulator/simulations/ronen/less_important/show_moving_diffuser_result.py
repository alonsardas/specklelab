import matplotlib.pyplot as plt
from opticalsimulator.results import DynamicDiffuserResult

import os
import sys

import matplotlib as mpl
# mpl.use('QT5Agg')

path = sys.argv[1]
name = os.path.basename(path)

ddr = DynamicDiffuserResult()
ddr.loadfrom(path)
ddr.show()
e = ddr.enhancement
e2 = ddr.pump_enhancement
# print("enhancement", ddr.enhancement)
plt.show()
