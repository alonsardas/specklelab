import numpy as np
import matplotlib.pyplot as plt
from numpy.fft import fft, fftshift, ifft, ifftshift

from opticalsimulator import VimbaCamera, RealSLMDevice

cam = VimbaCamera(1, exposure_time=45)

slm1 = RealSLMDevice(1)
slm2 = RealSLMDevice(2)

# Do some by-hand fitting
im = cam.get_image()
I = im[275, 150:450]
I = I.astype('double')
x = np.linspace(1, len(I), len(I))
x -= 139
fig, ax = plt.subplots()
ax.plot(x, I)
fig.show()
A = 550
s = 67
ax.plot(x, A*np.exp(-x**2/s))
fig.show()

# Calc what will happen with pi_step
new_x = np.linspace(-50, 50, 5000)
fit_I = A*np.exp(-new_x**2/s)
fig, ax = plt.subplots()
ax.plot(new_x, fit_I)
fig.show()

E = np.sqrt(fit_I)

slm_z = fftshift(ifft(ifftshift(E)))

pi_step = np.zeros(5000)
pi_step[2500:] = np.pi
new_slm_z = np.abs(slm_z) * np.exp(1j*pi_step)

double_gaus_E = fftshift(fft(ifftshift(new_slm_z)))
double_gaus_I = np.abs(double_gaus_E)**2
ax.plot(new_x, double_gaus_I)
fig.show()

plt.show()

# Check what really happens with pi_step
slm2.normal()
im = cam.get_image()
I = im[275, 150:450]
I = I.astype('double')
x = np.linspace(1, len(I), len(I))
x -= 139
fig, ax = plt.subplots()
ax.plot(x, I)
fig.show()

slm2.pi_step(598)
im = cam.get_image()
I = im[275, 150:450]
I = I.astype('double')
ax.plot(x, I)
fig.show()


def show_pi_step_result():
    A = 5
    s = 1

    X = np.linspace(-3, 3, 5000)
    I = A * np.exp(-X ** 2 / s)
    fig, ax = plt.subplots()
    ax.plot(X, I)
    fig.show()

    EE = np.sqrt(I)
    E = fftshift(ifft(ifftshift(EE)))

    pi_step = np.zeros(5000)
    pi_step[2500:] = np.pi
    new_E = np.abs(E) * np.exp(1j * pi_step)

    double_gaus_E = fftshift(fft(ifftshift(new_E)))
    double_gaus_I = np.abs(double_gaus_E) ** 2
    ax.plot(X, double_gaus_I)
    fig.show()

    plt.show()

