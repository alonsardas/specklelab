import numpy as np
import matplotlib.pyplot as plt

import os
import sys

import matplotlib as mpl
# mpl.use('QT5Agg')

path = sys.argv[1]
name = os.path.basename(path)

im = np.load(path)['image']
fig, axes = plt.subplots(num=name)
im = axes.imshow(im)
axes.set_title(name)
fig.colorbar(im, ax=axes)
fig.show()

plt.show()
