import matplotlib.pyplot as plt
import time
from opticalsimulator import *


def main():
    fig, axes = plt.subplots(3, 3)

    borders = Borders(-1, -1, 1, 1)
    light_source = BoxLightSource(20, 20)

    f = 0.1
    lense = LenseMask(borders, 0, 0, f=f)

    for dist, ax in zip(np.linspace(0, 0.1, 9), axes.flat):
        freespace_prop = AngularMomentumPropagator(dist)

        field = create_field_grid_by_light_source(borders, 400, 400, light_source)
        field = lense.prop_through(field)
        field = freespace_prop.apply(field)
        output_camera = BasicCamera(f'z={dist}', ax)
        output_camera.apply(field)

    plt.show()


if __name__ == "__main__":
    main()
