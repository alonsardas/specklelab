import cv2
import time
import glob
import datetime
import traceback
from opticalsimulator.elements import *
from opticalsimulator.simulations.ronen.misc_utils import spiral
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

LOGS_DIR = 'C:\\temp'


class CharacterazationResult(object):
    def __init__(self, diffuser_phase_grid=None, pixel_heat_map=None, original_speckle_pattern=None):
        self.diffuser_phase_grid = diffuser_phase_grid
        self.pixel_heat_map = pixel_heat_map
        self.original_speckle_pattern = original_speckle_pattern

    def show(self):
        fig, ax = plt.subplots()
        fig.suptitle('Results Of Characterization')
        ax.imshow(self.pixel_heat_map)
        fig.show()
        fig.canvas.flush_events()

    def saveto(self, path):
        f = open(path, 'wb')
        np.savez(f,
                 diffuser_phase_grid=self.diffuser_phase_grid,
                 pixel_heat_map=self.pixel_heat_map,
                 original_speckle_pattern=self.original_speckle_pattern)
        f.close()

    def loadfrom(self, path=None):
        if path is None:
            paths = glob.glob(f"{LOGS_DIR}\\*.npz")
            for i, path in enumerate(paths):
                print(f'{i}: {path}')
            choice = int(input('which one?'))
            path = paths[choice]

        f = open(path, 'rb')
        data = np.load(f, allow_pickle=True)

        self.diffuser_phase_grid = data['diffuser_phase_grid']
        self.pixel_heat_map = data['pixel_heat_map']
        self.original_speckle_pattern = data['original_speckle_pattern']
        f.close()


class PixelCharacterizer(object):
    """
        In order to optimize the Optimizer in optimize.py we want to characterize which pixels are actually effective on first SLM.
    """
    MICRO_PIXELS_X = 1272
    MICRO_PIXELS_Y = 1024
    MAG = 60
    MACRO_PIXELS_X_SLM = int(np.round(MICRO_PIXELS_X / MAG))
    MACRO_PIXELS_Y_SLM = int(np.round(MICRO_PIXELS_Y / MAG))
    MACRO_PIXELS_X_DIFFUSER = int(np.round(MICRO_PIXELS_X / MAG))
    MACRO_PIXELS_Y_DIFFUSER = int(np.round(MICRO_PIXELS_Y / MAG))
    FIELD_PIXELS_X = 200
    FIELD_PIXELS_Y = 200
    SLICE_OF_INTEREST = np.index_exp[99:101, 99:101]
    POINTS_FOR_LOCK_IN = 20

    BORDERS = Borders(-0.010, -0.010, 0.010, 0.010)
    WAVELENGTH = 808e-9
    FOCAL_LENGTH = 0.300

    def __init__(self, is_simulation=True, slice_of_interest=None,
                 macro_pixels_x_diffuser=None, macro_pixels_y_diffuser=None,
                 macro_pixels_x_slm=None, macro_pixels_y_slm=None,
                 sleep_period=0.0, exposure_time=1000, run_name='characterizer_result'):
        """
        Some numbers:
        - sigma / macro_pixels: it is important to small enough macro_pixels, so enough of them encounter the beam waist
        """
        self.is_simulation = is_simulation
        self.slice_of_interest = slice_of_interest or self.SLICE_OF_INTEREST
        self.macro_pixels_x_diffuser = macro_pixels_x_diffuser or self.MACRO_PIXELS_X_DIFFUSER
        self.macro_pixels_y_diffuser = macro_pixels_y_diffuser or self.MACRO_PIXELS_Y_DIFFUSER
        self.macro_pixels_x_slm = macro_pixels_x_slm or self.MACRO_PIXELS_X_SLM
        self.macro_pixels_y_slm = macro_pixels_y_slm or self.MACRO_PIXELS_Y_SLM
        self.sleep_period = sleep_period
        self.exposure_time = exposure_time
        self.power_scaling_factor = 1
        self.run_name = run_name

        self.result = CharacterazationResult()

        self.fig, self.pixel_heat_map_ax = plt.subplots()
        self.cos_fig, self.cos_ax = plt.subplots()
        self.cos_ax.set_title('changing cos fit')

        if self.is_simulation:
            self.light_source = GaussianLightSource(sigma=0.0035, wavelength=self.WAVELENGTH)
            self.slm = SLMMask(self.BORDERS, pixels_x=self.MICRO_PIXELS_X, pixels_y=self.MICRO_PIXELS_Y)
            self.diffuser = SLMMask(self.BORDERS, pixels_x=self.MICRO_PIXELS_X, pixels_y=self.MICRO_PIXELS_Y)
            self.farfield = FourierLens(f=self.FOCAL_LENGTH)
        else:
            # TODO: some "now_oposite" param
            self.slm = RealSLMDevice(1)
            self.diffuser = RealSLMDevice(2)
            self.camera = VimbaCamera(1, exposure_time=self.exposure_time)  # micro-s

        self._init_diffuser()
        self._show_initial_speckles()

        self.pixel_heat_map = np.zeros([self.macro_pixels_y_slm, self.macro_pixels_x_slm])
        self.timestamp = datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')

    # Preparation functions
    def _init_diffuser(self):
        diffuser_mask = self._generate_diffuser_mask()
        self.diffuser.update_phase(diffuser_mask)
        # This happens only once, so why not...
        time.sleep(self.sleep_period * 3)
        self.result.diffuser_phase_grid = diffuser_mask

    def _generate_diffuser_mask(self):
        phase_mask = 2 * np.pi * np.random.rand(self.macro_pixels_y_diffuser, self.macro_pixels_x_diffuser)
        phase_mask = cv2.resize(phase_mask, (self.MICRO_PIXELS_X, self.MICRO_PIXELS_Y),
                                interpolation=cv2.INTER_AREA)

        # fig, ax = plt.subplots()
        # show_phase_mask(phase_mask, ax, 'diffuser_mask')

        return phase_mask

    def _show_initial_speckles(self):
        if self.is_simulation:
            field = self.simulate_propagation()
            im = self._field_to_powers(field)
        else:
            im = self.camera.get_image()
        fig, ax = plt.subplots()
        self.show_power(ax, im, 'Original Speckle Pattern')
        self.result.original_speckle_pattern = im

    # Optimization functions
    def characterize(self, should_spiral=True):
        try:
            pixel_number = 1
            if should_spiral:
                for i, j in spiral(self.macro_pixels_y_slm, self.macro_pixels_x_slm):
                    print(f'pixel_number: {pixel_number}/{self.macro_pixels_y_slm * self.macro_pixels_x_slm}')
                    start_time = time.time()

                    amplitude = self.get_amplitude(i, j)
                    print(f'amplitude: {amplitude}')

                    self.pixel_heat_map[i, j] = amplitude

                    self.update_heat_mask()  # Show on screen. and update result to be saved
                    pixel_number += 1

                    duration = round(time.time() - start_time, 2)
                    time.sleep(self.sleep_period*3)  # Have a bigger sleep between pixels
                    print(f'took {duration} seconds')

                    if pixel_number % 300 == 0:
                        self._save_result()

        except Exception:
            print('==>ERROR!<==')
            traceback.print_exc()

        self.update_heat_mask()
        self._save_result(final=True)

    def update_heat_mask(self):
        try:
            self.show_power(self.pixel_heat_map_ax, self.pixel_heat_map, 'pixel heat map')
            self.result.pixel_heat_map = self.pixel_heat_map
        except Exception:
            print('==>ERROR!<==')
            traceback.print_exc()

    def get_amplitude(self, i, j):
        powers = []
        phis = np.linspace(0, 2 * np.pi, self.POINTS_FOR_LOCK_IN)
        phis = phis[:-1]  # Don't need both 0 and 2*pi

        for phi in phis:
            power = self._phi_to_power(phi, i, j)
            powers.append(power)

        amplitude = 2*self.fit_to_cos(phis, powers)  # factor of 2 because a*cos is only half of max_distance
        amplitude2 = self.approx_amplitude2(powers)

        return max(amplitude, amplitude2)

    def approx_amplitude2(self, powers):
        sorted_powers = sorted(powers)
        # TODO: Is this the logical thing to do?
        return sorted_powers[-2] - sorted_powers[1]  # cut off wierd edges

    def _phi_to_power(self, phi, i, j):
        cur_phase = np.zeros([self.macro_pixels_y_slm, self.macro_pixels_x_slm])
        cur_phase[i, j] = phi

        phase_mask = cv2.resize(cur_phase, (self.MICRO_PIXELS_X, self.MICRO_PIXELS_Y),
                                interpolation=cv2.INTER_AREA)
        self.slm.update_phase(phase_mask)
        time.sleep(self.sleep_period)

        if self.is_simulation:
            field = self.simulate_propagation()
            powers = self._field_to_powers(field)
        else:
            powers = self.camera.get_image()
        power = self.power_at_target(powers)

        return power

    def fit_to_cos(self, phis, powers, should_plot=True):
        # a, w, phi, c
        min_bounds = [0, 0.8, 0, 0]
        max_bounds = [np.inf, 1.2, 2*np.pi, np.inf]
        popt, pcov = curve_fit(self._cos, phis, powers, bounds=(min_bounds, max_bounds))
        a, w, phi, c = popt

        if should_plot:
            self.cos_ax.clear()
            self.cos_ax.plot(phis, powers, '*')
            P = np.linspace(0, 2*np.pi, 30)
            self.cos_ax.plot(P, self._cos(P, *popt), '--')
            self.cos_fig.show()
            self.cos_fig.canvas.flush_events()

        return a

    def _cos(self, x, a, w, phi, c):
        return a * np.cos(w * x + phi) + c

    def power_at_target(self, powers):
        power = np.sum(powers[self.slice_of_interest].flatten())
        return power

    def _save_result(self, final=False):
        BAK = '' if final else '.BAK'
        saveto_path = f"{LOGS_DIR}\\{self.timestamp}_{self.run_name}.npz{BAK}"
        self.result.saveto(saveto_path)

    # Simulation functions
    def simulate_propagation(self):
        assert self.is_simulation
        assert isinstance(self.slm, SLMMask)
        assert isinstance(self.diffuser, SLMMask)

        # TODO: do this once in init and deep copy each time instead of recreate
        field = create_field_grid_by_light_source(self.BORDERS, self.FIELD_PIXELS_X, self.FIELD_PIXELS_Y,
                                                  self.light_source)
        field = self.slm.prop_through(field)
        field = self.diffuser.prop_through(field)
        field = self.farfield.prop_through(field)
        return field

    def _field_to_powers(self, field):
        return np.abs(field.Es) ** 2

    # Plotting functions
    def show_power(self, ax, im, name='power'):
        ax.clear()
        ax.pcolormesh(im, shading='nearest')
        ax.set_title(name)
        ax.figure.show()
        ax.figure.canvas.flush_events()


if __name__ == '__main__':
    if True:  # Lab
        # This way we have enough of a speckle effect, enough exposure to see them, and the nothing (including DC)
        # isn't over exposed
        diffuser_macro_pixels = 150
        exposure_time = 1000
        slm_macro_pixels = 30
        c = PixelCharacterizer(is_simulation=False, slice_of_interest=np.index_exp[300:302, 300:302],
                               macro_pixels_x_diffuser=diffuser_macro_pixels,
                               macro_pixels_y_diffuser=diffuser_macro_pixels,
                               macro_pixels_x_slm=slm_macro_pixels,
                               macro_pixels_y_slm=slm_macro_pixels,
                               sleep_period=0.5, exposure_time=exposure_time,
                               run_name='characterize_slm1_30x30')
        c.characterize()

    else:  # Simulation
        diffuser_macro_pixels = 150
        exposure_time = 1000
        slm_macro_pixels = 10

        c = PixelCharacterizer(is_simulation=True,
                               macro_pixels_x_diffuser=diffuser_macro_pixels,
                               macro_pixels_y_diffuser=diffuser_macro_pixels,
                               macro_pixels_x_slm=slm_macro_pixels,
                               macro_pixels_y_slm=slm_macro_pixels,
                               sleep_period=0.001)
        c.characterize()
    plt.show()
