import os
import matplotlib.pyplot as plt
import numpy as np

from opticalsimulator import RealSLMDevice
from opticalsimulator.results import FullOptimizerExperimentResult, DynamicDiffuserResult

import matplotlib as mpl

from results import OptimizerResult
from simulations.ronen.misc_utils import my_mesh

mpl.use('QT5Agg')

FIG3_DATA_PATH = r"C:\ronen\HUJI\Amirim\Paper\Data For Figures\fig3_data.foe"
FIG3_OUT_PATH = r"C:\ronen\HUJI\Amirim\Paper\Lyx Figures\Figure 3 - Speckle pattern.png"

FIG4_DATA_PATH = r"C:\ronen\HUJI\Amirim\Paper\Data For Figures\fig4_data.foe"
FIG4_OUT_PATH = r"C:\ronen\HUJI\Amirim\Paper\Lyx Figures\Figure 4 - Optimization works.png"

FIG5_DATA_PATH = r"C:\ronen\HUJI\Amirim\Paper\Data For Figures\fig4_data.foe"  # Same data as fig4
FIG5_OUT_PATH = r"C:\ronen\HUJI\Amirim\Paper\Lyx Figures\Figure 5 - Second mode.png"

FIG6_DATA_PATH = r"C:\ronen\HUJI\Amirim\Paper\Data For Figures\fig6_data.ddr"
FIG6_OUT_PATH = r"C:\ronen\HUJI\Amirim\Paper\Lyx Figures\Figure 6 - Moving Atmosphere.png"

FIG2_DATA1_PATH = r"C:\ronen\HUJI\Amirim\Paper\Data For Figures\fig2_data1.diffuser"
FIG2_DATA2_PATH = r"C:\ronen\HUJI\Amirim\Paper\Data For Figures\fig2_data2.diffuser"
FIG2_OUT_PATH = r"C:\ronen\HUJI\Amirim\Paper\Lyx Figures\Figure 22 - Diffuser phase masks.png"


def fig3():
    plt.rcParams["figure.figsize"] = (6, 3.8)
    r = FullOptimizerExperimentResult()
    r.loadfrom(FIG3_DATA_PATH, remove_accidentals='fig3')
    fig = r.show_fig3(fig4=False)
    plt.show()

    fig.savefig(FIG3_OUT_PATH)


def fig3v2():
    FIG3_OUT_PATHV2 = r"C:\ronen\HUJI\Amirim\Paper\Lyx Figures\Figure 3 new - Speckle pattern.png"
    plt.rcParams["figure.figsize"] = (6.8, 2.1)
    r = FullOptimizerExperimentResult()
    r.loadfrom(FIG3_DATA_PATH, remove_accidentals='fig3')
    fig = r.show_fig3v2()
    plt.show()

    fig.savefig(FIG3_OUT_PATHV2)


def fig4():
    plt.rcParams["figure.figsize"] = (6, 6)
    r = FullOptimizerExperimentResult()
    r.loadfrom(FIG4_DATA_PATH, remove_accidentals=True)
    fig = r.show_fig3(fig4=True)
    plt.show()

    fig.savefig(FIG4_OUT_PATH)


def fig5():
    plt.rcParams["figure.figsize"] = (6.75, 2.1)
    r = FullOptimizerExperimentResult()
    r.loadfrom(FIG5_DATA_PATH, remove_accidentals=True)
    fig = r.show_fig5()
    plt.show()

    fig.savefig(FIG5_OUT_PATH)


def fig6(conv_rect=30):
    plt.rcParams.update({'font.size': 14})
    plt.rcParams["figure.figsize"] = (9, 5)

    ddr = DynamicDiffuserResult()
    ddr.loadfrom(FIG6_DATA_PATH)
    fig, ax = ddr.show_fig6_new(conv_rect=conv_rect)
    plt.show()

    fig.savefig(FIG6_OUT_PATH)


def create_fig2_data():
    mpl.use('TKAgg')

    slm = RealSLMDevice(10)
    slm.set_diffuser2(1.5e3)
    slm.save_diffuser(slm.phase_grid, FIG2_DATA1_PATH)
    slm.set_kolmogorov(1e-15)
    slm.save_diffuser(slm.phase_grid, FIG2_DATA2_PATH)


def fig2():
    plt.rcParams.update({'font.size': 14})
    plt.rcParams["figure.figsize"] = (8, 2.5)

    fig, axes = plt.subplots(1, 2, constrained_layout=True)
    A1 = np.load(FIG2_DATA1_PATH)['diffuser']
    A2 = np.load(FIG2_DATA2_PATH)['diffuser']

    im1 = axes[0].imshow(A1, cmap='gray')
    im2 = axes[1].imshow(A2, cmap='gray')
    cb1 = fig.colorbar(im1, ax=axes[0])
    cb2 = fig.colorbar(im2, ax=axes[1])
    cb1.set_label("Phase (Rad)")
    cb2.set_label("Phase (Rad)")
    axes[0].set_title('(A) Gaussian')
    axes[1].set_title('(B) Kolmogorov')

    axes[0].get_xaxis().set_visible(False)
    axes[0].get_yaxis().set_visible(False)
    axes[1].get_xaxis().set_visible(False)
    axes[1].get_yaxis().set_visible(False)

    fig.show()

    plt.show()
    fig.savefig(FIG2_OUT_PATH)


def new_fig2():
    # NEW_FIG_2_DATA_DIR_PATH = r"C:\Temp\2021.03.15\6"
    NEW_FIG_2_DATA_DIR_PATH = r"C:\Users\ronen\Google Drive (complexphoton@mail.huji.ac.il)\People\Ronen\results\2021-03-15\9"
    # NEW_FIG_2_DATA_DIR_PATH = r"C:\ronen\HUJI\Amirim\Paper\Data For Figures\new_fig_2"
    NEW_FIG_2_OUT_PATH = r"C:\ronen\HUJI\Amirim\Paper\Lyx Figures\Figure 2 new - phase screens.png"

    plt.rcParams.update({'font.size': 14})
    plt.rcParams["figure.figsize"] = (7, 4.5)

    fig, axes = plt.subplots(2, 2, constrained_layout=True)
    d = 70
    # Take only relevant part of phase mask
    A1 = np.load(os.path.join(NEW_FIG_2_DATA_DIR_PATH, 'gauss_diffuser_phase.diffuser'))['diffuser'][315+d:460+d, 325:470]
    A2 = np.load(os.path.join(NEW_FIG_2_DATA_DIR_PATH, 'kolmogorov_diffuser_phase.clouds'))['diffuser'][315+d:460+d, 325:470]

    im1 = axes[0, 0].imshow(A1, cmap='gray')
    im2 = axes[0, 1].imshow(A2, cmap='gray')
    cb1 = fig.colorbar(im1, ax=axes[0, 0])
    cb2 = fig.colorbar(im2, ax=axes[0, 1])
    cb1.set_label("Phase (Rad)")
    cb2.set_label("Phase (Rad)")
    axes[0, 0].set_title('(A1) Gaussian screen')
    axes[0, 1].set_title('(B1) Kolmogorov screen')

    axes[0, 0].get_xaxis().set_visible(False)
    axes[0, 0].get_yaxis().set_visible(False)
    axes[0, 1].get_xaxis().set_visible(False)
    axes[0, 1].get_yaxis().set_visible(False)

    B1 = np.load(os.path.join(NEW_FIG_2_DATA_DIR_PATH, 'gauss_speckles_cam.cam'))['image'][200:420, 440:660]
    B2 = np.load(os.path.join(NEW_FIG_2_DATA_DIR_PATH, 'kolmogorov_speckles_cam.cam'))['image'][200:420, 440:660]

    x_pixels, y_pixels = B1.shape
    # each pixel is 4.8 um = 4.8e-3 mm
    BLUE_X = np.linspace(0, x_pixels * 4.8e-3, x_pixels)
    BLUE_X -= np.mean(BLUE_X)
    BLUE_Y = np.linspace(0, y_pixels * 4.8e-3, y_pixels)
    BLUE_Y -= np.mean(BLUE_Y)

    my_mesh(BLUE_X, BLUE_Y, B1, axes[1, 0], c_label='arbitrary units')
    my_mesh(BLUE_X, BLUE_Y, B2, axes[1, 1], c_label='arbitrary units')

    axes[1, 0].set_title('(A2) Gaussian speckles')
    axes[1, 1].set_title('(B2) Kolmogorov speckles')

    axes[1, 0].set_xlabel("x (mm)")
    axes[1, 0].set_ylabel("y (mm)")
    axes[1, 1].set_xlabel("x (mm)")
    axes[1, 1].set_ylabel("y (mm)")

    fig.show()

    plt.show()
    fig.savefig(NEW_FIG_2_OUT_PATH)


def fig_nano():
    FIG_NANO_OUT_PATH = r"C:\ronen\HUJI\Amirim\Paper\Lyx Figures\Figure Nano.png"
    plt.rcParams["figure.figsize"] = (3, 4)
    r = FullOptimizerExperimentResult()
    r.loadfrom(FIG5_DATA_PATH)
    fig = r.show_nano()
    plt.show()

    fig.savefig(FIG_NANO_OUT_PATH)


def only_kolmogorov_phase():
    plt.rcParams.update({'font.size': 12})
    plt.rcParams["figure.figsize"] = (5.2, 4)

    FIG_OUT_KOLMOGOROV = r"C:\ronen\HUJI\Amirim\Paper\Lyx Figures\Figure kolmogorov phase screen.png"
    r = FullOptimizerExperimentResult()
    r.loadfrom(FIG5_DATA_PATH)
    fig = r.show_diffuser()
    plt.show()

    fig.savefig(FIG_OUT_KOLMOGOROV)


def only_kolmogorov_plain():
    plt.rcParams.update({'font.size': 12})
    plt.rcParams["figure.figsize"] = (5.2, 4)

    FIG_OUT_KOLMOGOROV = r"C:\ronen\HUJI\Amirim\Paper\Lyx Figures\Figure kolmogorov phase screen plain.png"
    r = FullOptimizerExperimentResult()
    r.loadfrom(FIG5_DATA_PATH)
    fig = r.show_diffuser_plain()
    plt.show()

    fig.savefig(FIG_OUT_KOLMOGOROV)


def optimization_process_for_review():
    plt.rcParams.update({'font.size': 14})
    plt.rcParams["figure.figsize"] = (9, 5)

    ro = OptimizerResult()
    DATA_PATH = r"C:\ronen\HUJI\Amirim\Paper\Data For Figures\figs_4_5_all_data\optimization.optimizer"
    ro.loadfrom(DATA_PATH)
    fig, ax = ro.show_optimization_review()
    plt.show()

    FIG22_REVIEW_OUT_PATH = r"C:\ronen\HUJI\Amirim\Paper\Lyx Figures\Figure optimization for review.png"
    fig.savefig(FIG22_REVIEW_OUT_PATH)


if __name__ == "__main__":
    # fig3()
    fig4()
    fig5()
    fig6(conv_rect=30)
    fig3v2()

    # create_fig2_data()
    # fig2()
    # new_fig2()
    # fig_nano()
    # only_kolmogorov_phase()
    # only_kolmogorov_plain()
    # optimization_process_for_review()
    # plt.show()
