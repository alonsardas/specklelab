import numpy as np
from numpy import pi, sin
import matplotlib.pyplot as plt

"""
    We can put on the SLM a phase mask between 0 to ~4*pi.
    Let's say we want to put on a phase mask that will resemble a mirror, so (depending on the wavelength) 
    we need that some of the light will be delayed by much more than 4*pi. 
    To achieve that we do MOD(2*pi), since a phase of phi+2*pi*n is ths same as phi. 
    They actually create mirrors this way, and it is called Blaed Grating, see here: 
    https://en.wikipedia.org/wiki/Blazed_grating
    The purpose of this script (at least the "main" function) is to demonstrate that the function,
    and then it's Fourier transfrom (which is what we will measure on the camera) really remain the same, 
    and that this is a good trick. 
    Finally, we also show what happens if we aren't perfect (we aren't, since lambda isn't 100% monochromatic etc.)
    and we see that it takes ~20% to show serious effect (you can play with the "pert" variable).  
    Usage: just run it. 
"""


def get_sin_function():
    X = np.linspace(0, 0.2, 1000000)
    A = 3
    B = 12
    w1 = 20
    w2 = 50
    Y = A*sin(2*pi*w1*X) + B*sin(2*pi*w2*X)
    return X, Y


def get_lin_function():
    X = np.linspace(0, 10, 100000)
    A = 50
    B = 10
    Y = A*X + B
    return X, Y


def plot_function(X, Y):
    plt.figure()
    plt.title("Function")
    plt.plot(X, Y, '.')
    plt.show()


def get_fourier(X, Y):
    fa = np.fft.fft(Y)
    freqs = np.fft.fftfreq(fa)
    return freqs, fa


def plot_fourier(freqs, fa):
    plt.figure()
    plt.title("Fourier")
    plt.plot(freqs, abs(fa), '.')
    plt.show()


def func_and_fourier():
    X, Y = get_lin_function()
    freqs, fa = get_fourier(X, Y)
    plot_function(X, Y)
    plot_fourier(freqs, fa)


def main():
    Lambda = 10
    X, Y = get_sin_function()
    E = np.exp(2*pi*1j/Lambda * Y)
    freqs, fa = get_fourier(X, E.real)

    plt.figure()
    plt.suptitle("Original")
    plt.subplot(211)
    plt.plot(X, E.real)
    plt.subplot(212)
    plt.plot(freqs, fa)
    plt.xlim(-1000, 1000)
    plt.show(block=False)

    Y_mod = np.mod(Y, Lambda)
    E = np.exp(2 * pi * 1j / Lambda * Y_mod)
    freqs, fa = get_fourier(X, E)
    plt.figure()
    plt.suptitle("With MOD lambda")
    plt.subplot(211)
    plt.plot(X, E)
    plt.subplot(212)
    plt.plot(freqs, fa)
    plt.xlim(-1000, 1000)
    plt.show(block=False)

    pert = 1.2
    Y_mod_pert = np.mod(Y, Lambda*pert)
    E = np.exp(2 * pi * 1j / Lambda * Y_mod_pert)
    freqs, fa = get_fourier(X, E)
    plt.figure()
    plt.suptitle(f"With MOD lambda*{pert}")
    plt.subplot(211)
    plt.plot(X, E)
    plt.subplot(212)
    plt.plot(freqs, fa)
    plt.xlim(-1000, 1000)
    plt.show()


if __name__ == "__main__":
    main()
