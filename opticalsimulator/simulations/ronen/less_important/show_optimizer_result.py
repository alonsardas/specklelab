import matplotlib.pyplot as plt
from opticalsimulator.results import OptimizerResult

import os
import sys

import matplotlib as mpl
# mpl.use('QT5Agg')

path = sys.argv[1]
name = os.path.basename(path)

r = OptimizerResult()
r.loadfrom(path)
r.show()
r.show_orig()
r.show_diffuser()
e = r.enhacement
plt.show()
