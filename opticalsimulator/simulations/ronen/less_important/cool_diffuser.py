import numpy as np
import matplotlib.pyplot as plt
from opticalsimulator import create_field_grid_by_light_source, GaussianLightSource, Borders


def show_gausian():
    BORDERS = Borders(-0.010, -0.010, 0.010, 0.010)

    light_source = GaussianLightSource(sigma=0.0035, wavelength=808e-9)

    field = create_field_grid_by_light_source(BORDERS, 1272, 1024,  light_source)
    intensity = np.abs(field.Es) ** 2
    fig, ax = plt.subplots()
    ax.imshow(intensity)
    fig.show()


def get_diffuser_mask(sigma=0.0035):
    BORDERS = Borders(-0.010, -0.010, 0.010, 0.010)

    light_source = GaussianLightSource(sigma=sigma, wavelength=808e-9)
    field = create_field_grid_by_light_source(BORDERS, 1272, 1024,  light_source)
    rand_phases = 2 * np.pi * np.random.rand(1024, 1272)
    field.Es = field.Es * np.exp(1j*rand_phases)

    field.Es = np.fft.fftshift(np.fft.ifft2(np.fft.ifftshift(field.Es), norm='ortho'))

    return np.angle(field.Es)
