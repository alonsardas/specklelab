import numpy as np
import matplotlib.pyplot as plt

# SLM no.2 (01) alphas from datasheet
wavelength = np.array([400, 410, 420, 430, 440, 450, 460, 470, 480, 488, 490, 500, 510, 520, 530, 532, 540, 550, 560, 570, 580, 590, 600, 610, 620, 630, 633, 640, 650, 660, 670, 680, 690, 700])
alpha = np.array([93, 100, 106, 112, 116, 121, 125, 130, 135, 138, 139, 144, 148, 152, 156, 157, 160, 164, 167, 171, 175, 178, 182, 186, 191, 195, 196, 199, 203, 207, 212, 216, 220, 223])
a, b = np.polyfit(wavelength, alpha, 1)
X = np.linspace(300, 900, 100)
Y = a*X + b
print('a:', a, 'b:', b)
plt.figure()
plt.plot(X, Y, '--')
plt.plot(wavelength, alpha, '*')
plt.show()
