import os;
import sys;

sys.path.append(os.path.abspath('../..'))
from opticalsimulator.elements import *
import matplotlib.pyplot as plt


def cicular_slit():
    fig, axes = plt.subplots(1, 2)

    # at disances of ~5 we start getting a lot of aliasing
    # typical_d = 0.15
    typical_d = 30

    slit_borders = Borders(-40, -40, 40, 40)
    x_pixels = 500
    y_pixels = 500

    # The light source is the whole field
    light_source = CircularLightSource(radius=1)
    input_camera = BasicCamera('input', axes[0])
    propagator = FourierLens()
    output_camera = BasicCamera('output', axes[1])

    system = OpticalSystem(slit_borders, x_pixels, y_pixels, light_source)
    system.add_device(input_camera)
    system.add_device(propagator)
    system.add_device(output_camera)
    system.simulate()
    # https://en.wikipedia.org/wiki/Fresnel_diffraction#Alternative_forms
    # https://en.wikipedia.org/wiki/Fresnel_diffraction#/media/File:Circular_Aperture_Fresnel_Diffraction_high_res.jpg
    # We want to see these rings. with tailoring the borders and distance we get something -
    # but really not surewhich and why and when exactly numerical effects come in here


def fresnel_spot():
    fig, axes = plt.subplots(1, 2)

    typical_d = 2

    slit_borders = Borders(-40, -40, 40, 40)
    x_pixels = 1500
    y_pixels = 1500

    # The light source is the whole field
    light_source = CircularBlockLightSource(radius=1)
    input_camera = BasicCamera('input', axes[0])
    propagator = AngularMomentumPropagator(typical_d)
    output_camera = BasicCamera('output', axes[1])

    system = OpticalSystem(slit_borders, x_pixels, y_pixels, light_source)
    system.add_device(input_camera)
    system.add_device(propagator)
    system.add_device(output_camera)
    system.simulate()
    # We try and see the Arago/Fresnel spot like here:
    # https://en.wikipedia.org/wiki/Arago_spot
    # Again - we see some success, but without real understanding of numerical issues


if __name__ == '__main__':
    # cicular_slit()
    fresnel_spot()
    plt.show()
