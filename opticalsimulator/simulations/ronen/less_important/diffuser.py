"""
Generate phase mask that simulating thin diffuser.
"""

import matplotlib.axes
import numpy as np
from matplotlib import pyplot as plt

from opticalsimulator import Borders, BasicCamera, OpticalSystem, \
    GaussianLightSource, SLMMask, FourierLens


# TODO: Rename module?
# TODO: Move module
def set_diffuser2(slm_device: SLMMask, sigma):
    """
    Creates a smooth and realistic diffuser phase mask.
    This method is more realistic then the macro-pixel method since macro-pixels approach has
    non-physical discontinuities (jumps) in the phase mask.
    This approach uses something similar to Gerchberg-Saxton algorithm.
    It is base on a code Ohad Lib wrote.
    """
    N_x = slm_device.pixels_x
    N_y = slm_device.pixels_y
    dx = slm_device.pixel_size_x
    dy = slm_device.pixel_size_y
    # The spacing between neighboring cells in the frequency grid
    # df_x = 1/(N_x*dx)
    # df_y = 1/(N_y*dy)
    f_x = np.fft.fftshift(np.fft.fftfreq(N_x, dx))
    f_y = np.fft.fftshift(np.fft.fftfreq(N_y, dy))
    f_Xs, f_Ys = np.meshgrid(f_x, f_y)

    # Power spectrum (von Karman) (What?)
    PSD = np.exp(-(f_Xs ** 2 + f_Ys ** 2) / (2 * sigma ** 2))
    PSD[int(N_y / 2), int(N_x / 2)] = 0  # removing the zero freq term
    # print(f_Xs[int(N_y / 2), int(N_x / 2)], f_Ys[int(N_y / 2), int(N_x / 2)])
    rand_spectrum = (np.random.randn(N_y, N_x) + 1j * np.random.randn(N_y, N_x)) * np.sqrt(PSD)
    ifft = np.fft.fftshift(np.fft.ifft2(np.fft.ifftshift(rand_spectrum), norm='ortho'))
    slm_device.phase_grid = np.angle(ifft)


def calc_sigma(theta_diffuser, k_wavevector):
    return theta_diffuser * k_wavevector / (2 * np.pi)


def test_ift_diffuser():
    fig, axes = plt.subplots(1, 2)
    slm_ax, far_field_camera_ax = axes
    assert isinstance(slm_ax, matplotlib.axes.Axes)
    assert isinstance(far_field_camera_ax, matplotlib.axes.Axes)

    wavelength = 633e-6
    k_wavevector = 2 * np.pi / wavelength

    light_source = GaussianLightSource(0, 0, 5, wavelength=wavelength)

    # Making large boundaries let us have more resolution in the far field (after Fourier is applied)
    world_borders = Borders(-20, -20, 20, 20)
    world_x_pixels = 5000
    world_y_pixels = 1000

    simulator = OpticalSystem(world_borders, world_x_pixels, world_y_pixels, light_source)

    slm_width = 15.9
    slm_height = 12.8

    slm_borders = Borders(-slm_width / 2, -slm_height / 2, slm_width / 2, slm_height / 2)
    slm_x_pixels = 1272
    slm_y_pixels = 1024

    slm_device = SLMMask(slm_borders, slm_x_pixels, slm_y_pixels, dark_outside_borders=True)

    sigma = calc_sigma(0.001, k_wavevector)
    set_diffuser2(slm_device, sigma)

    slm_ax.set_title('slm mask')
    np.mod(slm_device.phase_grid, 2 * np.pi, out=slm_device.phase_grid)
    slm_ax.imshow(slm_device.phase_grid, vmin=0, vmax=2 * np.pi)

    f = 100
    fourier_lens = FourierLens(f)
    far_field_camera = BasicCamera('far field', far_field_camera_ax, live_update=False)

    simulator.add_device(slm_device)
    simulator.add_device(fourier_lens)
    simulator.add_device(far_field_camera)
    simulator.simulate()
    plt.show()


if __name__ == '__main__':
    test_ift_diffuser()
