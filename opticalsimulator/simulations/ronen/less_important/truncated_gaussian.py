import numpy as np
import matplotlib.pyplot as plt


N = 2048
dx = 12.5e-3
x = np.linspace(-N / 2, N / 2, N) * dx
y = x
dy = dx
X, Y = np.meshgrid(x, y)


def show_field(A, title=''):
    fig, ax = plt.subplots()
    extent = (x[0], x[-1], y[0], y[-1])
    ax.imshow(A, extent=extent)
    ax.set_title(title)
    ax.set_aspect('equal')
    fig.show()

def main():
    sigma_0 = 0.7  # 0.7 mm

    G = np.exp(-(X**2 + Y**2)/(sigma_0**2))
    show_field(G, 'original')

    new_G = np.zeros((N, N))
    N_pixels_x = 1 / dx
    # x_pixels_range = np.index_exp[-N_pixels_x / 2: N_pixels_x / 2]
    N_pixels_y = 2 / dx
    # y_pixels_range = np.index_exp[-N_pixels_y / 2: N_pixels_y / 2]
    active_range = np.index_exp[int(N/2 - N_pixels_y // 2): int(N/2 + N_pixels_y // 2), int(N/2 - N_pixels_x // 2): int(N/2 + N_pixels_x // 2)]

    new_G[active_range] = G[active_range]
    show_field(new_G, 'truncated')

    final = np.abs(np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(new_G))))**2
    show_field(final, 'farfield')



if __name__ == "__main__":
    main()
    plt.show()
