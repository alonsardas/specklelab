import os; import sys; sys.path.append(os.path.abspath('../..'))
from opticalsimulator.elements import * 

import numpy as np 
import matplotlib.pyplot as plt 
import time 


def lab():
    slm = RealSLMDevice()
    cam = VimbaCamera(1)

    angle = np.pi/4
    fig, ax = plt.subplots()
    for m in range(1, 200, 20):
        phase = get_mirror_phase(m, angle, slm.correction.shape[1], slm.correction.shape[0])
        time.sleep(0.2)
        im = cam.get_image()
        ax.clear()
        ax.imshow(im)
        fig.show()
        plt.pause(0.0001)  # Note this correction
        slm.update_phase(phase)

    cam.close()


def alternating_angles():
    slm = RealSLMDevice()

    angle = np.pi/4
    sign = -1

    while True:
        sign *= -1
        phase = get_mirror_phase(sign*50, sign*angle, slm.correction.shape[1], slm.correction.shape[0])
        time.sleep(0.6)
        plt.pause(0.0001)  # Note this correction
        slm.update_phase(phase)


def simulate():
    fig, axes = plt.subplots(1, 2)

    borders = Borders(-1, -1, 1, 1)
    light_source = GaussianLightSource(sigma=0.02)

    input_camera = BasicCamera('input', axes[0])
    field = create_field_grid_by_light_source(borders, 600, 600, light_source)
    input_camera.prop_through(field)

    mask_border = Borders(-1, -1, 1, 1)
    pixels_x = 1272
    pixels_y = 1024
    slm = SLMMask(mask_border, pixels_x=pixels_x, pixels_y=pixels_y)

    angle = np.pi/4
    for m in range(200, 300, 20):
        field = create_field_grid_by_light_source(borders, 400, 400, light_source)
        mirror_phase = get_mirror_phase(m, angle, pixels_x, pixels_y)
        slm.update_phase(mirror_phase)

        field = slm.prop_through(field)
        # Fourier lense takes us through the lense, to z=f of lense
        fourier_lens = FourierLens()
        field = fourier_lens.prop_through(field)

        output_camera = BasicCamera('output', axes[1])
        output_camera.prop_through(field)

    plt.show()


def get_mirror_phase(m, angle, sizex, sizey):
    X = np.linspace(0, m*np.pi, sizex)
    Y = np.linspace(0, m*np.pi, sizey)
    Xs, Ys = np.meshgrid(X, Y)
    phase = np.sin(angle)*Xs + np.cos(angle)*Ys
    return phase 


if __name__ == "__main__":
    # lab()
    alternating_angles()
