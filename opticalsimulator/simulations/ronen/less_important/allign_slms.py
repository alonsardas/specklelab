# Use these lines in ipython to do aligning
from opticalsimulator import RealSLMDevice, VimbaCamera
import numpy as np

slm1 = RealSLMDevice(1)
slm2 = RealSLMDevice(2, use_mirror=True)
cam = VimbaCamera(0, exposure_time=150)

slm1.normal(); slm2.normal(); cam.show_image()  # This is for reference (in colorbar) of max light we expect

# Here we see that we capture 90% of all the light
slm1.not_rand_range(630, 780, 240, 510); slm2.normal(); cam.show_image()
slm1.normal(); slm2.not_rand_range(323, 473, 312, 582); cam.show_image()

# Here we see that after diffuser on specific active_mask we have almost no DC
A=2*np.pi*np.random.rand(17, 17); slm1.update_phase_in_active(A, active_mask_slice=np.index_exp[240:510, 630:780]); slm2.normal(); cam.show_image()
A=2*np.pi*np.random.rand(17, 17); slm1.normal(); slm2.update_phase_in_active(A, active_mask_slice=np.index_exp[312:582, 323:473]); cam.show_image()

# Here we see that opposite phases on slms fix each other.
A=2*np.pi*np.random.rand(17, 17); slm1.update_phase_in_active(A, active_mask_slice=np.index_exp[240:510, 630:780]); slm2.update_phase_in_active(2*np.pi-np.fliplr(A), active_mask_slice=np.index_exp[312:582, 323:473]); cam.show_image()


