import numpy as np
import matplotlib.pyplot as plt
import datetime

from opticalsimulator.results import RedOptimizerResult
from opticalsimulator.simulations.ronen.optimize import Optimizer
from opticalsimulator.simulations.ronen.photon_scan import PhotonScanner

LOGS_DIR = "C:\\temp"


def get_optimizer():
    frame_around_interest_width = 3
    D = frame_around_interest_width
    # This way we have enough of a speckle effect, enough exposure to see them, and the nothing (including DC)
    # isn't over exposed
    diffuser_macro_pixels = 40
    exposure_time = 500
    slm_macro_pixels = 12  # When only in slice
    # slm_macro_pixels = 60
    sleep_period = 0.1
    # active_mask_slice = np.index_exp[470:770, 570:770]
    active_mask_slice = np.index_exp[440:800, 540:800]
    # active_mask_slice = None
    a = 276
    b = 292

    o = Optimizer(is_simulation=False, slices_of_interest=np.index_exp[a:a + 2, b:b + 2],
                  output_of_interest=np.index_exp[a - D:a + 2 + D, b - D:b + 2 + D],
                  macro_pixels_x_diffuser=diffuser_macro_pixels, macro_pixels_y_diffuser=diffuser_macro_pixels,
                  macro_pixels_x_slm=slm_macro_pixels, macro_pixels_y_slm=slm_macro_pixels,
                  sleep_period=sleep_period, exposure_time=exposure_time,
                  active_mask_slice=active_mask_slice)

    return o


def get_scanner():
    best_x = 6.3
    best_y = 8.825

    integration_time = 6

    start_x = 6.275
    start_y = 8.825
    x_pixels = 3
    y_pixels = 3
    pixel_size_x = 0.025
    pixel_size_y = 0.025

    scanner = PhotonScanner(integration_time, start_x, start_y, x_pixels, y_pixels, pixel_size_x, pixel_size_y)
    return scanner


def main():
    o = get_optimizer()
    scanner = get_scanner()
    res = RedOptimizerResult()
    timestamp = datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')

    ans = None
    # while ans != 'yes':
    #     o._init_diffuser(should_save=True)
    #     ans = input('Do you like the speckles? ')

    path = r"C:\Users\Owner\Google Drive\People\Ronen\results\2020-09-16\speckles6\diffuser4-3pix"
    phase_grid = np.load(path)['phase_grid']
    o._init_diffuser(True, phase_grid)

    optimize_g = o.optimize(method=Optimizer.PARTITIONING, iterations=5000)

    blue_powers = []
    red_powers = []

    i = 0
    while i < 5000:
        i += 1
        blue_powers = next(optimize_g)

        if i % 15 == 1:
            print('\n*Scanning reds*')
            single1s, single2s, coincidences = scanner.scan()
            red_power = coincidences.sum()
            red_powers.append((i, red_power))
            print(f'Red power: {red_power}\n')
            res.red_powers = np.array(red_powers)
            res.blue_powers = np.array(blue_powers)

            saveto_path = f"{LOGS_DIR}\\{timestamp}_red_blue.npz"
            res.saveto(saveto_path)

    red_powers = np.array(red_powers)
    x_blue = np.arange(1, len(blue_powers) + 1)
    fig, ax = plt.subplots()
    ax.set_title('optimization of red and blue')
    ax.plot(x_blue, blue_powers, color='blue')
    ax.set_xlabel('iterations')
    ax.set_ylabel('blue power')
    ax2 = ax.twinx()
    ax2.plot(red_powers[:, 0], red_powers[:, 1], color='red', marker='*')
    ax2.set_ylabel('red power')
    fig.show()
    input('finish 1?')
    plt.show()
    fig.savefig(f"C:\\temp\\blue_red_optimization.png")


if __name__ == '__main__':
    main()
    plt.show()
    input('finish 2?')
