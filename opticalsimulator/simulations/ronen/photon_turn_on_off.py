# run this script when you turn the single photon detector on / off

import time
from opticalsimulator.lab_games.photon_counter import PhotonCounter


def main():

    print('getting photon counter...')
    ph = PhotonCounter(integration_time=1)
    flag = True
    while flag:
        try:
            datas, _, actual_exp_time = ph.read()
            datas = datas / actual_exp_time
            # TODO: fancy graphs like in matlab, or at least some kind of gui that you can see from a distance
            print(f'singles: [{datas[0]:0.2f}] [{datas[1]:0.2f}] [{datas[2]:0.2f}] [{datas[3]:0.2f}] ')

        except:
            pass

    ph.close()


if __name__ == '__main__':
    main()